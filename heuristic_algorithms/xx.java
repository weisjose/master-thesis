import java.io.BufferedInputStream; 
import java.io.IOException; 
import java.io.InputStream; 
import java.util.ArrayList; 
import java.util.Collections; 
import java.util.logging.Logger; 

public class Sort { 
    private static final Logger logger = Logger.getLogger(Sort.class.getName()); 
    
    public static void main(String[] args) { 
        Input input = new Input(-1, SortType.NOT_SORTED, false, new ArrayList<>()); 
        
        try { 
            input = Reader.read(System.in); 
            // input = Reader.read(new FileInputStream("src/resources/sort_1_10.in")); 
        } catch (IOException e) { 
            logger.warning(e.getMessage()); 
        } 
        
        if (input.sortType() == SortType.DESCENDING) { 
            Collections.reverse(input.data()); 
        } 
        
        InputValidation.validate(input); 
        solveSorting(input); 
        
        for (int number : input.data()) { 
            System.out.println(number); 
        } 
    } 
    
    private static void solveSorting(Input input) { 
        switch (input.sortType()) { 
            case NOT_SORTED -> quickSort(input.data(), 0, input.data().size() - 1); 
            case ASCENDING, DESCENDING -> { 
                if (input.hasVirus()) { 
                    insertionSort(input.data()); 
                } 
            } 
        } 
    } 
    
    public static void quickSort(ArrayList<Integer> arrayList, int low, int high) { 
        if (low < high) { 
            int pivotIndex = partition(arrayList, low, high); 
            quickSort(arrayList, low, pivotIndex - 1); 
            quickSort(arrayList, pivotIndex + 1, high); 
        } 
    } 
    
    public static int partition(ArrayList<Integer> arrayList, int low, int high) { 
        int pivot = arrayList.get(high); 
        int i = low - 1; 
        
        for (int j = low; j < high; j++) { 
            if (arrayList.get(j) < pivot) { 
                i++; 
                int temp = arrayList.get(i); 
                arrayList.set(i, arrayList.get(j)); 
                arrayList.set(j, temp); 
            } 
        } 
        
        int temp = arrayList.get(i + 1); 
        arrayList.set(i + 1, arrayList.get(high)); 
        arrayList.set(high, temp); 
        
        return i + 1; 
    } 
    
    private static void insertionSort(ArrayList<Integer> data) { 
        for (int i = 1; i < data.size(); i++) { 
            int key = data.get(i); 
            int j = i - 1; 
            
            while (j >= 0 && data.get(j) > key) { 
                data.set(j + 1, data.get(j)); 
                j = j - 1; 
            } 
            
            data.set(j + 1, key); 
        } 
    } 
} 

class Reader { 
    public static Input read(InputStream inputStream) throws IOException { 
        try { 
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream); 
            
            // Read the maximum value, Sort type, and presence of virus 
            String[] firstLine = readLine(bufferedInputStream).split(" "); 
            int maximum = Integer.parseInt(firstLine[0]); 
            SortType sortType = SortType.valueOf(Integer.parseInt(firstLine[1])); 
            boolean hasVirus = false; 
            
            if (firstLine[2].equals("1")) { 
                hasVirus = true; 
            } else if (!firstLine[2].equals("0")) { 
                System.err.println("Error: Nelze urcit, zda posloupnost napadl virus!"); 
                System.exit(1); 
            } 
            
            // Read the array data 
            ArrayList<Integer> data = new ArrayList<>(); 
            String line; 
            
            while ((line = readLine(bufferedInputStream)) != null && !line.isEmpty()) { 
                data.add(Integer.parseInt(line.trim())); 
            } 
            
            return new Input(maximum, sortType, hasVirus, data); 
        } finally { 
            // Close the input stream 
            if (inputStream != null) { 
                inputStream.close(); 
            } 
        } 
    } 
    
    private static String readLine(BufferedInputStream bufferedInputStream) throws IOException { 
        StringBuilder stringBuilder = new StringBuilder(); 
        int character; 
        
        while ((character = bufferedInputStream.read()) != '\n' && character != -1) { 
            stringBuilder.append((char) character); 
        } 
        
        return stringBuilder.toString().trim(); 
    } 
} 

class InputValidation { 
    public static void validate(Input input) { 
        validateMaximum(input.maximum()); 
        validateData(input); 
    } 
    
    private static void validateData(Input input) { 
        validateNumbers(input); 
        
        if (!input.hasVirus()){ 
            if (input.sortType() != SortType.NOT_SORTED) { 
                checkIfSorted(input.data()); 
            } 
        } 
        
        validateDataLength(input.data()); 
    } 
    
    private static void validateMaximum(int maximum) { 
        if (maximum < 1) { 
            System.err.println("Error: Maximum neni kladne!"); 
            System.exit(1); 
        } 
    } 
    
    private static void validateNumbers(Input input) { 
        int maximum = input.maximum(); 
        
        for (int number : input.data()) { 
            if (number <= 0 || number > maximum) { 
                System.err.println("Error: Prvek posloupnosti je mimo rozsah!"); 
                System.exit(1); 
            } 
        } 
    } 
    
    private static void validateDataLength(ArrayList<Integer> data) { 
        if (data.size() > 2000000) { 
            System.err.println("Error: Posloupnost ma vic nez 2000000 prvku!"); 
            System.exit(1); 
        } 
        
        if (data.size() < 1000) { 
            System.err.println("Error: Posloupnost ma mene nez 1000 prvku!"); 
            System.exit(1); 
        } 
    } 
    
    private static void checkIfSorted(ArrayList<Integer> data) { 
        for (int i = 0; i < data.size() - 1; i++) { 
            if (data.get(i) > data.get(i + 1)) { 
                System.err.println("Error: Posloupnost neni usporadana!"); 
                System.exit(1); 
            } 
        } 
    } 
} 

record Input(int maximum, SortType sortType, boolean hasVirus, ArrayList<Integer> data) { 
} 

enum SortType { 
    ASCENDING(1), 
    DESCENDING(2), 
    NOT_SORTED(0); 
    
    private final int value; 
    
    SortType(int value) { 
        this.value = value; 
    } 
    
    public static SortType valueOf(int value) { 
        for (SortType sortType : SortType.values()) { 
            if (sortType.value == value) { 
                return sortType; 
            } 
        } 
        
        System.err.println("Error: Neznamy typ razeni posloupnosti!"); 
        System.exit(1); 
        
        return null; 
    } 
}
