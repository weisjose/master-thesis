import matplotlib.pyplot as plt
import numpy as np

np.random.seed(0)
data1 = np.random.normal(loc=0, scale=1, size=1000)
data2 = np.random.normal(loc=2, scale=1.5, size=1000)

num_bins = 20

hist1, bins1, _ = plt.hist(data1, bins=num_bins, alpha=0.5, color='blue', label='Before')
hist2, bins2, _ = plt.hist(data2, bins=num_bins, alpha=0.5, color='red', label='After')

plt.xlabel('Value')
plt.ylabel('Frequency')
plt.title('Histogram with Two Boxes per Bin')
plt.legend()

plt.show()