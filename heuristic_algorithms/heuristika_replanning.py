import time

from utilities.path_finder import find_path204
from utilities.utils import sort_by_time_difference

def replanning(info_array_all, seznam_sousedu, data_grafikony,
               cointainers_to_replan, to_drop_mask,
               containers_in_grafikon, grafikons_per_container2, grafikon_stops, 
               num_containers_in_grafikon, wagon_allocation, wagons_per_container, total_times, timetables_total_weight,
               path_per_container):              

   start_time = time.time()
   cointainers_to_replan = sorted(cointainers_to_replan)
   
   info_array = []
   for i in range(len(cointainers_to_replan)):
      curr_container = cointainers_to_replan[i]
      info_array.append(info_array_all[curr_container])

   info_array = sorted(info_array, key=sort_by_time_difference)

   grafikons_per_container2_changes = []
   containers_in_grafikon_changes = []
   grafikon_stops_changes = []
   wagon_allocation_changes = []
   wagons_per_container_changes = []
   timetables_total_weight_changes = []
   path_per_container_changes = []
   end_time = time.time()
   total_times[0] += end_time - start_time
   finished_correctly = True
   for i in range(len(cointainers_to_replan)):
      start_time = time.time()
      info = info_array[i]
      odkud = info[0]
      kam = info[1]
      release_date = info[2]
      deadline = info[3]
      teu = info[4]
      idx2 = info[5]
      weight = info[8]
   #   print(i, odkud, kam, len(valid_container_array_id))
      end_time = time.time()
      total_times[1] += end_time - start_time
      start_time = time.time()
      a, grafikon_arr = find_path204(seznam_sousedu, odkud, kam, release_date, deadline, #super_dict
                                   data_grafikony['trains'], wagon_allocation, teu, to_drop_mask, weight, timetables_total_weight)
      end_time = time.time()
      total_times[4] += end_time - start_time
      start_time = time.time()
      valid = True
      if valid:
         curr_grafikon_stops = []
         wagons_per_container_changes.append((idx2, wagons_per_container[idx2]))

         wagons_per_container[idx2] = []
         for idd, grafikon_idd in enumerate(grafikon_arr):
            odkud_now = a[idd]
            kam_now = a[idd + 1]
            containers_in_grafikon[grafikon_idd].append(idx2)
            containers_in_grafikon_changes.append((grafikon_idd, idx2))
            num_containers_in_grafikon[grafikon_idd] += 1

           # used_grafikons[grafikon_id] += 1
            found_start = False
            start_idx = 0
            end_idx = 0
            for j in range(len(data_grafikony['trains'][grafikon_idd]["hub_route"])):
               stop_name = data_grafikony['trains'][grafikon_idd]["hub_route"][j]["stop_code"]
               if stop_name == odkud_now:
                  found_start = True
                  start_idx = j
                  continue
               if found_start:
                  for k in range(27):
                     if len(wagon_allocation[grafikon_idd][j - 1][k]) < 5 - teu:
                        wagon_allocation_changes.append((grafikon_idd, j - 1, k, idx2, teu))
                        for _ in range(teu):
                           wagon_allocation[grafikon_idd][j - 1][k].append(idx2)
                        wagons_per_container[idx2].append(k)
                        break
                  timetables_total_weight_changes.append((grafikon_idd, j - 1, weight))
                  timetables_total_weight[grafikon_idd][j - 1] += weight
               if stop_name == kam_now:
                  end_idx = j - 1
                  break
            curr_grafikon_stops.append((start_idx, end_idx))
         grafikon_stops_changes.append((idx2, grafikon_stops[idx2]))
         grafikon_stops[idx2] = curr_grafikon_stops

      end_time = time.time()
      total_times[2] += end_time - start_time
      if len(a) > 0:
         grafikons_per_container2_changes.append((idx2, grafikons_per_container2[idx2]))
         grafikons_per_container2[idx2] = grafikon_arr
         path_per_container_changes.append((idx2, path_per_container[idx2]))
         path_per_container[idx2] = a
      else:
         #return to previous state
         start_time = time.time()
         for change in path_per_container_changes:
            cont_idd = change[0]
            arr = change[1]
            path_per_container[cont_idd] = arr
         for change in timetables_total_weight_changes:
            grafikon_idd = change[0]
            stop = change[1]
            weight = change[2]
            timetables_total_weight[grafikon_idd][stop] -= weight
         for change in grafikons_per_container2_changes:
            cont_idd = change[0]
            arr = change[1]
            grafikons_per_container2[cont_idd] = arr
         for change in containers_in_grafikon_changes:
            graf_idd = change[0]
            cont_idd = change[1]
            containers_in_grafikon[graf_idd].remove(cont_idd)
            num_containers_in_grafikon[graf_idd] -= 1
         for change in grafikon_stops_changes:
            cont_idd = change[0]
            stops = change[1]
            grafikon_stops[cont_idd] = stops
         for change in wagon_allocation_changes:
            grafikon_idd = change[0]
            stop = change[1]
            wagon = change[2]
            container = change[3]
            teu = change[4]
            for _ in range(teu):
               wagon_allocation[grafikon_idd][stop][wagon].remove(container)
         for change in wagons_per_container_changes:
            curr_container = change[0]
            wagons_per_container[curr_container] = change[1]

       #  used_grafikons_changes.append((grafikon_idd, j - 1))
         end_time = time.time()
         total_times[3] += end_time - start_time
         finished_correctly = False
         break

   return finished_correctly, total_times
