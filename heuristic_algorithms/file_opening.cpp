#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <ctime>
#include <chrono>
#include <iomanip>
#include <unordered_map>
//#include <jsoncpp/json/json.h>
#include "json.hpp"
#include <queue>

using json = nlohmann::json;
using namespace std;


#define HOUR 60
#define DAY 1440

struct Train {
    int max_length;
    std::vector<std::unordered_map<std::string, std::string>> hub_route;
    int operational_days;
    int grafikon_ID;
};

vector<string> split(const string& s, char delimiter) {
   vector<string> tokens;
   stringstream ss(s);
   string token;
   while (getline(ss, token, delimiter)) {
      tokens.push_back(token);
   }
   return tokens;
}

std::chrono::system_clock::time_point convert_to_time_point(const string& datetime_string, const string& parsing_format) {
   tm tmStruct = {};
   istringstream ss(datetime_string);
   ss >> get_time(&tmStruct, parsing_format.c_str());
   return chrono::system_clock::from_time_t(mktime(&tmStruct));
}

int convert_to_minutes(const string& datetime_string_1, const string& datetime_string_2, const string& parsing_format){
   std::chrono::system_clock::time_point time_point_1 = convert_to_time_point(datetime_string_1, parsing_format);
   std::chrono::system_clock::time_point time_point_2 = convert_to_time_point(datetime_string_2, parsing_format);
   std::chrono::minutes difference_in_minutes= std::chrono::duration_cast<std::chrono::minutes>(time_point_2 - time_point_1);
   int difference = difference_in_minutes.count();
   return difference;
}

struct ContainerInfo {
   int ID;
   int release_date;
   int deadline;
   string source;
   string destination;
};

void bubblesort(vector<pair<int, int>>& difference_in_times_vector){
   size_t num_of_containers = difference_in_times_vector.size();
   for (size_t i = 0; i < num_of_containers; i++){
      for (size_t j = 0; j < num_of_containers - i - 1; j++){
         if (difference_in_times_vector[j].second > difference_in_times_vector[j + 1].second){
            pair<int, int> temp = difference_in_times_vector[j];
            difference_in_times_vector[j] = difference_in_times_vector[j + 1];
            difference_in_times_vector[j + 1] = temp;
         }
      }
   }
}

void repairTop(vector<pair<int, int>>& a, int top, int bott) {
   int i = top; 
   int j = i*2; 
   int topVal = a[top - 1].second;
   pair<int, int> topValPair = a[top - 1];
   if ((j < bott) && (a[j - 1].second < a[j+1 - 1].second)) j++;
   while ((j <= bott) && (topVal < a[j - 1].second)) {
      a[i - 1] = a[j - 1];
      i = j; j = j*2; 
   if ((j < bott) && (a[j - 1].second < a[j+1 - 1].second)) j++;
   }
   a[i - 1] = topValPair;
}

void heapSort(vector<pair<int, int>>& a) {
   int n = a.size();
   int i, j;
   for (i = n/2; i > 0; i--)
      repairTop(a, i, n);
   for (i = n; i > 1; i--) {
      pair<int, int> temp = a[i - 1];
      a[i - 1] = a[0];
      a[0] = temp;
      repairTop(a, 1, i-1);
   }
}

/*
def find_path200(seznam_sousedu, start, end, release_date, deadline, super_dict, 
               used_grafikons, data_graf, grafikons_capacity, do_print = False):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0, start))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY
    best_so_far_cost = {}
    best_so_far_cost[start] = 0
    counter1 = 0
    counter3 = 0
    counter_2_max = 0
    while not queue.empty():
        priority, city_release_date, current, path, grafikons, curr_cost, code_string = queue.get()
        curr_city_end = current + end
        counter1 += 1
   #     if queue.qsize() > 50:
   #      print(start, end, queue.qsize())
        if best_so_far[current] < city_release_date:
           continue
        if current == end:
            paths.append(path)
            grafikon_arr = grafikons
      #      print(counter1, counter3, counter_2_max)
            return path, grafikon_arr
        if len(path) >= 5:
            continue
        else:
            counter3 += 1
            counter2 = 0
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               counter2 += 1
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               new_code_string = code_string + neighbor
               code_string_to_check = code_string + end
          #     print(code_string_to_check, start, current, neighbor, end)
               if code_string_to_check not in super_dict:
                  if neighbor != end:
                     continue
               elif neighbor not in super_dict[code_string_to_check]:
                  continue

               found_start = False
               over_capacity = False
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start and used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]:
                     over_capacity = True
                     break
                  if stop_name == neighbor:
                     break

               if over_capacity:
                  continue
               if neighbor not in path:
                  cost = curr_cost
                  if used_grafikons[grafikon_idx] == 0:
                     cost += data_graf[grafikon_idx]['cost']
                  if neighbor in best_so_far:
                     if best_so_far[neighbor] > n_deadline:
                        best_so_far[neighbor] = n_deadline
                        best_so_far_cost[neighbor] = cost
                     else:
                        continue
                  else:
                     best_so_far[neighbor] = n_deadline
                     best_so_far_cost[neighbor] = cost
                  new_path = path + [neighbor]
                  new_grafikons = grafikons + [grafikon_idx]
                  new_priority = cost
                  queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons, cost, new_code_string))
               counter_2_max = max(counter_2_max, counter2)
    return paths, grafikon_arr
*/


struct Neighbor {
    std::string neighbor;
    int grafikon_idx;
    int neighbor_release_date;
    int n_deadline;
};

struct Compare {
    bool operator()(const std::tuple<int, int, std::string, std::vector<std::string>, std::vector<int>, int, std::string>& a, const std::tuple<int, int, std::string, std::vector<std::string>, std::vector<int>, int, std::string>& b) {
        return std::get<0>(a) > std::get<0>(b); 
    }
};

void find_path(unordered_map<string, vector<tuple<string, int, int, int>>> & seznam_sousedu, 
               string source, string destination, int release_date, int deadline, json& possible_cesty,
               vector<vector<int>>& used_grafikons, json& data_grafikony_json, 
               vector<int>& grafikons_capacity, vector<string>& a, vector<int>& grafikon_arr, bool do_print) {
    using QueueElement = tuple<int, int, string, vector<string>, vector<int>, int, string>;
    std::priority_queue<QueueElement, std::vector<QueueElement>, Compare> queue;
    queue.push(make_tuple(0, release_date, source, vector<string>{source}, vector<int>{}, 0, source));
   // std::vector<std::vector<std::string>> paths;

    std::unordered_map<std::string, int> best_so_far;
    std::unordered_map<std::string, int> best_so_far_cost;
    best_so_far[source] = release_date;
    best_so_far_cost[source] = 0;

    while (!queue.empty()) {
        auto [priority, city_release_date, current, path, grafikons, curr_cost, code_string] = queue.top();
        queue.pop();
        
        string curr_city_end = current + destination;

        if (best_so_far[current] < city_release_date)
            continue;

        if (current == destination) {
            a = path;
            grafikon_arr = grafikons;
//            cout << city_release_date << endl;
            return;
        }

        if (path.size() >= 5){
            continue;
        }
        auto neighbors = seznam_sousedu.find(current);
            //for (auto [neighbor, grafikon_idx, neighbor_release_date, n_deadline] : neighbors->second) {
         for (std::tuple<std::string, int, int, int>& tup : neighbors->second){
            std::string neighbor = std::get<0>(tup);
            int grafikon_idx = std::get<1>(tup);
            int neighbor_release_date = std::get<2>(tup);
            int n_deadline = std::get<3>(tup);
            if (do_print){
            for (int i=0; i < path.size(); i++ ){
               cout << path[i] << " ";
            }
            cout << neighbor << " ";
            cout << grafikon_idx << " ";
            cout << neighbor_release_date << " ";
            cout << n_deadline << " ";
            cout << city_release_date << " ";
           // cout << deadline << " ";
            cout << endl;
            }
            if (neighbor_release_date < city_release_date || deadline  < n_deadline){
           //    cout << endl;
               continue;
            }
            std::string new_code_string = code_string + neighbor;
            std::string code_string_to_check = code_string + destination;
            /*
            if (!possible_cesty.contains(code_string_to_check)){
               if (neighbor != destination){
                  continue;
               }
            }
            else if ((possible_cesty[code_string_to_check].find(neighbor) == possible_cesty[code_string_to_check].end())){
               continue;
            }
            */
            bool found_start = false;
            bool over_capacity = false;
            for (size_t j = 0; j < data_grafikony_json[grafikon_idx]["hub_route"].size(); ++j) {
               std::string stop_name = data_grafikony_json[grafikon_idx]["hub_route"][j]["stop_code"];
               if (stop_name == current) {
                  found_start = true;
                  continue;
               }
               if (found_start && used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]) {
                  over_capacity = true;
                  break;
               }
               if (stop_name == neighbor)
                  break;
            }
          //  if (n_deadline == 34047){
          //     cout << used_grafikons[198][0] << " " << grafikons_capacity[198] << " " << over_capacity << endl;
          //  }
            if (over_capacity){
            //   cout << endl;
               continue;
            }
        //    if (grafikon_idx == 1886){
        //       cout << "tyxx" << " " << neighbor << endl;
        //    }
            if (path.end() == std::find(path.begin(), path.end(), neighbor)) {
               int cost = neighbor_release_date;
               auto best_neighbor = best_so_far.find(neighbor);
               if (best_neighbor != best_so_far.end()) {
                  if (best_neighbor->second > n_deadline) {
                        best_so_far[neighbor] = n_deadline;
                        best_so_far_cost[neighbor] = cost;
                  } else {
                //     cout << "xx" << " " << endl;
                        continue;
                  }
               } else {
                  best_so_far[neighbor] = n_deadline;
                  best_so_far_cost[neighbor] = cost;
               }

               auto new_path = path;
               new_path.push_back(neighbor);

               auto new_grafikons = grafikons;
               new_grafikons.push_back(grafikon_idx);

               auto new_priority = n_deadline;
             //  cout << "grafikon " << grafikon_idx  << " neighbor "<< neighbor << " has been added to queue" << endl;
               queue.push(std::make_tuple(new_priority, n_deadline, neighbor, new_path, new_grafikons, cost, new_code_string));
            }
        } 
    }

    return;
}

void constructive_heuristic(vector<ContainerInfo>& container_info_vector, vector<int>& grafikons_capacity,
                           vector<vector<int>>& used_grafikons, vector<vector<int>>& containers_in_grafikon,
                           vector<int>& grafikon_ids,
                           unordered_map<string, vector<tuple<string, int, int, int>>> & seznam_sousedu, 
                           vector<int>& num_containers_in_grafikon, json& data_grafikony_json, 
                           vector<vector<pair<int, int>>>& grafikon_stops, vector<vector<int>>& grafikons_per_container2,
                           vector<pair<int, int>>& difference_in_times_vector, json& possible_cesty){

   int num_containers = container_info_vector.size();
   int total_cost = 0;
   int counter = 0;

   std::ifstream inputFile("serazeni.txt");
    
    if (!inputFile.is_open()) {
        std::cerr << "Error opening file!" << std::endl;
        return;
    }
    
    std::vector<int> numbers;
    int num;
    
    while (inputFile >> num) {
        numbers.push_back(num);
    }
    


   for (int i = 0; i < num_containers; i++){
      int idx = numbers[i]; //14243;//numbers[i]; //difference_in_times_vector[i].first;
      ContainerInfo info = container_info_vector[idx];
      string kam = container_info_vector[idx].destination;
      string odkud = container_info_vector[idx].source;
      int release_date = container_info_vector[idx].release_date;
      int deadline = container_info_vector[idx].deadline;

      if (i % 1000 == 0){
         cout << i << endl;
      }
    //  if (i != 2034){
    //     continue;
     // }
      vector<string> a;
      vector<int> grafikon_arr;
  /*    if (i == 2034){
      find_path(seznam_sousedu, odkud, kam, release_date, deadline, possible_cesty,
                                       used_grafikons, data_grafikony_json, grafikons_capacity,
                                       a, grafikon_arr, true);
      }
      else{*/
         find_path(seznam_sousedu, odkud, kam, release_date, deadline, possible_cesty,
                                       used_grafikons, data_grafikony_json, grafikons_capacity,
                                       a, grafikon_arr, false);
     // }
   //   if (i == 2034){
         cout << i << " ";
         for (int y=0; y < a.size(); y++){
            cout << a[y] << " ";
         }
         for (int y=0; y < grafikon_arr.size(); y++){
            cout << grafikon_arr[y] << " ";
         }
         cout << endl;
     //    return;
    //  }
   //   if (a.size() == 0){
  //       cout << i << " " <<  idx << " " << odkud << " " <<  kam << " " << release_date << " " << deadline << endl;
   //   }
   //   return;
   //   if (i > 100){
  //    for (int j = 0; j < grafikon_arr.size(); j++){
  //       cout << i << " " << grafikon_arr[j] << " ";
  //    }
   //   cout << endl;
    //  }
   //   return;
    //  if (i > 120){
    //     return;
    //  }
      if (a.size() > 0) {
         counter += 1;
        int cost = 0;
        for (size_t idd = 0; idd < grafikon_arr.size(); ++idd) {
            int grafikon_idd = grafikon_arr[idd];
            if (grafikon_idd == 0) {
                std::cout <<  ", " << ", " << grafikon_idd << std::endl;
                exit(0);
            }

            std::string odkud_now = a[idd];
            std::string kam_now = a[idd + 1];
            bool is_not_used = true;
            containers_in_grafikon[grafikon_idd].push_back(idx);
            num_containers_in_grafikon[grafikon_idd] += 1;
            for (size_t j = 0; j < used_grafikons[grafikon_idd].size(); ++j) {
                if (used_grafikons[grafikon_idd][j] != 0) {
                    is_not_used = false;
                    break;
                }
            }
            if (is_not_used) {
               cost += int(data_grafikony_json[grafikon_idd]["cost"]);
            }
            bool found_start = false;
            bool over_capacity = false;
            int start_idx = 0;
            int end_idx = 0;
            bool ended = false;

            for (size_t j = 0; j < data_grafikony_json[grafikon_idd]["hub_route"].size(); ++j) {
                std::string stop_name = data_grafikony_json[grafikon_idd]["hub_route"][j]["stop_code"];
                if (stop_name == odkud_now) {
                    found_start = true;
                    start_idx = j;
                    continue;
                }
                if (found_start) {
                    used_grafikons[grafikon_idd][j - 1] += 1;
                }
                if (stop_name == kam_now) {
                    ended = true;
                    end_idx = j - 1;
                    break;
                }
            }
            if (!ended) {
                exit(0);
            }

            grafikon_stops[idx].push_back(std::make_pair(start_idx, end_idx));
        }

        total_cost += cost;
    }

   }
   cout << counter << endl; 
   cout << total_cost << endl;
}

int main() {
   ifstream file("../data_processing/processed_data_containers/kontejnery_filtered_removed_impossible.csv");

   if (!file.is_open()) {
      cerr << "Error opening file!" << endl;
      return 1;
   }
   const char* parsing_format = "%Y-%m-%d";
   const string start_of_month = "2023-01-01";
 //  std::chrono::system_clock::time_point start_of_month_timepoint = convert_to_timepoint(start_of_month, parsing_format);
   vector<ContainerInfo> container_info_vector;
   vector<pair<int, int>> difference_in_times_vector;
   string line;
   int counter = 0;
   while (getline(file, line)) {
      vector<string> tokens = split(line, ',');
      if (counter == 0){
         counter += 1;
         continue;
      }
      struct ContainerInfo container;
      //token[3] is TEU
      container.ID = std::stoi(tokens[0]);
      container.release_date = max(convert_to_minutes(start_of_month, tokens[5], parsing_format), 0);
      container.deadline = convert_to_minutes(start_of_month, tokens[4], parsing_format);
      container.source = tokens[2];
      container.destination = tokens[1];
      container_info_vector.push_back(container);
      int difference = (container.deadline - container.release_date) / (60 * 24);
      difference_in_times_vector.push_back(make_pair(counter - 1, difference));
  //    cout << (container.deadline - container.release_date) / (60 * 24)<< endl;
      counter += 1;
   }
   file.close();
 //  bubblesort(difference_in_times_vector);
   heapSort(difference_in_times_vector);
   for (int i=0; i < difference_in_times_vector.size(); i++){
 //     cout << difference_in_times_vector[i].first << " " << difference_in_times_vector[i].second << endl;
   }

  // std::unordered_map<std::string, std::vector<std::string>> seznam_sousedu;
  // std::unordered_map<std::string, std::vector<std::array<int, 4>>> seznam_sousedu;
   std::unordered_map<std::string, std::vector<std::tuple<std::string, int, int, int>>> seznam_sousedu;
   file.open("../data_processing/heuristic_simplification_data/terminals_matrix_filtered.csv");
   if (!file.is_open()) {
      std::cerr << "Error opening file! \n" << endl;
      return 1;
   }

   if (std::getline(file, line)) {
      std::istringstream iss(line);
      std::string token;
      if (std::getline(iss, token, ',')) {
         while (std::getline(iss, token, ',')) {
               seznam_sousedu[token] = {};
         }
      }
   //todo - check seznam sousedu length with python
   } else {
      std::cerr << "Empty file.\n";
      return 1;
   }

   /*
     file.open("../data_processing/processed_data_timetables/timetables.json");
   if (!file.is_open()) {
      std::cerr << "Error opening file! \n" << endl;
      return 1;
   }
   */
   file.close();

   std::ifstream f("../data_processing/processed_data_timetables/timetables.json");
   json data_grafikony_json2 = json::parse(f);
   json data_grafikony_json = data_grafikony_json2["trains"];
   f.close();

   std::ifstream f2("../data_processing/heuristic_simplification_data/path_to_stops_in_routes.json");
   json possible_cesty = json::parse(f2);
   f2.close();

   int num_containers = difference_in_times_vector.size();

   /*
   std::vector<Train> data_grafikony_json(data_grafikony_json_json["trains"].size());
   
   for (size_t i = 0; i < data_grafikony_json_json["trains"].size(); ++i) {
      data_grafikony_json[i].max_length = data_grafikony_json_json["trains"][i]["max_length"];
      data_grafikony_json[i]["hub_route"].resize(data_grafikony_json_json["trains"][i]["hub_route"].size());
      for (size_t j = 0; j < data_grafikony_json_json["trains"][i]["hub_route"].size(); ++j) {
   //      data_grafikony_json[i]["hub_route"][j]["departure"] = data_grafikony_json_json["trains"][i]["hub_route"][j]["departure"];
   //      data_grafikony_json[i]["hub_route"][j]["stop_code"] = data_grafikony_json_json["trains"][i]["hub_route"][j]["stop_code"];
         cout << data_grafikony_json_json["trains"][i]["hub_route"][j]["curr_total_minutes"] << endl;
         data_grafikony_json[i]["hub_route"][j]["curr_total_minutes"] = data_grafikony_json_json["trains"][i]["hub_route"][j]["curr_total_minutes"];
      }
   //   data_grafikony_json[i].operational_days = data_grafikony_json_json["trains"][i]["operational_days"];
  //    data_grafikony_json[i].grafikon_ID = data_grafikony_json_json["trains"][i]["grafikon_ID"];
   }
   */

   std::vector<int> grafikons_capacity(data_grafikony_json.size(), 0);
   std::vector<std::vector<int>> used_grafikons(data_grafikony_json.size());
   std::vector<std::vector<int>> containers_in_grafikon(num_containers);
   std::vector<int> num_containers_in_grafikon(data_grafikony_json.size(), 0);
   std::vector<std::vector<pair<int, int>>> grafikon_stops(num_containers);
   std::vector<int> grafikon_ids;
   std::vector<std::vector<int>> grafikons_per_container2(data_grafikony_json.size());

   for (size_t i = 0; i < data_grafikony_json.size(); ++i) {
      int data_grafikony_json_max_length = data_grafikony_json[i]["max_length"];
      grafikons_capacity[i] = data_grafikony_json_max_length / 12;

      for (size_t x = 1; x < data_grafikony_json[i]["hub_route"].size(); ++x) {;
         used_grafikons[i].push_back(0);
      }
      grafikon_ids.push_back(data_grafikony_json[i]["grafikon_ID"]);
      string departure = data_grafikony_json[i]["hub_route"][0]["departure"];
      int departure_hours = std::stoi(departure.substr(0, 2));
      int departure_minutes = std::stoi(departure.substr(3, 2)) + departure_hours * HOUR;
      for (size_t x = 0; x < data_grafikony_json[i]["hub_route"].size(); ++x) {
         auto starting_city = data_grafikony_json[i]["hub_route"][x]["stop_code"];

         string this_departure = data_grafikony_json[i]["hub_route"][x]["departure"];
         int this_departure_hours = std::stoi(this_departure.substr(0, 2));
         int this_departure_minutes = std::stoi(this_departure.substr(3, 2)) + this_departure_hours * HOUR;
         int op_day_start = data_grafikony_json[i]["operational_days"];
         int op_day;

         if (x == 0){
            op_day = (op_day_start - 1);
         }
         else{
            int curr_total_minutes = data_grafikony_json[i]["hub_route"][x]["curr_total_minutes"];
            op_day = (op_day_start - 1) + ((departure_minutes + curr_total_minutes) / DAY);
         }
         
         
         int this_grafikon_time = op_day * DAY + this_departure_minutes;
         for (size_t j = x + 1; j < data_grafikony_json[i]["hub_route"].size(); ++j) {
               string end_city = data_grafikony_json[i]["hub_route"][j]["stop_code"];
               int curr_total_minutes = data_grafikony_json[i]["hub_route"][j]["curr_total_minutes"];
             //  seznam_sousedu[starting_city].push_back({end_city, static_cast<int>(i), this_grafikon_time,
             //                                         this_grafikon_time + curr_total_minutes});
               seznam_sousedu[starting_city].push_back(std::make_tuple(end_city, 
                           static_cast<int>(i), this_grafikon_time, 
                           this_grafikon_time + curr_total_minutes));
         }
      }
   }

  // container_info = (odkud_array, kam_array, info_array)
 //  grafikony_info = (grafikons_capacity, used_grafikons, containers_in_grafikon, grafikon_ids, seznam_sousedu,
  //                   num_containers_in_grafikon)
   constructive_heuristic(container_info_vector, 
                           grafikons_capacity,
                           used_grafikons,
                           containers_in_grafikon,
                           grafikon_ids,
                           seznam_sousedu,
                           num_containers_in_grafikon,
                           data_grafikony_json,
                           grafikon_stops,
                           grafikons_per_container2,
                           difference_in_times_vector,
                           possible_cesty);





   return 0;
}
