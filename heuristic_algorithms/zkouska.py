def compare_files(file1, file2):
    with open(file1, 'r') as f1, open(file2, 'r') as f2:
        # Read lines from both files
        lines1 = f1.readlines()
        lines2 = f2.readlines()
        
        # Compare lines
        for i, (line1, line2) in enumerate(zip(lines1, lines2), 1):
            if line1 != line2:
                return False, i
        
        # Check if one file has more lines than the other
        if len(lines1) != len(lines2):
            return False, max(len(lines1), len(lines2))
        
        # If all lines are the same
        return True, None

# Example usage
file1 = 'xxx.out'
file2 = 'xxx2.out'
same, diff_line = compare_files(file1, file2)
if same:
    print("The files are the same.")
else:
    print(f"The files differ at line {diff_line}.")
