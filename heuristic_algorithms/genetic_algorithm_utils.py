import numpy as np
from heuristika_1_container_serazeni_validator11 import constructive_heuristic
import copy
from grafikon_removal import find_least_full_timetable
from utilities.validator import validate_solution2
from heuristika_replanning import replanning
from utilities.utils import compute_cost

def createInitSolution(num_dims = 10, mean = 0, std_dev = 0.1):
    init_solution = np.random.normal(mean, std_dev, num_dims)
    normalized_init_solution = (init_solution - np.min(init_solution)) / (np.max(init_solution) - np.min(init_solution))
    return normalized_init_solution

def evaluate_terminal_order_with_cost(x, info):
    terminal_priorities = np.argsort(np.argsort(x))
    container_info = copy.deepcopy(info[0])
    grafikony_info = copy.deepcopy(info[1])
    grafikon_stops = copy.deepcopy(info[2])
    grafikons_per_container2 = copy.deepcopy(info[3])
    cities = info[5]
    info_array = container_info[0]
    wagons_per_container = container_info[1]
    timetables_total_weight = grafikony_info[6]
    path_per_container = container_info[2]
#    f, cost = constructive_heuristic_with_terminal_end(df, terminal_priorities)
    num_timetables = len(grafikony_info[0])
    wagon_allocation = grafikony_info[3]
    f, total_cost, info_arrays = constructive_heuristic(container_info, grafikony_info, grafikon_stops, 
                                                        grafikons_per_container2, terminal_priorities)
    if f != 1:
        return 50000000
        return f, 0
    
    new_cost = compute_cost(num_timetables, wagon_allocation, grafikony_info, path_per_container, cities)
   
    print(new_cost)
    #validate_solution2(info[4], grafikony_info[4]['trains'], grafikons_per_container2, grafikon_stops, grafikony_info[3])
    grafikons_per_container2, containers_in_grafikon = info_arrays[0], info_arrays[1]
    grafikon_stops = info_arrays[2]
    num_containers_in_grafikon = info_arrays[3]
    wagon_allocation = grafikony_info[3]
    immune_grafikons = np.zeros(num_timetables, dtype=bool)
    to_drop_mask = np.zeros(len(containers_in_grafikon), dtype=bool)

    counter_containers = 0
    total_elapsed_time_replanning = 0
    total_elapsed_time_find_least_full_timetable = 0
    total_elapsed_time_before_replanning = 0
    total_elapsed_time_after_replanning = 0
    total_times = [0, 0, 0, 0, 0]
    seznam_sousedu = grafikony_info[1]
    for x in range(10000):
        min_idx, min_val = find_least_full_timetable(immune_grafikons, num_containers_in_grafikon, to_drop_mask)
        if min_val == np.inf:
            break
        counter = 0
    #    if x % 100 == 0:
   #         print(x)
        containers_to_replan = copy.deepcopy(containers_in_grafikon[min_idx])
        previous = copy.deepcopy(containers_in_grafikon[min_idx])
        all_used_grafikons_by_container_to_replan = []
        counter_containers += len(containers_to_replan)
        for i in range(len(containers_to_replan)):
            curr_container = containers_to_replan[i]
            teu = info_array[curr_container][4]
            weight = info_array[curr_container][8]
            used_grafikons_by_container_to_replan = copy.deepcopy(grafikons_per_container2[curr_container])
            all_used_grafikons_by_container_to_replan.append(used_grafikons_by_container_to_replan)
            counter = 0
            for j, used in enumerate(used_grafikons_by_container_to_replan):
                curr_grafikon_stops_start = grafikon_stops[curr_container][j][0]
                curr_grafikon_stops_end = grafikon_stops[curr_container][j][1]
                for k in range(curr_grafikon_stops_start, curr_grafikon_stops_end + 1):
                    curr_wagon = wagons_per_container[curr_container][counter]
                    counter += 1
                    for _ in range(teu):
                #       print(curr_container, grafikons_per_container2[curr_container], k, wagon_allocation[used][k][15], teu)
                        wagon_allocation[used][k][curr_wagon].remove(curr_container)
                    timetables_total_weight[used][k] -= weight
                containers_in_grafikon[used].remove(curr_container)
                num_containers_in_grafikon[used] -= 1

        to_drop_mask[min_idx] = True

        finished_correctly, total_times = replanning(info_array, 
                                                    seznam_sousedu, 
                                                    grafikony_info[4],
                                                    containers_to_replan, 
                                                    to_drop_mask,
                                                    containers_in_grafikon,
                                                    grafikons_per_container2,
                                                    grafikon_stops,
                                                    num_containers_in_grafikon,
                                                    wagon_allocation,
                                                    wagons_per_container,
                                                    total_times,
                                                    timetables_total_weight,
                                                    path_per_container)

        if not finished_correctly:
            for i in range(len(containers_to_replan)):
                curr_container = containers_to_replan[i]
                teu = info_array[curr_container][4]
                weight = info_array[curr_container][8]
                used_grafikons_by_container_to_replan = grafikons_per_container2[curr_container]
                counter = 0
                for j, used in enumerate(used_grafikons_by_container_to_replan):
                    curr_grafikon_stops_start = grafikon_stops[curr_container][j][0]
                    curr_grafikon_stops_end = grafikon_stops[curr_container][j][1]
                    for k in range(curr_grafikon_stops_start, curr_grafikon_stops_end + 1):
                        curr_wagon = wagons_per_container[curr_container][counter]
                        counter += 1
                        for _ in range(teu):
                            wagon_allocation[used][k][curr_wagon].append(curr_container)
                        timetables_total_weight[used][k] += weight
                        
                    containers_in_grafikon[used].append(curr_container)
                    num_containers_in_grafikon[used] += 1

            immune_grafikons[min_idx] = True
            containers_in_grafikon[min_idx] = previous
            num_containers_in_grafikon[min_idx] = len(previous)
            to_drop_mask[min_idx] = False
            
    new_cost = compute_cost(num_timetables, wagon_allocation, grafikony_info, path_per_container, cities)
    print(new_cost)
   # validate_solution2(info[4], grafikony_info[4]['trains'], grafikons_per_container2, grafikon_stops, wagon_allocation)

    return new_cost #f, new_cost

def PertubationNormalNormalize(x, std_dev = 0.1):
    x_copy = copy.deepcopy(x)
    normal_mean = 0
    x_copy += np.random.normal(normal_mean, std_dev, len(x))
    normalized_x_copy = (x_copy - np.min(x_copy)) / (np.max(x_copy) - np.min(x_copy))
    return normalized_x_copy