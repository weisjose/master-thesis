import pandas as pd
import numpy as np
import json
from datetime import datetime
import copy
from collections import deque
from queue import PriorityQueue
import time
from utilities.constants import HOUR, DAY, start_of_month

from utilities.validator import validate_solution, validate_solution2
from utilities.path_finder import find_path, find_path2, find_path40, find_path6, \
                                    find_path200, find_path20, find_path20000, find_path201
from utilities.utils import parse_date, sort_by_second_element, sort_by_time_difference, \
                            convert_paths_and_grafikons_to_end_points
from grafikon_removal import find_least_full_timetable

from global_variables import path_to_stops_in_routes, path_to_terminal_matrix_filtered
"""
def replanning(container_info, grafikony_info, data_grafikony,
               cointainers_to_replan, used_grafikons, to_drop, to_drop_mask,
               containers_in_grafikon, grafikons_per_container2, grafikon_stops):
"""
def replanning(odkud_array_all, kam_array_all, info_array_all, 
               grafikons_capacity, grafikon_ids, seznam_sousedu, data_grafikony,
               cointainers_to_replan, used_grafikons, to_drop, to_drop_mask,
               containers_in_grafikon, grafikons_per_container2, grafikon_stops, 
               possible_cesty, num_containers_in_grafikon):              
#   start_time = time.time()

   #teu_array = []
   cointainers_to_replan = sorted(cointainers_to_replan)
  # odkud_array_all = container_info[0]
 #  kam_array_all = container_info[1]
 #  info_array_all = container_info[2]

 #  grafikons_capacity = grafikony_info[0]
 #  grafikon_ids = grafikony_info[1]
 #  seznam_sousedu = grafikony_info[2]
   
   info_array = []
   for i in range(len(cointainers_to_replan)):
      curr_container = cointainers_to_replan[i]
      info_array.append(info_array_all[curr_container])

 #  end_time = time.time()
 #  elapsed_time = end_time - start_time
  # print("Iterating through df:", elapsed_time, "seconds")

   for i in range(len(cointainers_to_replan)):
      curr_container = cointainers_to_replan[i]

   info_array = sorted(info_array, key=sort_by_time_difference)

#   end_time = time.time()
#   elapsed_time = end_time - start_time
  # print("Sorting containers:", elapsed_time, "seconds")


 #  with open('../data_processing/processed_data_timetables//grafikon_casy_komplet_filter.json', 'r') as file:
 #     data_grafikony = json.load(file)

 #  with open(path_to_stops_in_routes) as file:
 #     possible_cesty = json.load(file)

   #used_grafikons = np.zeros(len(data_grafikony['trains']), dtype=int)

 #  end_time = time.time()
 #  elapsed_time = end_time - start_time

   # Print the elapsed time
 #  print("Iterating through df trains:", elapsed_time, "seconds")
         
   valid_container_array_id = []
   total_cost = 0
   path_per_container = []
   grafikons_per_container = []
   container_id_array = []

   #end_time = time.time()
  # elapsed_time = end_time - start_time

   find_path_type = 0
   do_cut = False
 #  print("Start of path finding:", elapsed_time, "seconds")
   for i in range(len(cointainers_to_replan)):
      info = info_array[i]
      idx2 = info[2]
    #  idx2 = info[3]

      kam = kam_array_all[idx2]
      odkud = odkud_array_all[idx2]
   #   print(i, odkud, kam, len(valid_container_array_id))
      if find_path_type == 0:
         if do_cut:
            a, grafikon_arr = find_path6(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                       used_grafikons, data_grafikony['trains'], grafikons_capacity, None)
         else:
            a, grafikon_arr = find_path201(seznam_sousedu, odkud, kam, info[1], info[0],possible_cesty, #super_dict
                                       used_grafikons, data_grafikony['trains'], grafikons_capacity, to_drop_mask)
      elif find_path_type == 1:
         a, grafikon_arr = find_path3(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                    used_grafikons, data_grafikony['trains'], grafikons_capacity)
      else:
         a, grafikon_arr = find_path40(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                    used_grafikons, data_grafikony['trains'], grafikons_capacity)
      
      valid = True
      if valid:
         cost = 0
         curr_grafikon_stops = []
         for idd, grafikon_idd in enumerate(grafikon_arr):
            grafikon_id = grafikon_ids[grafikon_idd]
         #   print(a, grafikon_arr)
            odkud_now = a[idd]
            kam_now = a[idd + 1]
            is_not_used = True
            containers_in_grafikon[grafikon_idd].append(idx2)
            num_containers_in_grafikon[grafikon_idd] += 1
            for j in range(len(used_grafikons[grafikon_idd])):
               if used_grafikons[grafikon_idd][j] != 0:
                  is_not_used = False
                  break
            if is_not_used:
               cost += data_grafikony['trains'][grafikon_idd]['cost']

           # used_grafikons[grafikon_id] += 1
            found_start = False
            over_capacity = False
            start_idx = 0
            end_idx = 0
            for j in range(len(data_grafikony['trains'][grafikon_idd]["hub_route"])):
               stop_name = data_grafikony['trains'][grafikon_idd]["hub_route"][j]["stop_code"]
               if stop_name == odkud_now:
                  found_start = True
                  start_idx = j
                  continue
               if found_start:
                  used_grafikons[grafikon_idd][j - 1] += 1
               if stop_name == kam_now:
                  end_idx = j - 1
                  break
            curr_grafikon_stops.append((start_idx, end_idx))
         grafikon_stops[idx2] = curr_grafikon_stops
    #        print(used_grafikons[grafikon_id], stop_name, odkud, kam, a, grafikons_capacity[grafikon_id])
   #      print(total_cost, cost, i)
         total_cost += cost
      if len(a) > 0:
         container_id_array.append(idx2)
         valid_container_array_id.append(idx2)
         path_per_container.append(a)
         grafikons_per_container.append(grafikon_arr)
         grafikons_per_container2[idx2] = grafikon_arr
      else:
         break
         container_id_array.append(None)
         path_per_container.append([])
         grafikons_per_container.append([])

 #  end_time = time.time()
 #  elapsed_time = end_time - start_time
 #  print("End of path finding:", elapsed_time, "seconds")

   info_arrays = (grafikons_capacity, used_grafikons, grafikons_per_container2, containers_in_grafikon, 
                  grafikon_stops, num_containers_in_grafikon)
   return len(valid_container_array_id), total_cost, info_arrays
