#include <iostream>
#include <fstream>
#include <vector>
#include <deque>
#include <queue>
#include <algorithm>
#include <ctime>
#include <map>
\

void constructive_heuristic(const DataFrame& df, int find_path_type = 0, bool do_cut = false) {
    std::clock_t start_time = std::clock();

    std::vector<std::string> kam_array;
    std::vector<std::string> odkud_array;
    std::vector<int> container_array_id;
    std::vector<std::tuple<int, int, int>> info_array;
    int container_counter = 0;

    for (int index = 0; index < df.size(); ++index) {
        kam_array.push_back(df.at(index, "KAM"));
        odkud_array.push_back(df.at(index, "ODKUD"));

        DateTime parsed_deadline = parse_date(df.at(index, "DEADLINE"));
        DateTime parsed_release_time = parse_date(df.at(index, "RELEASE_TIME"));
        TimeDelta time_difference_deadline = parsed_deadline - start_of_month;
        TimeDelta time_difference_release_date = parsed_release_time - start_of_month;
        int deadline_in_minutes = std::max(0, static_cast<int>(time_difference_deadline.total_seconds() / 60 / DAY));
        int release_date_in_minutes = std::max(0, static_cast<int>(time_difference_release_date.total_seconds() / 60 / DAY));
        std::string name = df.at(index, "ODKUD") + df.at(index, "KAM");
        container_array_id.push_back(index);
        std::tuple<int, int, int> info = std::make_tuple(deadline_in_minutes, release_date_in_minutes, container_counter);
        container_counter++;
        info_array.push_back(info);
        if (release_date_in_minutes >= deadline_in_minutes) {
            std::cerr << "error" << std::endl;
            exit(1);
        }
    }

    std::clock_t end_time = std::clock();
    double elapsed_time = double(end_time - start_time) / CLOCKS_PER_SEC;
    std::cout << "Iterating through df: " << elapsed_time << " seconds" << std::endl;


}

int main() {
    constructive_heuristic(df);

    return 0;
}
