import pandas as pd
import numpy as np
import json
from datetime import datetime
import sys
import os

current_path = os.getcwd()
directory_path = os.path.dirname(current_path)

sys.path.append(directory_path)

from utilities.utils import parse_date, sort_by_third_element
from utilities.constants import HOUR, DAY, start_of_month
from global_variables import path_to_kontejnery_filtered_removed_impossible, path_to_stops_in_routes, \
                           path_to_terminal_matrix_filtered

def read_container_dataset(cities):
   info_array = []
   df = pd.read_csv(path_to_kontejnery_filtered_removed_impossible)
   num_containers = len(df)
   for index, row in df.iterrows():
        kam = row['KAM']
        odkud = row['ODKUD']

        parsed_deadline = parse_date(row['DEADLINE'])
        parsed_release_time = parse_date(row['RELEASE_TIME'])
        teu = int(row['TEU'])
        weight = int(row['VAHA_ZBOZI'])
        time_difference_deadline = parsed_deadline - start_of_month
        time_difference_release_date = parsed_release_time - start_of_month
        deadline_in_minutes = int(max(0, time_difference_deadline.total_seconds() // 60 // DAY))
        release_date_in_minutes = int(max(0, time_difference_release_date.total_seconds() // 60 // DAY))
        deadline_in_minutes = min(31, deadline_in_minutes)
        odkud_index = cities.index(row['ODKUD'])
        kam_index = cities.index(row['KAM'])
        info = (odkud, kam, release_date_in_minutes, deadline_in_minutes, teu, index, odkud_index, kam_index, weight)
        info_array.append(info)
   return info_array, num_containers, df

def read_timetables(seznam_sousedu, cities):
   with open('../data_processing/processed_data_timetables/timetables.json', 'r') as file:
      data_grafikony = json.load(file)

   num_grafikons = len(data_grafikony['trains'])
  # grafikons_capacity = np.zeros(len(data_grafikony['trains']), dtype=int) + 108
  # used_grafikons = [[] for _ in range(num_grafikons)]
   containers_in_grafikon = [[] for _ in range(num_grafikons)]
   wagon_allocation = [[] for _ in range(num_grafikons)]
   num_containers_in_grafikon = np.zeros(num_grafikons, dtype=int)
   used_timetables = np.zeros(num_grafikons, dtype=bool)
   timetables_total_weight = [[] for _ in range(num_grafikons)]
   grafikon_ids = []
   for i in range(num_grafikons):
      hub_route = data_grafikony['trains'][i]['hub_route']
      for x in range(1, len(hub_route)):
   #      used_grafikons[i].append(0)
         wagon_allocation[i].append([[] for _ in range(27)])
         timetables_total_weight[i].append(0)
      grafikon_ids.append(data_grafikony['trains'][i]['grafikon_ID'])

      parsed_start_date = datetime.strptime(hub_route[0]["departure"] , "%H:%M:%S")
      parsed_start_date_in_minutes = parsed_start_date.hour * HOUR + parsed_start_date.minute
      for x in range(len(hub_route)):
         starting_city = hub_route[x]["stop_code"]
         parsed_date = datetime.strptime(hub_route[x]["departure"] , "%H:%M:%S")
         if x == 0:
            op_day = (data_grafikony['trains'][i]['operational_days'] - 1)
         else:
            op_day = (data_grafikony['trains'][i]['operational_days'] - 1) + \
                     (parsed_start_date_in_minutes + hub_route[x]["curr_total_minutes"]) // DAY
         this_grafikon_time = op_day * DAY + parsed_date.hour * 60 + parsed_date.minute
         for j in range(x+1, len(hub_route)):
            end_city = hub_route[j]["stop_code"]
            seznam_sousedu[starting_city].append([end_city, i, this_grafikon_time, this_grafikon_time + 
                                                data_grafikony['trains'][i]['hub_route'][j]["curr_total_minutes"]])
   for city in cities:
      seznam_sousedu[city] = sorted(seznam_sousedu[city], key=sort_by_third_element)

   grafikony_info = (containers_in_grafikon, seznam_sousedu,
                     num_containers_in_grafikon, wagon_allocation, data_grafikony, used_timetables, timetables_total_weight)
   return grafikony_info

def read_terminal_matrix():
   df_cities = pd.read_csv(path_to_terminal_matrix_filtered).columns[1:].to_list()
   return df_cities

def read_stops_in_routes():
   with open(path_to_stops_in_routes, 'r') as file:
      possible_cesty = json.load(file)
   return possible_cesty
