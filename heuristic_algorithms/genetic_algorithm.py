import numpy as np
import sys
import os

from genetic_algorithm_utils import createInitSolution, PertubationNormalNormalize, evaluate_terminal_order_with_cost

from data_reading import read_container_dataset, read_terminal_matrix, read_timetables

class EvolutionaryAlgorithm:
    def __init__(self, f_objective, f_init, f_select, f_crossover, crossover_prob, 
                 f_mutation, replacement_strategy, max_iters, n_of_dims, population_size):
        self.objectiveFunction = f_objective
        self.initPopulation = f_init
        self.parentSelection = f_select
        self.crossoverFunction = f_crossover
        self.crossover_prob = crossover_prob
        self.mutationFunction = f_mutation
        self.replacement_strategy = replacement_strategy
        self.max_iters = max_iters
        self.D = n_of_dims
        self.population_size = population_size

def initPopulation(f_init_solution = createInitSolution, population_size = 10, D = 10):
        population = []
        for i in range(population_size):
            population.append(f_init_solution(D))
        return population

def tournamentSelection(fitness_values, n_of_parents = 11, tournament_size = 3):
        np_fitness_values = np.array(fitness_values)
        parent_indices = []
        indices = np.arange(len(fitness_values))
        can_be_chosen = np.ones(len(fitness_values), dtype=bool)
        for i in range(n_of_parents):
            candidates = np.random.choice(indices[can_be_chosen], tournament_size)
          #  print(candidates)
            best_candidate_index = np.argmax(np_fitness_values[candidates])
            best_candidate = candidates[best_candidate_index]
            parent_indices.append(best_candidate)
        return parent_indices

def singlePointCrossover(parents):
    num_of_parents = len(parents)
    D = len(parents[0])
    np_parents = np.array(parents)
    offsprings = np.zeros((num_of_parents * (num_of_parents - 1), D)) #2 offspring for each pair, n parents -> 2 * (n over 2) -> n * (n - 1)
    counter = 0
    for i in range(num_of_parents):
        crossover_point = np.random.randint(0, D)
        for j in range(i+1, num_of_parents):
            offsprings[counter][0:crossover_point] = parents[i][0:crossover_point]
            offsprings[counter][crossover_point::] = parents[j][crossover_point::]
            counter += 1
            offsprings[counter][0:crossover_point] = parents[j][0:crossover_point]
            offsprings[counter][crossover_point::] = parents[i][crossover_point::]
            counter += 1
    return offsprings

def priorityAdd(parents):
    num_of_parents = len(parents)
    D = len(parents[0])
    np_parents = np.array(parents)
    offsprings = np.zeros((20, D)) #2 offspring for each pair, n parents -> 2 * (n over 2) -> n * (n - 1)
    counter = 0
    for i in range(num_of_parents):
        crossover_point = np.random.randint(0, D)
        for j in range(i+1, num_of_parents):
            offsprings[counter] = parents[i] + parents[j]
            counter += 1
    print(counter)
    return offsprings

def generationalReplacement(old_population, old_population_eval, offsprings, offsprings_eval, population_size):
    return offsprings, offsprings_eval

def elitism(old_population, old_population_eval, offsprings, offsprings_eval, population_size):
    ranks_old = np.argsort(np.argsort(old_population_eval))
    ranks_new = np.argsort(np.argsort(offsprings_eval))
    new_population = []
    new_population_eval = []
    counter_used_old = 0
    counter_used_new = 0
    max_old_pop = int(0.2 * population_size)
    for i in range(population_size):
        if counter_used_old < max_old_pop:
            if old_population_eval[ranks_old[counter_used_old]] > offsprings_eval[ranks_new[counter_used_new]]:
                new_population.append(old_population[ranks_old[counter_used_old]])
                new_population_eval.append(old_population_eval[ranks_old[counter_used_old]])
                counter_used_old += 1  
            else:
                new_population.append(offsprings[ranks_new[counter_used_new]])
                new_population_eval.append(offsprings_eval[ranks_new[counter_used_new]])
                counter_used_new += 1
        else:
            new_population.append(offsprings[ranks_new[counter_used_new]])
            new_population_eval.append(offsprings_eval[ranks_new[counter_used_new]])
            counter_used_new += 1
    new_population = np.array(new_population)
    return new_population, new_population_eval

def runEA(EA_configuration):
    eval_results = []
    #get parameters
    D = EA_configuration.D
    max_iters = EA_configuration.max_iters
    population_size = EA_configuration.population_size
    #init
    population = np.array(EA_configuration.initPopulation(population_size = population_size, D = D))
    #evaluate
    evaluated_population = []
    num_parents = 5
    
    for i in range(population_size):
        val = EA_configuration.objectiveFunction(population[i])
        evaluated_population.append(-val)# - val[1] / 100000000)
        print("EVALUATED POPULATION: ",evaluated_population)
    eval_results.append(evaluated_population[0])
    evaluated_population = np.array(evaluated_population)
    BFS_solution_index = np.argmax(evaluated_population)
    BFS_value = evaluated_population[BFS_solution_index]
    
    print(BFS_value, BFS_solution_index)
    BFS_solution = population[BFS_solution_index]
    eval_results.append(BFS_value)
    for i in range(max_iters):
        #select
        selected_parents = np.array(EA_configuration.parentSelection(evaluated_population, num_parents))
        #crossover
        offsprings = EA_configuration.crossoverFunction(population[selected_parents])
        indices = np.arange(len(offsprings))
        random_indices = np.random.choice(indices, size=population_size, replace=False)
        offsprings = offsprings[random_indices]
        #mutate
        mutated_offsprings = []
        for j in range(len(offsprings)):
            mutated_offsprings.append(EA_configuration.mutationFunction(offsprings[j]))
        #evaluate
        mutated_offsprings = np.array(mutated_offsprings)
        evaluated_offsprings = []
        for k in range(len(offsprings)):
            val = EA_configuration.objectiveFunction(mutated_offsprings[k])
            evaluated_offsprings.append(-val)# - val[1] / 100000000)
            print("evaluated_offsprings: ", evaluated_offsprings)
        evaluated_offsprings = np.array(evaluated_offsprings)
        #update_bfs
        BFS_solution_index_offspring = np.argmax(evaluated_offsprings)
        BFS_value_offspring = evaluated_offsprings[BFS_solution_index_offspring]
        if i % 10 == 0:
             print("iter: ", i, " value: ", BFS_value)
        if BFS_value_offspring > BFS_value:
             BFS_value = BFS_value_offspring
             BFS_solution = offsprings[BFS_solution_index_offspring]
     #   print(BFS_solution, BFS_value)
        #replace
  #      print(BFS_value, BFS_value_offspring, evaluated_offsprings, "       offspring:   ", offsprings[BFS_solution_index_offspring])
      #  print(evaluated_population)
      #  print("offsprings:", selected_parents)
      #  print(offsprings)
        print(len(evaluated_offsprings))
        eval_results.append(BFS_value)
        population, evaluated_population = EA_configuration.replacement_strategy(population, evaluated_population, 
                                                                                 mutated_offsprings, evaluated_offsprings, 
                                                                                 population_size)
    return BFS_solution, eval_results
        




if __name__=="__main__":
    """
    df = pd.read_csv('kontejnery_filtered_no_impossible_containers.csv')
    """

    cities = read_terminal_matrix()
    info_array, num_containers, df = read_container_dataset(cities)

    grafikon_stops  = [[] for _ in range(num_containers)]
    grafikons_per_container2 = [[] for _ in range(num_containers)]
    wagons_per_container = [[] for _ in range(num_containers)]
    path_per_container = [[] for _ in range(num_containers)]

    seznam_sousedu = {}
    for city in cities:
        seznam_sousedu[city] = []

    container_info = (info_array, wagons_per_container, path_per_container)
    grafikony_info = read_timetables(seznam_sousedu, cities)
    num_timetables = len(grafikony_info[0])
   # df_cities = pd.read_csv('cities_matrix_filtered.csv')
    #cities = df_cities.columns[1:]
    info = (container_info, grafikony_info, grafikon_stops, grafikons_per_container2, df, cities)
    max_iters = 10
    D = len(cities)
    population_size = 20

    evaluate_terminal_order_lambda = lambda x: evaluate_terminal_order_with_cost(x, info=info)

    EA_configuration = EvolutionaryAlgorithm(evaluate_terminal_order_lambda, initPopulation, tournamentSelection, 
                                              priorityAdd, 1, PertubationNormalNormalize, generationalReplacement,
                                              max_iters, D, population_size)
    BFS_solution, eval_results = runEA(EA_configuration)
    print(BFS_solution)
    print(eval_results)

    