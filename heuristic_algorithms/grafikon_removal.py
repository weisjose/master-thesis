import numpy as np

def find_least_full_timetable(immune_grafikons, num_containers_in_grafikon, to_drop_mask):
   min_val = float('inf')
   min_idx = float('inf')

   mask = (to_drop_mask == True) | (num_containers_in_grafikon == 0)
   to_drop_mask[mask] = True

   masked_arr = np.where((to_drop_mask == True) | (immune_grafikons == True), np.inf, num_containers_in_grafikon)
   min_idx = np.argmin(masked_arr)
   min_val = masked_arr[min_idx]

   return min_idx, min_val
   