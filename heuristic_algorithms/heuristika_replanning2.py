import pandas as pd
import numpy as np
import json
from datetime import datetime
import copy
from collections import deque
from queue import PriorityQueue
import time
from utilities.constants import HOUR, DAY, start_of_month

from utilities.validator import validate_solution, validate_solution2
from utilities.path_finder import find_path, find_path2, find_path40, find_path6, \
                                    find_path200, find_path20, find_path20000, find_path201
from utilities.utils import parse_date, sort_by_second_element, sort_by_time_difference, \
                            convert_paths_and_grafikons_to_end_points
from grafikon_removal import find_least_full_timetable

from global_variables import path_to_stops_in_routes, path_to_terminal_matrix_filtered

def replanning(df, data_grafikony, cointainers_to_replan, 
               used_grafikons, to_drop, containers_in_grafikon, 
               grafikons_per_container2, grafikon_stops):
   start_time = time.time()

   kam_array = []
   odkud_array = []
   #teu_array = []
   container_array_id = []
   info_array = []
   container_counter = 0
   cointainers_to_replan = sorted(cointainers_to_replan)
   counter = 0
   for index, row in df.iterrows():
      if index == cointainers_to_replan[container_counter]:
         container_counter += 1
      else:
         continue
      kam_array.append(row['KAM'])
      odkud_array.append(row['ODKUD'])
   #  teu_array.append(row['TEU'])

      parsed_deadline = parse_date(row['DEADLINE'])
      parsed_release_time = parse_date(row['RELEASE_TIME'])
      time_difference_deadline = parsed_deadline - start_of_month
      time_difference_release_date = parsed_release_time - start_of_month
      deadline_in_minutes = int(max(0, time_difference_deadline.total_seconds() // 60 // DAY))
      release_date_in_minutes = int(max(0, time_difference_release_date.total_seconds() // 60 // DAY))
      name = row['ODKUD'] + row['KAM']
      container_array_id.append(index)
      info = (deadline_in_minutes, release_date_in_minutes, container_counter - 1, cointainers_to_replan[container_counter - 1])
      info_array.append(info)
      if release_date_in_minutes >= deadline_in_minutes:
         print("error")
         exit()
      if container_counter == len(cointainers_to_replan):
         break
   # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("Iterating through df:", elapsed_time, "seconds")

   info_array = sorted(info_array, key=sort_by_time_difference)
   #1st data preprocessing: dat do balicku, 
   #chceme, aby v kazdem balicku byly kontejnery se stejnym pocatecnim a koncovym terminalem

   # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("Sorting containers:", elapsed_time, "seconds")


 #  with open('../data_processing/processed_data_timetables//grafikon_casy_komplet_filter.json', 'r') as file:
 #     data_grafikony = json.load(file)

   with open(path_to_stops_in_routes) as file:
      possible_cesty = json.load(file)

   #used_grafikons = np.zeros(len(data_grafikony['trains']), dtype=int)
   grafikons_capacity = np.zeros(len(data_grafikony['trains']), dtype=int)
   seznam_sousedu = {}
   df_cities = pd.read_csv(path_to_terminal_matrix_filtered)
   cities = df_cities.columns[1:]
   for city in cities:
      seznam_sousedu[city] = []

   hub_route_array = []
   grafikon_ids = []

   grafikon_counter = 0
   for i in range(len(data_grafikony['trains'])):
      #length.. 6 = 1 TEU.. wagon - 24 TEU = length / 12 containers
      grafikons_capacity[i] = data_grafikony['trains'][i]['max_length'] // 12
      hub_route_array.append(data_grafikony['trains'][i]['hub_route'])
      temp_array = []
      temp_array2 = []
      grafikon_ids.append(data_grafikony['trains'][i]['grafikon_ID'])
  #    if i != data_grafikony['trains'][i]['grafikon_ID']:
  #       exit()
      parsed_start_date = datetime.strptime(hub_route_array[i][0]["departure"] , "%H:%M:%S")
      parsed_start_date_in_minutes = parsed_start_date.hour * HOUR + parsed_start_date.minute
      if i == to_drop[grafikon_counter]:
         grafikon_counter += 1
         continue
      for x in range(len(hub_route_array[i])):
         starting_city = hub_route_array[i][x]["stop_code"]
         parsed_date = datetime.strptime(hub_route_array[i][x]["departure"] , "%H:%M:%S")
         if x == 0:
            op_day = (data_grafikony['trains'][i]['operational_days'] - 1)
         else:
            op_day = (data_grafikony['trains'][i]['operational_days'] - 1) + \
                     (parsed_start_date_in_minutes + hub_route_array[i][x]["curr_total_minutes"]) // DAY
         this_grafikon_time = op_day * DAY + parsed_date.hour * 60 + parsed_date.minute
         for j in range(x+1, len(hub_route_array[i])):
            end_city = hub_route_array[i][j]["stop_code"]
            seznam_sousedu[starting_city].append([end_city, i, this_grafikon_time, this_grafikon_time + 
                                                data_grafikony['trains'][i]['hub_route'][j]["curr_total_minutes"]])
      # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("Iterating through df trains:", elapsed_time, "seconds")
         
   valid_container_array_id = []
   total_cost = 0
   path_per_container = []
   grafikons_per_container = []
   container_id_array = []
   # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   find_path_type = 0
   do_cut = False
   print("Start of path finding:", elapsed_time, "seconds")
   for i in range(len(container_array_id)):
      info = info_array[i]
      idx = info[2]
      idx2 = info[3]

      kam = kam_array[idx]
      odkud = odkud_array[idx]
   #   print(i, odkud, kam, len(valid_container_array_id))
      if find_path_type == 0:
         if do_cut:
            a, grafikon_arr = find_path6(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                       used_grafikons, data_grafikony['trains'], grafikons_capacity, None)
         else:
            a, grafikon_arr = find_path201(seznam_sousedu, odkud, kam, info[1], info[0],possible_cesty, #super_dict
                                       used_grafikons, data_grafikony['trains'], grafikons_capacity, [])
      elif find_path_type == 1:
         a, grafikon_arr = find_path3(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                    used_grafikons, data_grafikony['trains'], grafikons_capacity)
      else:
         a, grafikon_arr = find_path40(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                    used_grafikons, data_grafikony['trains'], grafikons_capacity)
      
      valid = True

      if valid:
         cost = 0
         curr_grafikon_stops = []
         for idd, grafikon_idd in enumerate(grafikon_arr):
            grafikon_id = grafikon_ids[grafikon_idd]
         #   print(a, grafikon_arr)
            odkud_now = a[idd]
            kam_now = a[idd + 1]
            is_not_used = True
            containers_in_grafikon[grafikon_idd].append(idx2)
            for j in range(len(used_grafikons[grafikon_idd])):
               if used_grafikons[grafikon_idd][j] != 0:
                  is_not_used = False
                  break
            if is_not_used:
               cost += data_grafikony['trains'][grafikon_idd]['cost']

           # used_grafikons[grafikon_id] += 1
            found_start = False
            over_capacity = False
            start_idx = 0
            end_idx = 0
            for j in range(len(data_grafikony['trains'][grafikon_idd]["hub_route"])):
               stop_name = data_grafikony['trains'][grafikon_idd]["hub_route"][j]["stop_code"]
               if stop_name == odkud_now:
                  found_start = True
                  start_idx = j
                  continue
               if found_start:
                  used_grafikons[grafikon_idd][j - 1] += 1
               if stop_name == kam_now:
                  end_idx = j - 1
                  break
            curr_grafikon_stops.append((start_idx, end_idx))
         grafikon_stops[idx2] = curr_grafikon_stops
    #        print(used_grafikons[grafikon_id], stop_name, odkud, kam, a, grafikons_capacity[grafikon_id])
   #      print(total_cost, cost, i)
         total_cost += cost
      if len(a) > 0:
         container_id_array.append(idx2)
         valid_container_array_id.append(idx2)
         path_per_container.append(a)
         grafikons_per_container.append(grafikon_arr)
         grafikons_per_container2[idx2] = grafikon_arr
      else:
         container_id_array.append(None)
         path_per_container.append([])
         grafikons_per_container.append([])
   
      # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("End of path finding:", elapsed_time, "seconds")

   # if len(b) > 0:
   #    valid_container_array_id2.append(i)
   #print(sum(used_grafikons))
  # count_positive = np.sum(used_grafikons > 0)

  # print("number of used grafikons :", count_positive)


   #min_idx, to_drop = find_least_full_timetable(data_grafikony, grafikons_capacity, used_grafikons, 
   #                                            grafikons_per_container2, containers_in_grafikon)
   info_arrays = (grafikons_capacity, used_grafikons, grafikons_per_container2, containers_in_grafikon, grafikon_stops)
   print("End of validation:", elapsed_time, "seconds")
   print("everything ok")
   return len(valid_container_array_id), total_cost, info_arrays
