import pandas as pd
import numpy as np
import json
from datetime import datetime
import copy
import time
import sys
import os

current_path = os.getcwd()
directory_path = os.path.dirname(current_path)

sys.path.append(directory_path)

from utilities.constants import HOUR, DAY, start_of_month

from utilities.validator import validate_solution2
from utilities.path_finder import find_path40, find_path6, \
                                    find_path200, find_path205, find_path206
from utilities.utils import sort_by_time_difference, sort_by_third_element, sort_by_terminal_then_time_difference, \
                              compute_cost
from grafikon_removal import find_least_full_timetable
from heuristika_replanning import replanning

from data_reading import read_container_dataset, read_terminal_matrix, read_stops_in_routes, read_timetables
import matplotlib.pyplot as plt

def constructive_heuristic(container_info, grafikony_info, grafikon_stops, grafikons_per_container2, 
                           terminal_order = None):

   containers_in_grafikon = []

   info_array = container_info[0]
   wagons_per_container = container_info[1]
   path_per_container = container_info[2]

   if terminal_order is None:
      info_array = sorted(info_array, key=sort_by_time_difference)
   else:
      info_array = sorted(info_array, key=lambda item: sort_by_terminal_then_time_difference(item, terminal_order))

   containers_in_grafikon = grafikony_info[0]
   seznam_sousedu = grafikony_info[1]
   num_containers_in_grafikon = grafikony_info[2]
   wagon_allocation = grafikony_info[3]
   data_grafikony = grafikony_info[4]
   used_timetables = grafikony_info[5]
   timetables_total_weight = grafikony_info[6]

   valid_container_array_id = []
   total_cost = 0

   counted_grafikons = []
   counted_grafikons_counter = 0
   num_containers = len(wagons_per_container)
   for i in range(num_containers):
      info = info_array[i]
      odkud = info[0]
      kam = info[1]
      release_date = info[2]
      deadline = info[3]
      teu = info[4]
      idx = info[5]
      weight = info[8]
      a, grafikon_arr = find_path205(seznam_sousedu, odkud, kam, release_date, deadline, 
                                       data_grafikony['trains'], wagon_allocation, teu, weight, timetables_total_weight)
      if len(a) == 0:
         return 0, 0, []
         a, grafikon_arr = find_path206(seznam_sousedu, odkud, kam, release_date, deadline, 
                                       data_grafikony['trains'],  wagon_allocation, teu)
         continue
      if len(a) > 0:
         cost = 0
         for idd, grafikon_idd in enumerate(grafikon_arr):
            odkud_now = a[idd]
            kam_now = a[idd + 1]
            containers_in_grafikon[grafikon_idd].append(idx)
            num_containers_in_grafikon[grafikon_idd] += 1

            if not used_timetables[grafikon_idd]:
               cost += data_grafikony['trains'][grafikon_idd]['cost']
               counted_grafikons.append(grafikon_idd)
               counted_grafikons_counter += 1

           # used_grafikons[grafikon_id] += 1
            found_start = False
            start_idx = 0
            end_idx = 0
            ended = False
            for j in range(len(data_grafikony['trains'][grafikon_idd]["hub_route"])):
               stop_name = data_grafikony['trains'][grafikon_idd]["hub_route"][j]["stop_code"]
               if stop_name == odkud_now:
                  found_start = True
                  start_idx = j
                  continue
               if found_start:
                  for k in range(27):
                     if len(wagon_allocation[grafikon_idd][j - 1][k]) < 5 - teu:
                        for _ in range(teu):
                           wagon_allocation[grafikon_idd][j - 1][k].append(idx)
                        wagons_per_container[idx].append(k)
                        break
                  used_timetables[grafikon_idd] = True
                  timetables_total_weight[grafikon_idd][j - 1] += weight
               if stop_name == kam_now:
                  ended = True
                  end_idx = j - 1
                  break
            if not ended:
               print("not ended")
               exit()
            grafikon_stops[idx].append((start_idx, end_idx))
    #        print(used_grafikons[grafikon_id], stop_name, odkud, kam, a, grafikons_capacity[grafikon_id])
   #      print(total_cost, cost, i)
         total_cost += cost
      if len(a) > 0:
         path_per_container[idx] = a
         grafikons_per_container2[idx] = grafikon_arr

   info_arrays = (grafikons_per_container2, containers_in_grafikon, grafikon_stops, num_containers_in_grafikon)
   print(total_cost)
   return 1, total_cost, info_arrays

if __name__=="__main__":
   cities = read_terminal_matrix()
   info_array, num_containers, df = read_container_dataset(cities)

   grafikon_stops  = [[] for _ in range(num_containers)]
   grafikons_per_container2 = [[] for _ in range(num_containers)]
   wagons_per_container = [[] for _ in range(num_containers)]
   path_per_container = [[] for _ in range(num_containers)]
   
   seznam_sousedu = {}
   for city in cities:
      seznam_sousedu[city] = []

   container_info = (info_array, wagons_per_container, path_per_container)
   grafikony_info = read_timetables(seznam_sousedu, cities)
   num_timetables = len(grafikony_info[0])
   timetables_total_weight = grafikony_info[6]
   wagon_allocation = grafikony_info[3]
   #for city in cities:
   #     seznam_sousedu[city] = sorted(seznam_sousedu[city], key=sort_by_third_element)
   valid_containers, total_cost, info_arrays = constructive_heuristic(container_info, grafikony_info,
                                                                      grafikon_stops, grafikons_per_container2)
   
   new_cost = compute_cost(num_timetables, wagon_allocation, grafikony_info, path_per_container, cities)
   
   print(new_cost)
   print(path_per_container[0])
   grafikons_per_container2, containers_in_grafikon = info_arrays[0], info_arrays[1]
   grafikon_stops = info_arrays[2]
   num_containers_in_grafikon = info_arrays[3]
   wagon_allocation = grafikony_info[3]
   immune_grafikons = np.zeros(num_timetables, dtype=bool)
   print(valid_containers, num_containers, total_cost)
   new_cost = 0
   x = 0
   validate_solution2(df, grafikony_info[4]['trains'], grafikons_per_container2, grafikon_stops, wagon_allocation)
   to_drop_mask = np.zeros(len(containers_in_grafikon), dtype=bool)
   y_array = []
   max_x = 0
   cum_sum = 0
   num_bins = 20

   hist1, bins1, _ = plt.hist(num_containers_in_grafikon, bins=num_bins, alpha=0.5, color='blue', label='Before')
   counter_containers = 0
   total_elapsed_time_replanning = 0
   total_elapsed_time_find_least_full_timetable = 0
   total_elapsed_time_before_replanning = 0
   total_elapsed_time_after_replanning = 0
   total_times = [0, 0, 0, 0, 0]
   for x in range(10000):
      start_time = time.time()
      min_idx, min_val = find_least_full_timetable(immune_grafikons, num_containers_in_grafikon, to_drop_mask)
    #  print(x, min_val, min_idx)
      end_time = time.time()
      total_elapsed_time_find_least_full_timetable += end_time - start_time
      start_time = time.time()
      if min_val == np.inf:
         max_x = x - 1
         break
      counter = 0
      if x % 100 == 0:
         print(x)
      containers_to_replan = copy.deepcopy(containers_in_grafikon[min_idx])
      previous = copy.deepcopy(containers_in_grafikon[min_idx])
      all_used_grafikons_by_container_to_replan = []
      counter_containers += len(containers_to_replan)
      for i in range(len(containers_to_replan)):
         curr_container = containers_to_replan[i]
         teu = info_array[curr_container][4]
         weight = info_array[curr_container][8]
         used_grafikons_by_container_to_replan = copy.deepcopy(grafikons_per_container2[curr_container])
         all_used_grafikons_by_container_to_replan.append(used_grafikons_by_container_to_replan)
         counter = 0
         for j, used in enumerate(used_grafikons_by_container_to_replan):
            curr_grafikon_stops_start = grafikon_stops[curr_container][j][0]
            curr_grafikon_stops_end = grafikon_stops[curr_container][j][1]
            for k in range(curr_grafikon_stops_start, curr_grafikon_stops_end + 1):
               curr_wagon = wagons_per_container[curr_container][counter]
               counter += 1
               for _ in range(teu):
           #       print(curr_container, grafikons_per_container2[curr_container], k, wagon_allocation[used][k][15], teu)
                  wagon_allocation[used][k][curr_wagon].remove(curr_container)
               timetables_total_weight[used][k] -= weight
            containers_in_grafikon[used].remove(curr_container)
            num_containers_in_grafikon[used] -= 1

      if len(containers_in_grafikon[min_idx]) > 0:
         print("xxxx")
         print(min_idx, containers_in_grafikon[min_idx], all_used_grafikons_by_container_to_replan, containers_to_replan)
         exit()
      to_drop_mask[min_idx] = True

      end_time = time.time()
      total_elapsed_time_before_replanning += end_time - start_time
      finished_correctly, total_times = replanning(info_array, 
                                                   seznam_sousedu, 
                                                   grafikony_info[4],
                                                   containers_to_replan, 
                                                   to_drop_mask,
                                                   containers_in_grafikon,
                                                   grafikons_per_container2,
                                                   grafikon_stops,
                                                   num_containers_in_grafikon,
                                                   wagon_allocation,
                                                   wagons_per_container,
                                                   total_times,
                                                   timetables_total_weight,
                                                   path_per_container)
      end_time = time.time()
      total_elapsed_time_replanning += end_time - start_time
      start_time = time.time()
      cum_sum += len(containers_to_replan)
      y_array.append(min_val)

      if not finished_correctly:
         for i in range(len(containers_to_replan)):
            curr_container = containers_to_replan[i]
            teu = info_array[curr_container][4]
            weight = info_array[curr_container][8]
            used_grafikons_by_container_to_replan = grafikons_per_container2[curr_container]
            counter = 0
            for j, used in enumerate(used_grafikons_by_container_to_replan):
               curr_grafikon_stops_start = grafikon_stops[curr_container][j][0]
               curr_grafikon_stops_end = grafikon_stops[curr_container][j][1]
               for k in range(curr_grafikon_stops_start, curr_grafikon_stops_end + 1):
                  curr_wagon = wagons_per_container[curr_container][counter]
                  counter += 1
                  for t in range(teu):
                     wagon_allocation[used][k][curr_wagon].append(curr_container)
                  if len(wagon_allocation[used][k][curr_wagon]) > 4:
                     print("ERROR: there are too many containers in wagon")
                     print(wagon_allocation[used][k], used, k, curr_wagon, info_array[info_array[11756][3]][2])
                     exit()
                  timetables_total_weight[used][k] += weight
               containers_in_grafikon[used].append(curr_container)
               num_containers_in_grafikon[used] += 1

         immune_grafikons[min_idx] = True
         containers_in_grafikon[min_idx] = previous
         num_containers_in_grafikon[min_idx] = len(previous)
         to_drop_mask[min_idx] = False
         end_time = time.time()
         total_elapsed_time_after_replanning += end_time - start_time

   print(counter_containers, total_elapsed_time_find_least_full_timetable, total_elapsed_time_before_replanning, 
         total_elapsed_time_replanning, total_elapsed_time_after_replanning, total_times)
   

   new_cost = compute_cost(num_timetables, wagon_allocation, grafikony_info, path_per_container, cities)
   
   validate_solution2(df, grafikony_info[4]['trains'], grafikons_per_container2, grafikon_stops, wagon_allocation)
   print(new_cost, x)
   exit()

   hist2, bins2, _ = plt.hist(num_containers_in_grafikon, bins=num_bins, alpha=0.5, color='red', label='After')

   plt.xlabel('Value')
   plt.ylabel('Frequency')
   plt.title('Histogram of grafikon distribution')
   plt.legend()

   plt.show()
   """
   x = np.linspace(0, max_x, max_x + 1)  
   plt.plot(x, y_array, color='green')
   plt.xlabel('Iteration')
   plt.ylabel('Timetables removed')
   plt.title('Removed timetables graph')
   plt.show()
   """
  #   _, used_grafikons, grafikons_per_container3, containers_in_grafikon = info_arrays2[0], info_arrays2[1], \
  #                                                                                        info_arrays2[2], info_arrays2[3]
 #     grafikon_stops = info_arrays2[4]
 #     num_containers_in_grafikon = info_arrays2[5]
  #    for i in range(len(containers_to_replan)):
  #       curr_container = containers_to_replan[i]
  #       grafikons_per_container2[curr_container] = grafikons_per_container3[curr_container]

   #   print(valid_containers2 != len(containers_to_replan), min_idx, containers_to_replan, all_used_grafikons_by_container_to_replan,
   #         containers_in_grafikon[75], containers_in_grafikon[3057], containers_in_grafikon[40], grafikons_per_container2[22151],
   #        grafikons_per_container3[22151], grafikon_stops[22151])
   #   exit()
  #    grafikons_capacity, used_grafikons, grafikons_per_container2, containers_in_grafikon = info_arrays[0], info_arrays[1], \
  #                                                                                        info_arrays[2], info_arrays[3]
  #    grafikon_stops = info_arrays[4]
      
   print(total_cost)
   #calculate the new cost
   new_cost = 0
   x = 0

 #  used_start_points_array, used_end_points_array = convert_paths_and_grafikons_to_end_points(path_per_container, 
  #                                                                                           grafikons_per_container2, 
  #                                                                                           data_grafikony['trains'])

   validate_solution2(df, data_grafikony['trains'], grafikons_per_container2, grafikon_stops, info_array, wagon_allocation, total_cost)
   exit()
   for j in range(3):
      for i in range(1000, len(df), 1000):
         sample_size = i
         random_sample = df.sample(n=sample_size)
         valid_containers, total_cost = constructive_heuristic(random_sample, j, True)
         invalid_containers = sample_size - valid_containers
         if j == 0:
            a_cost.append(total_cost)
            a_invalid.append(invalid_containers)
            valid_containers, total_cost = constructive_heuristic(random_sample, j, False)
            if total_cost != a_cost[-1] or (sample_size - valid_containers) != a_invalid[-1]:
               exit()
         elif j == 1:
            b_cost.append(total_cost)
            b_invalid.append(invalid_containers)
         else:
            c_cost.append(total_cost)
            c_invalid.append(invalid_containers)
      valid_containers, total_cost = constructive_heuristic(df, j)
      invalid_containers = len(df) - valid_containers
      if j == 0:
         a_cost.append(total_cost)
         a_invalid.append(invalid_containers)
      elif j == 1:
         b_cost.append(total_cost)
         b_invalid.append(invalid_containers)
      else:
         c_cost.append(total_cost)
         c_invalid.append(invalid_containers)
   visualize_three_posibilities(len(df), a_cost, b_cost, c_cost)
   visualize_three_posibilities(len(df), a_invalid, b_invalid, c_invalid)
