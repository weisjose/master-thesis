
letter_to_number = {'a': 1, 'b':2, 'c':3, 'd':4, 'e':5, 'f':6, 'g':7, 
                        'h': 8, 'i':9, 'j':10, 'k':11, 'l':12, 'm':13, 'n':14, 
                        'o': 15, 'p':16, 'q':17, 'r':18, 's':19, 't':20, 'u':21, 
                        'v': 22, 'w':23, 'x':24, 'y':25, 'z':26, ' ':31}

def compute_hash(s):
   result = 0
   b = 32
   c = 11
   for i in range(len(s)): 
      a = s[i]
      k = i
      result += (a * mod_exp(b, 1, k, c)) % c
   return result % c

def mod_exp(a, b, k, c):
    result = 1
    a %= c  
    while k > 0:
        if k % 2 == 1:
            result = (result * a) % c
        k //= 2
        a = (a * a) % c
    return result

a = 3
b = 5
k = 4
c = 7
s = [3, 1, 18, 15, 4, 5, 10] #aa
s = "sel deda s babickou na hriby"
s_numbers = []
for i in range(len(s)):
   s_numbers.append(letter_to_number[s[i]])
print(s, s_numbers)
res = compute_hash(s_numbers)

print("Result:", res)