import pandas as pd
import numpy as np
import json
from datetime import datetime
import copy
from collections import deque
from queue import PriorityQueue
import sys
import os

current_path = os.getcwd()
directory_path = os.path.dirname(current_path)

sys.path.append(directory_path)

from utilities.validator import validate_solution
from utilities.path_finder import find_path
from utilities.constants import DAY

from global_variables import path_to_terminals

def parse_date(date_str):
   formats_to_try = ["%Y-%m-%d %H:%M:%S", "%Y-%m-%d", "%H:%M:%S"]
   
   for fmt in formats_to_try:
      try:
         parsed_date = datetime.strptime(date_str, fmt)
         return parsed_date
      except ValueError:
         pass

   raise ValueError("Date string does not match any expected format")

def sort_by_time_difference(item):
    return item[3] - item[2]

def sort_by_terminal_then_time_difference(item, terminal_order):
    return terminal_order[item[6]] * 10000 + (item[3] - item[2])

def sort_by_third_element(item):
   return item[2]

def convert_paths_and_grafikons_to_end_points(path_per_container, grafikons_per_container, grafikon_data):
   used_start_points_array = []
   used_end_points_array = []
   for x in range(len(grafikons_per_container)):
      curr_used_end_points_array = []
      curr_used_start_points_array = []
      if len(grafikons_per_container[x]) == 0:
         used_start_points_array.append(curr_used_start_points_array)
         used_end_points_array.append(curr_used_end_points_array)
         continue
      for i in range(len(grafikons_per_container[x])):
         curr_grafikon = grafikons_per_container[x][i]
         start = path_per_container[x][i]
         end = path_per_container[x][i + 1]
         start_point_idx = -1
         end_point_idx = -1
         for j in range(len(grafikon_data[curr_grafikon]['hub_route'])):
            if grafikon_data[curr_grafikon]['hub_route'][j]["stop_code"] == start:
               start_point_idx = j
            elif grafikon_data[curr_grafikon]['hub_route'][j]["stop_code"] == end:
               end_point_idx = j
         if start_point_idx == -1 or end_point_idx == -1:
            print("ERROR: error in convertion")
            print(start_point_idx, end_point_idx, len(grafikon_data[curr_grafikon]['hub_route']), start, end,
                  path_per_container[x], grafikons_per_container[x], x)
            exit()
         curr_used_start_points_array.append(start_point_idx)
         curr_used_end_points_array.append(end_point_idx)
      used_start_points_array.append(curr_used_start_points_array)
      used_end_points_array.append(curr_used_end_points_array)
   return used_start_points_array, used_end_points_array

def compute_cost(num_timetables, wagon_allogation, grafikony_info, path_per_container, cities):
   cost = 0
   terminal_to_cost = {}
   terminal_start = {}
   terminal_end = {}
   df = pd.read_excel(path_to_terminals)
   for city in cities:
      terminal_start[city] = 0
      terminal_end[city] = 0
   for index, row in df.iterrows():
      city = row['ODKUD']
      terminal_to_cost[city] = 30 if row['VLASTNI'] == 'Y' else 50
   for i in range(num_timetables):
      curr_used = wagon_allogation[i]
      actually_used = False
      
      for j in range(len(curr_used)):
         wagon_count = 0
         for k in range(27):
            if len(curr_used[j][k]) >= 1:
               actually_used = True
               wagon_count += 1
         cost += int((grafikony_info[4]['trains'][i]['hub_route'][j + 1]['curr_total_minutes'] / DAY) * 20 * wagon_count)
      if actually_used:
         cost += grafikony_info[4]['trains'][i]['cost']
         terminal_start[grafikony_info[4]['trains'][i]['hub_route'][0]['stop_code']] += 1
         terminal_end[grafikony_info[4]['trains'][i]['hub_route'][-1]['stop_code']] += 1

   for city in cities:
      start = terminal_start[city]
      end = terminal_end[city]
    #  print(city, start, end)
      cost += abs(start - end) * 2000
      
   for path in path_per_container:
      for stop in path:
         cost += terminal_to_cost[stop]
   
   return cost