import numpy as np
from queue import PriorityQueue
import copy

HOUR = 60
DAY = 24 * HOUR
MAX_WEIGHT = 1200000

#standartni prohledavani, neuvazuje kapacitu grafikonu, best_so_far ma v sobe nejdrivejsi deadline
def find_path(seznam_sousedu, start, end, release_date, deadline, possible_cesty):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], []))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY
    if start + end not in possible_cesty:
       return paths, grafikon_arr
    while not queue.empty():
        priority, city_release_date, current, path, grafikons = queue.get()
        curr_city_end = current + end
       # print(queue.qsize(), start, end, path, curr_city_end)
        #zjistime zda nebylo predtim neco lepsi [start, current]
        if best_so_far[current] < city_release_date:
           continue
        if current == end:
            paths = []
            grafikon_arr = []
            paths.append(path)
            grafikon_arr = grafikons
         #   return path, grafikon_arr
   #     elif len(path) >= 6:
   #         continue
        else:
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               if neighbor not in possible_cesty[curr_city_end]:
                  continue
               if neighbor not in path:
                     if neighbor in best_so_far:
                        if best_so_far[neighbor] < n_deadline:
                           continue
                        else:
                           best_so_far[neighbor] = n_deadline
                     else:
                        best_so_far[neighbor] = n_deadline
                     new_path = path + [neighbor]
                     new_grafikons = grafikons + [grafikon_idx]
                     new_priority = n_deadline  # Priority based on neighbor's release date
                     queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons))
    return paths, grafikon_arr

#lepsi superdict
def find_path2(seznam_sousedu, start, end, release_date, deadline, super_dict):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], []))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY
    while not queue.empty():
        priority, city_release_date, current, path, grafikons, code_string = queue.get()
        curr_city_end = current + end
       # print(queue.qsize(), start, end, path, curr_city_end)
        #zjistime zda nebylo predtim neco lepsi [start, current]
        if best_so_far[current] < city_release_date:
           continue
        if current == end:
            paths.append(path)
            grafikon_arr = grafikons
            return path, grafikon_arr
   #     elif len(path) >= 6:
   #         continue
        else:
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               new_code_string = code_string + neighbor
               code_string_to_check = code_string + end
          #     print(code_string_to_check, start, current, neighbor, end)
               if code_string_to_check not in super_dict:
                  if neighbor != end:
                     continue
               elif neighbor not in super_dict[code_string_to_check]:
                  continue
               if neighbor not in path:
                     if neighbor in best_so_far:
                        if best_so_far[neighbor] < n_deadline:
                           continue
                        else:
                           best_so_far[neighbor] = n_deadline
                     else:
                        best_so_far[neighbor] = n_deadline
                     new_path = path + [neighbor]
                     new_grafikons = grafikons + [grafikon_idx]
                     new_priority = n_deadline  # Priority based on neighbor's release date
                     queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons, new_code_string))
    return paths, grafikon_arr

#cut time, zatim nepouzivame, nefunguje dobre
def find_path6(seznam_sousedu, start, end, release_date, deadline, possible_cesty, 
               used_grafikons, data_graf, grafikons_capacity, cut_time):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY
    best_so_far_cost = {}
    best_so_far_cost[start] = 0
    while not queue.empty():
        priority, city_release_date, current, path, grafikons, curr_cost = queue.get()
        curr_city_end = current + end
        this_day = str(int((city_release_date // DAY) % 7))
        if best_so_far[current] < city_release_date:
           continue
        if current == end:
            paths.append(path)
            grafikon_arr = grafikons
            return path, grafikon_arr
        elif cut_time[curr_city_end + this_day] * DAY >= (deadline + 1) * DAY - city_release_date:
           continue
   #     elif len(path) >= 6:
   #         continue
        else:
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               if neighbor not in possible_cesty[curr_city_end]:
                  continue
               if used_grafikons[grafikon_idx] + 1 > grafikons_capacity[grafikon_idx]:
                  continue
               if neighbor not in path:
                  cost = curr_cost
                  if used_grafikons[grafikon_idx] == 0:
                     cost += data_graf[grafikon_idx]['cost']
                  if neighbor in best_so_far:
                     if best_so_far[neighbor] > n_deadline:
                        best_so_far[neighbor] = n_deadline
                        best_so_far_cost[neighbor] = cost
                     else:
                        continue
                  else:
                     best_so_far[neighbor] = n_deadline
                     best_so_far_cost[neighbor] = cost
                  new_path = path + [neighbor]
                  new_grafikons = grafikons + [grafikon_idx]
                  new_priority = cost
                  queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons, cost))
    return paths, grafikon_arr

#kapacita napric stopy, possible_cesty nejspis spatne
def find_path20(seznam_sousedu, start, end, release_date, deadline, possible_cesty, 
               used_grafikons, data_graf, grafikons_capacity):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY
    best_so_far_cost = {}
    best_so_far_cost[start] = 0
    while not queue.empty():
        priority, city_release_date, current, path, grafikons, curr_cost = queue.get()
        curr_city_end = current + end
        if best_so_far[current] < city_release_date:
           continue
        if current == end:
            paths.append(path)
            grafikon_arr = grafikons
         #   print(counter1, counter3, counter_2_max)
            return path, grafikon_arr
   #     elif len(path) >= 6:
   #         continue
        else:
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               if neighbor not in possible_cesty[curr_city_end]:
                  continue
               found_start = False
               over_capacity = False
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start and used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]:
                     over_capacity = True
                     break
                  if stop_name == neighbor:
                     break

               if over_capacity:
                  continue
               if neighbor not in path:
                  cost = curr_cost
                  if used_grafikons[grafikon_idx] == 0:
                     cost += data_graf[grafikon_idx]['cost']
                  if neighbor in best_so_far:
                     if best_so_far[neighbor] > n_deadline:
                        best_so_far[neighbor] = n_deadline
                        best_so_far_cost[neighbor] = cost
                     else:
                        continue
                  else:
                     best_so_far[neighbor] = n_deadline
                     best_so_far_cost[neighbor] = cost
                  new_path = path + [neighbor]
                  new_grafikons = grafikons + [grafikon_idx]
                  new_priority = cost
                  queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons, cost))
    return paths, grafikon_arr

#snaha o pareto
def find_path40(seznam_sousedu, start, end, release_date, deadline, possible_cesty, 
               used_grafikons, data_graf, grafikons_capacity):
   queue = PriorityQueue()
   queue.put((0, release_date * DAY, start, [start], [], 0))  # Priority, city_release_date, current_city, path
   paths = []
   grafikon_arr = []
   best_so_far = {}
   best_so_far[start] = [release_date * DAY]
   best_so_far_cost = {}
   best_so_far_cost[start] = [0]
   while not queue.empty():
      priority, city_release_date, current, path, grafikons, curr_cost = queue.get()
      curr_city_end = current + end
      #start pareto
      index = 0
      valid = True
      while index < len(best_so_far[current]):
         if best_so_far[current][index] > city_release_date and best_so_far_cost[current][index] > curr_cost:
            best_so_far[current].pop(index)
            best_so_far_cost[current].pop(index)
         elif best_so_far[current][index] < city_release_date and best_so_far_cost[current][index] < curr_cost:
            valid = False
            break
         else:
            index += 1
      if not valid:
         continue
      #end pareto
      if current == end:
         paths.append(path)
         grafikon_arr = grafikons
         return path, grafikon_arr
#     elif len(path) >= 6:
#         continue
      else:
         #chceme jenom ten nejlepsi - [start, ]
         for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
            if neighbor_release_date < city_release_date:
               continue
            if deadline * DAY < n_deadline:
               continue
            if neighbor not in possible_cesty[curr_city_end]:
               continue
            if neighbor in path:
               continue
            found_start = False
            over_capacity = False
            for j in range(len(data_graf[grafikon_idx]["hub_route"])):
               stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
               if stop_name == current:
                  found_start = True
                  continue
               if found_start and used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]:
                  over_capacity = True
                  break
               if stop_name == neighbor:
                  break

            if over_capacity:
               continue

            cost = curr_cost
            if used_grafikons[grafikon_idx] == 0:
               cost += data_graf[grafikon_idx]['cost']
            if neighbor in best_so_far:
               valid = True
               index = 0
               while index < len(best_so_far[neighbor]):
                  if best_so_far[neighbor][index] > neighbor_release_date and best_so_far_cost[neighbor][index] > cost:
                     best_so_far[neighbor].pop(index)
                     best_so_far_cost[neighbor].pop(index)
                  elif best_so_far[neighbor][index] < neighbor_release_date and best_so_far_cost[neighbor][index] < cost:
                     valid = False
                     break
                  else:
                     index += 1
               if not valid:
                  continue
               else:
                  best_so_far[neighbor].append(n_deadline)
                  best_so_far_cost[neighbor].append(cost)
            else:
               best_so_far[neighbor] = [n_deadline]
               best_so_far_cost[neighbor] = [cost]
            new_path = path + [neighbor]
            new_grafikons = grafikons + [grafikon_idx]
            new_priority = cost
            queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons, cost))
            
   return paths, grafikon_arr

#kapacita napric stopy, super_dict dobre, 
def find_path200(seznam_sousedu, start, end, release_date, deadline, super_dict, 
               used_grafikons, data_graf, grafikons_capacity, do_print = False):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0, start))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY

    while not queue.empty():
        _, city_release_date, current, path, grafikons, curr_cost, code_string = queue.get()
     #   curr_city_end = current + end
        if best_so_far[current] < city_release_date:
           continue
        #if current == end:
        #    grafikon_arr = grafikons
        #    return path, grafikon_arr
        elif len(path) >= 5:
            continue
        else:
    #        counter3 += 1
    #        counter2 = 0
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
          #     if do_print:
          #        print(path, neighbor, grafikon_idx, neighbor_release_date, n_deadline, city_release_date, "")
          #        print(path, neighbor, grafikon_idx, neighbor_release_date, n_deadline, city_release_date, deadline, "")
               #if grafikon_idx == 1886:
               #   print(neighbor_release_date, city_release_date, deadline * DAY, n_deadline)
 #              if do_print and n_deadline == 34047:
  #                print("not okkk")
                  #continue
               if neighbor_release_date < city_release_date:
   #               if do_print and n_deadline == 34047:
    #                 print("not okkk")
                  continue
               if deadline * DAY < n_deadline:
     #             if do_print and n_deadline == 34047:
      #               print("not okkk")
                  continue
            #   new_code_string = code_string + neighbor
            #   code_string_to_check = code_string + end
            #   print(code_string_to_check, start, current, neighbor, end)
          #     if code_string_to_check not in super_dict:
          #        if neighbor != end:
              #       if do_print and n_deadline == 1311:
             #           print("not okkkk")
           #          continue
            #   elif neighbor not in super_dict[code_string_to_check]:
               #   if do_print and n_deadline == 1311:
                #     print("not okkkkk")
            #      continue
              # if do_print and n_deadline == 1311:
               #   print("ok")
               found_start = False
               over_capacity = False
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start and used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]:
                     over_capacity = True
                     break
                  if stop_name == neighbor:
                     break

               if over_capacity:
                  if do_print and n_deadline == 34047:
                     print("okk",  grafikons_capacity[198], used_grafikons[198])
                  continue
               elif neighbor not in path:
             #     if do_print and n_deadline == 34047:
             #        print("ok")
                  cost = curr_cost
                  if used_grafikons[grafikon_idx] == 0:
                     cost += data_graf[grafikon_idx]['cost']
                  if neighbor in best_so_far:
                     if best_so_far[neighbor] > n_deadline + len(path) / 10:
                        best_so_far[neighbor] = n_deadline + len(path) / 10
                     else:
                      #  if do_print and n_deadline == 34047:
                      #     print("nok")
                        continue
                  else:
                     best_so_far[neighbor] = n_deadline + len(path) / 10
                  #   best_so_far_cost[neighbor] = cost
                  new_path = path + [neighbor]
                  new_grafikons = grafikons + [grafikon_idx]
                  if neighbor == end:
                     return new_path, new_grafikons
                  queue.put((n_deadline + len(path) / 10, n_deadline, neighbor, new_path, new_grafikons, cost, ""))
    return paths, grafikon_arr

def find_path204(seznam_sousedu, start, end, release_date, deadline, 
                 data_graf, wagon_allocation, teu, to_drop, weight, timetables_total_weight):
    
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], []))  # Priority, city_release_date, current_city, path
    best_so_far = {}
    best_so_far[start] = release_date * DAY

    while not queue.empty():
        _, city_release_date, current, path, grafikons = queue.get()
     #   curr_city_end = current + end
        if best_so_far[current] < city_release_date:
           continue
        else:
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if (to_drop[grafikon_idx] == True):
                  continue
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               if neighbor in path:
                  continue
               if neighbor in best_so_far:
                  if best_so_far[neighbor] <= n_deadline + len(path) / 10:
                     continue

               found_start = False
               over_capacity = False
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start:
                     curr_over_capacity = True
                     for k in range(27):
                        if len(wagon_allocation[grafikon_idx][j - 1][k]) < 5 - teu:
                           curr_over_capacity = False
                           break
                     if curr_over_capacity or timetables_total_weight[grafikon_idx][j - 1] + weight > MAX_WEIGHT:
                        over_capacity = True
                        break
                  if stop_name == neighbor:
                     break

               if over_capacity:
                  continue
               
               best_so_far[neighbor] = n_deadline + len(path) / 10
               new_path = path + [neighbor]
               new_grafikons = grafikons + [grafikon_idx]
               if neighbor == end:
                  return new_path, new_grafikons
               queue.put((n_deadline + len(path) / 10, n_deadline, neighbor, new_path, new_grafikons))
    return [], []

def find_path205(seznam_sousedu, start, end, release_date, deadline, 
                 data_graf, wagon_allocation, teu, weight, timetables_total_weight):
    
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], []))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY

    while not queue.empty():
        _, city_release_date, current, path, grafikons = queue.get()
        if best_so_far[current] < city_release_date:
           continue
        else:
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               if neighbor in path:
                  continue
               if neighbor in best_so_far:
                  if best_so_far[neighbor] <= n_deadline + len(path) / 10:
                     continue
               
               found_start = False
               over_capacity = False
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start:
                     curr_over_capacity = True
                     for k in range(27):
                        if len(wagon_allocation[grafikon_idx][j - 1][k]) < 5 - teu:
                           curr_over_capacity = False
                           break
                     if curr_over_capacity or timetables_total_weight[grafikon_idx][j - 1] + weight > MAX_WEIGHT:
                        over_capacity = True
                        break
                  if stop_name == neighbor:
                     break

               if over_capacity:
                  continue
               
               best_so_far[neighbor] = n_deadline + len(path) / 10
               new_path = path + [neighbor]
               new_grafikons = grafikons + [grafikon_idx]
               if neighbor == end:
                  return new_path, new_grafikons
               queue.put((n_deadline + len(path) / 10, n_deadline, neighbor, new_path, new_grafikons))
    return paths, grafikon_arr

def find_path206(seznam_sousedu, start, end, release_date, deadline, super_dict, 
               used_grafikons, data_graf, grafikons_capacity, wagon_allocation, teu):
    
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0, start))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY

    while not queue.empty():
        _, city_release_date, current, path, grafikons, curr_cost, code_string = queue.get()
     #   curr_city_end = current + end
        if best_so_far[current] < city_release_date:
           continue
        #if current == end:
        #    grafikon_arr = grafikons
        #    return path, grafikon_arr
        elif len(path) >= 5:
            continue
        else:
    #        counter3 += 1
    #        counter2 = 0
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
          #     if do_print:
          #        print(path, neighbor, grafikon_idx, neighbor_release_date, n_deadline, city_release_date, "")
          #        print(path, neighbor, grafikon_idx, neighbor_release_date, n_deadline, city_release_date, deadline, "")
               #if grafikon_idx == 1886:
               #   print(neighbor_release_date, city_release_date, deadline * DAY, n_deadline)
 #              if do_print and n_deadline == 34047:
  #                print("not okkk")
                  #continue
               if neighbor_release_date < city_release_date:
   #               if do_print and n_deadline == 34047:
    #                 print("not okkk")
                  continue
               if deadline * DAY < n_deadline:
     #             if do_print and n_deadline == 34047:
      #               print("not okkk")
                  continue
               if neighbor in path:
                  continue
               neighbor_in_best_so_far = False
               if neighbor in best_so_far:
                  if best_so_far[neighbor] <= n_deadline + len(path) / 10:
                     continue
                  else:
                     neighbor_release_date = True
            #   new_code_string = code_string + neighbor
            #   code_string_to_check = code_string + end
            #   print(code_string_to_check, start, current, neighbor, end)
          #     if code_string_to_check not in super_dict:
          #        if neighbor != end:
              #       if do_print and n_deadline == 1311:
             #           print("not okkkk")
           #          continue
            #   elif neighbor not in super_dict[code_string_to_check]:
               #   if do_print and n_deadline == 1311:
                #     print("not okkkkk")
            #      continue
              # if do_print and n_deadline == 1311:
               #   print("ok")
               found_start = False
               over_capacity = False
               """
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start and used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]:
                     over_capacity = True
                     break
                  if stop_name == neighbor:
                     break

               if over_capacity:
                  continue
               """
               over_capacity = False
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
              #    for k in range(len()):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start:
                     curr_over_capacity = True
                     count = 0
                     for k in range(27):
               #         print(wagon_allocation[grafikon_idx], j)
              #          print(wagon_allocation[grafikon_idx][j])
                        if len(wagon_allocation[grafikon_idx][j - 1][k]) == 3:
                           count += 1
                           if count > 1:
                              curr_over_capacity = False
                              break
                        elif len(wagon_allocation[grafikon_idx][j - 1][k]) < 3:
                           curr_over_capacity = False
                           break
                     if curr_over_capacity:
                        over_capacity = True
                        break
                  if stop_name == neighbor:
                     break

               if over_capacity:
              #    print(wagon_allocation[grafikon_idx])
              #    print(start, end, release_date, deadline, grafikon_idx)
                  continue
               
               best_so_far[neighbor] = n_deadline + len(path) / 10
               #   best_so_far_cost[neighbor] = cost
               new_path = path + [neighbor]
               new_grafikons = grafikons + [grafikon_idx]
               if neighbor == end:
                  return new_path, new_grafikons
               queue.put((n_deadline + len(path) / 10, n_deadline, neighbor, new_path, new_grafikons, 0, ""))
    return paths, grafikon_arr


def find_path210(seznam_sousedu, start, end, release_date, deadline, super_dict, 
               used_grafikons, data_graf, do_print = False):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0, start))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY

    while not queue.empty():
        _, city_release_date, current, path, grafikons, curr_cost, code_string = queue.get()
     #   curr_city_end = current + end
        if best_so_far[current] < city_release_date:
           continue
        elif len(path) >= 5:
            continue
        else:
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               if neighbor not in path:
                  if neighbor in best_so_far:
                     if best_so_far[neighbor] < n_deadline:
                           continue
                     else:
                        best_so_far[neighbor] = n_deadline
                      #  if do_print and n_deadline == 34047:
                      #     print("nok")
                        continue
                  else:
                     best_so_far[neighbor] = n_deadline #+ len(path) / 10
                  #   best_so_far_cost[neighbor] = cost
                  new_path = path + [neighbor]
                  new_grafikons = grafikons + [grafikon_idx]
                  if neighbor == end:
                     return new_path, new_grafikons
                  queue.put((n_deadline, n_deadline, neighbor, new_path, new_grafikons, 0, ""))
    return paths, grafikon_arr

def find_path222(seznam_sousedu, start, end, release_date, deadline, super_dict):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], start))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY

    while not queue.empty():
        _, city_release_date, current, path, grafikons, code_string = queue.get()
        if current == 'BLAVA':
           print(current, path, grafikons, city_release_date)
        if best_so_far[current] < city_release_date:
           continue
        elif len(path) >= 5:
           continue
   #     elif len(path) >= 6:
   #         continue
        else:
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if current == 'BLAVA' and grafikon_idx == 2863:
                     print(neighbor, grafikon_idx, neighbor_release_date, city_release_date, n_deadline, deadline * DAY)
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               if neighbor not in path:
                     if neighbor in best_so_far:
                        if best_so_far[neighbor] < n_deadline:
                           continue
                        else:
                           best_so_far[neighbor] = n_deadline
                     else:
                        best_so_far[neighbor] = n_deadline
                     new_path = path + [neighbor]
                     new_grafikons = grafikons + [grafikon_idx]
                     new_priority = n_deadline  # Priority based on neighbor's release date
                     if neighbor == end:
                        return new_path, new_grafikons
                     queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons, ""))
    return paths, grafikon_arr
#kapacita napric stopy, super_dict dobre, 
def find_path2000(seznam_sousedu, start, end, release_date, deadline, super_dict, 
               used_grafikons, data_graf, grafikons_capacity, seznam_sousedu_per_container, do_print = False):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0, start))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY
    best_so_far_cost = {}
    best_so_far_cost[start] = 0
    counter1 = 0
    counter3 = 0
    counter_2_max = 0
    while not queue.empty():
        priority, city_release_date, current, path, grafikons, curr_cost, code_string = queue.get()
        curr_city_end = current + end
        counter1 += 1
   #     if queue.qsize() > 50:
   #      print(start, end, queue.qsize())
        if best_so_far[current] < city_release_date:
           continue
        if current == end:
            paths = path
            grafikon_arr = grafikons
            continue
        if len(path) >= 5:
            continue
        else:
            counter3 += 1
            counter2 = 0
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if do_print:
                  print(path, neighbor, grafikon_idx, neighbor_release_date, n_deadline, city_release_date, "")
          #        print(path, neighbor, grafikon_idx, neighbor_release_date, n_deadline, city_release_date, deadline, "")
               counter2 += 1
               #if grafikon_idx == 1886:
               #   print(neighbor_release_date, city_release_date, deadline * DAY, n_deadline)
               if do_print and n_deadline == 34047:
                  print("not okkk")
                  #continue
               if neighbor_release_date < city_release_date:
                  if do_print and n_deadline == 34047:
                     print("not okkk")
                  continue
                  continue
               if deadline * DAY < n_deadline:
                  if do_print and n_deadline == 34047:
                     print("not okkk")
                  continue
               new_code_string = code_string + neighbor
               code_string_to_check = code_string + end
            #   print(code_string_to_check, start, current, neighbor, end)
          #     if code_string_to_check not in super_dict:
          #        if neighbor != end:
              #       if do_print and n_deadline == 1311:
             #           print("not okkkk")
           #          continue
            #   elif neighbor not in super_dict[code_string_to_check]:
               #   if do_print and n_deadline == 1311:
                #     print("not okkkkk")
            #      continue
              # if do_print and n_deadline == 1311:
               #   print("ok")
               found_start = False
               over_capacity = False
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start and used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]:
                     over_capacity = True
                     break
                  if stop_name == neighbor:
                     break

               if over_capacity:
                  if do_print and n_deadline == 34047:
                     print("okk",  grafikons_capacity[198], used_grafikons[198])
                  continue
               if neighbor not in path:
                  if do_print and n_deadline == 34047:
                     print("ok")
                  cost = curr_cost
                  if used_grafikons[grafikon_idx] == 0:
                     cost += data_graf[grafikon_idx]['cost']
                  if neighbor in best_so_far:
                     if best_so_far[neighbor] > n_deadline + len(path) / 10:
                        best_so_far[neighbor] = n_deadline + len(path) / 10
                        best_so_far_cost[neighbor] = cost
                     else:
                        if do_print and n_deadline == 34047:
                           print("nok")
                        continue
                  else:
                     best_so_far[neighbor] = n_deadline + len(path) / 10
                     best_so_far_cost[neighbor] = cost
                  new_path = path + [neighbor]
                  new_grafikons = grafikons + [grafikon_idx]
                  new_priority = cost
                  if do_print and n_deadline == 34047:
                     print("ok")
                  queue.put((n_deadline + len(path) / 10, n_deadline, neighbor, new_path, new_grafikons, cost, new_code_string))
               counter_2_max = max(counter_2_max, counter2)
    return paths, grafikon_arr

#jako find_path40, ale neuvazuje kapacitu 
def find_path400(seznam_sousedu, start, end, release_date, deadline, super_dict, 
               used_grafikons, data_graf, grafikons_capacity, do_print = False):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0, start))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY
    best_so_far_cost = {}
    best_so_far_cost[start] = 0
    counter1 = 0
    counter3 = 0
    counter_2_max = 0
    counter_all = 0
    counter_removed = 0
    counter_through = 0
    while not queue.empty():
        priority, city_release_date, current, path, grafikons, curr_cost, code_string = queue.get()
        curr_city_end = current + end
        counter1 += 1
   #     if queue.qsize() > 50:
   #      print(start, end, queue.qsize())
        if best_so_far[current] < city_release_date:
           continue
        if current == end:
            paths.append(path)
            grafikon_arr = grafikons
           # print(counter_all, counter_removed, counter_through)
            return path, grafikon_arr
        if len(path) >= 5:
            continue
        else:
            counter3 += 1
            counter2 = 0
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               counter2 += 1
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               counter_all += 1
               new_code_string = code_string + neighbor
               code_string_to_check = code_string + end
          #     print(code_string_to_check, start, current, neighbor, end)
               if code_string_to_check not in super_dict:
                  if neighbor != end:
                     counter_removed += 1
                     if neighbor in path:
                        counter_through += 1
                     continue
               elif neighbor not in super_dict[code_string_to_check]:
                  counter_removed += 1
                  if neighbor in path:
                     counter_through += 1
                  continue

               if neighbor in path:
                  continue
               found_start = False
               over_capacity = False
               """
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start and used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]:
                     over_capacity = True
                     break
                  if stop_name == neighbor:
                     break
               """
               if over_capacity:
                  continue
               cost = curr_cost
               if used_grafikons[grafikon_idx] == 0:
                  cost += data_graf[grafikon_idx]['cost']
               if neighbor in best_so_far:
                  if best_so_far[neighbor] > n_deadline:
                     best_so_far[neighbor] = n_deadline
                     best_so_far_cost[neighbor] = cost
                  else:
                     continue
               else:
                  best_so_far[neighbor] = n_deadline
                  best_so_far_cost[neighbor] = cost
               new_path = path + [neighbor]
               new_grafikons = grafikons + [grafikon_idx]
               new_priority = cost
               queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons, cost, new_code_string))
               counter_2_max = max(counter_2_max, counter2)
    return paths, grafikon_arr

#pro zjisteni vsech moznych cest (neni kapacita a nezajima nas best so far)
def find_path20000(seznam_sousedu, start, end, release_date, deadline, super_dict, 
               used_grafikons, data_graf, grafikons_capacity, do_print = False):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0, start))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY
    best_so_far_cost = {}
    best_so_far_cost[start] = 0
    counter1 = 0
    counter3 = 0
    counter_2_max = 0
    counter_all = 0
    counter_removed = 0
    counter_through = 0
    while not queue.empty():
        priority, city_release_date, current, path, grafikons, curr_cost, code_string = queue.get()
        curr_city_end = current + end
        counter1 += 1
   #     if queue.qsize() > 50:
   #      print(start, end, queue.qsize())
        if current == end:
            paths.append(path)
            grafikon_arr = grafikons
           # print(counter_all, counter_removed, counter_through)
        if len(path) >= 5:
            continue
        else:
            counter3 += 1
            counter2 = 0
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               counter2 += 1
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               counter_all += 1
               new_code_string = code_string + neighbor
               code_string_to_check = code_string + end
          #     print(code_string_to_check, start, current, neighbor, end)
               if code_string_to_check not in super_dict:
                  if neighbor != end:
                     counter_removed += 1
                     if neighbor in path:
                        counter_through += 1
                     continue
               elif neighbor not in super_dict[code_string_to_check]:
                  counter_removed += 1
                  if neighbor in path:
                     counter_through += 1
                  continue

               if neighbor in path:
                  continue
               found_start = False
               over_capacity = False
               """
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start and used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]:
                     over_capacity = True
                     break
                  if stop_name == neighbor:
                     break
               """
               if over_capacity:
                  continue
               cost = curr_cost
               if used_grafikons[grafikon_idx] == 0:
                  cost += data_graf[grafikon_idx]['cost']
               """
               if neighbor in best_so_far:
                  if best_so_far[neighbor] > n_deadline:
                     best_so_far[neighbor] = n_deadline
                     best_so_far_cost[neighbor] = cost
                  else:
                     continue
               else:
                  best_so_far[neighbor] = n_deadline
                  best_so_far_cost[neighbor] = cost
               """
               new_path = path + [neighbor]
               new_grafikons = grafikons + [grafikon_idx]
               new_priority = cost
               queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons, cost, new_code_string))
               counter_2_max = max(counter_2_max, counter2)
    return paths, grafikon_arr


def find_path201(seznam_sousedu, start, end, release_date, deadline, super_dict, 
               used_grafikons, data_graf, grafikons_capacity, to_drop):
    queue = PriorityQueue()
    queue.put((0, release_date * DAY, start, [start], [], 0, start))  # Priority, city_release_date, current_city, path
    paths = []
    grafikon_arr = []
    best_so_far = {}
    best_so_far[start] = release_date * DAY
    best_so_far_cost = {}
    best_so_far_cost[start] = 0
    counter1 = 0
    counter3 = 0
    counter_2_max = 0
    while not queue.empty():
        priority, city_release_date, current, path, grafikons, curr_cost, code_string = queue.get()
        curr_city_end = current + end
        counter1 += 1
   #     if queue.qsize() > 50:
   #      print(start, end, queue.qsize())
        if best_so_far[current] < city_release_date:
           continue
        if current == end:
            paths.append(path)
            grafikon_arr = grafikons
      #      print(counter1, counter3, counter_2_max)
            return path, grafikon_arr
        if len(path) >= 5:
            continue
        else:
            counter3 += 1
            counter2 = 0
            #chceme jenom ten nejlepsi - [start, ]
            for neighbor, grafikon_idx, neighbor_release_date, n_deadline in seznam_sousedu.get(current, {}):
               if (to_drop[grafikon_idx] == True):
                  continue
               counter2 += 1
               if neighbor_release_date < city_release_date:
                  continue
               if deadline * DAY < n_deadline:
                  continue
               new_code_string = code_string + neighbor
               code_string_to_check = code_string + end
          #     print(code_string_to_check, start, current, neighbor, end)
               if code_string_to_check not in super_dict:
                  if neighbor != end:
                     continue
               elif neighbor not in super_dict[code_string_to_check]:
                  continue

               found_start = False
               over_capacity = False
               for j in range(len(data_graf[grafikon_idx]["hub_route"])):
                  stop_name = data_graf[grafikon_idx]["hub_route"][j]["stop_code"]
                  if stop_name == current:
                     found_start = True
                     continue
                  if found_start and used_grafikons[grafikon_idx][j - 1] + 1 > grafikons_capacity[grafikon_idx]:
                     over_capacity = True
                     break
                  if stop_name == neighbor:
                     break

               if over_capacity:
                  continue
               if neighbor not in path:
                  cost = curr_cost
                  if used_grafikons[grafikon_idx] == 0:
                     cost += data_graf[grafikon_idx]['cost']
                  if neighbor in best_so_far:
                     if best_so_far[neighbor] > n_deadline:
                        best_so_far[neighbor] = n_deadline
                        best_so_far_cost[neighbor] = cost
                     else:
                        continue
                  else:
                     best_so_far[neighbor] = n_deadline
                     best_so_far_cost[neighbor] = cost
                  new_path = path + [neighbor]
                  new_grafikons = grafikons + [grafikon_idx]
                  new_priority = cost
                  queue.put((new_priority, n_deadline, neighbor, new_path, new_grafikons, cost, new_code_string))
               counter_2_max = max(counter_2_max, counter2)
    return paths, grafikon_arr