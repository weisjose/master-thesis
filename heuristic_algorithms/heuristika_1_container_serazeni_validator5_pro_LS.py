import pandas as pd
import numpy as np
import json
from datetime import datetime
import copy
from collections import deque
from queue import PriorityQueue
import time
from utilities.constants import HOUR, DAY, start_of_month

from utilities.validator import validate_solution, validate_solution2
from utilities.path_finder import find_path, find_path2, find_path6, find_path20
from utilities.utils import parse_date, sort_by_second_element, sort_by_time_difference, convert_paths_and_grafikons_to_end_points, \
                  sort_by_terminal_then_time_difference
#from utilities.visualizer import visualize_three_posibilities

def constructive_heuristic_for_LS(df,  terminal_order=None, find_path_type=0, do_cut=False):
   start_time = time.time()

   kam_array = []
   odkud_array = []
   #teu_array = []
   container_array_id = []
   info_array = []
   df_cities = pd.read_csv('cities_matrix_filtered.csv')
   cities = df_cities.columns[1:]
   cities = df_cities.columns[1:].to_list()
   container_counter = 0
   for index, row in df.iterrows():
      kam_array.append(row['KAM'])
      odkud_array.append(row['ODKUD'])
   #  teu_array.append(row['TEU'])

      parsed_deadline = parse_date(row['DEADLINE'])
      parsed_release_time = parse_date(row['RELEASE_TIME'])
      time_difference_deadline = parsed_deadline - start_of_month
      time_difference_release_date = parsed_release_time - start_of_month
      deadline_in_minutes = int(max(0, time_difference_deadline.total_seconds() // 60 // DAY))
      release_date_in_minutes = int(max(0, time_difference_release_date.total_seconds() // 60 // DAY))

      name = row['ODKUD'] + row['KAM']
      container_array_id.append(index)
      odkud_index = cities.index(row['ODKUD'])
      kam_index = cities.index(row['KAM'])

      info = (deadline_in_minutes, release_date_in_minutes, container_counter, terminal_order[odkud_index])
      container_counter += 1
      info_array.append(info)
      if release_date_in_minutes >= deadline_in_minutes:
         print("error")
         exit()

   # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("Iterating through df:", elapsed_time, "seconds")

#   info_array = sorted(info_array, key=sort_by_time_difference)
   info_array = sorted(info_array, key=sort_by_terminal_then_time_difference)
   #1st data preprocessing: dat do balicku, 
   #chceme, aby v kazdem balicku byly kontejnery se stejnym pocatecnim a koncovym terminalem

   # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("Sorting containers:", elapsed_time, "seconds")


   with open('grafikony/grafikon_casy_komplet_filter.json', 'r') as file:
      data_grafikony = json.load(file)

   with open('paths2.json', 'r') as file:
      possible_cesty = json.load(file)

   with open('cut_time_paths.json', 'r') as file:
      cut_time = json.load(file)

   #used_grafikons = np.zeros(len(data_grafikony['trains']), dtype=int)
   grafikons_capacity = np.zeros(len(data_grafikony['trains']), dtype=int)
   seznam_sousedu = {}
   for city in cities:
      seznam_sousedu[city] = []

   hub_route_array = []
   grafikon_ids = []


   used_grafikons = []
   for i in range(len(data_grafikony['trains'])):
      #length.. 6 = 1 TEU.. wagon - 24 TEU = length / 12 containers
      grafikons_capacity[i] = data_grafikony['trains'][i]['max_length'] // 12
      hub_route_array.append(data_grafikony['trains'][i]['hub_route'])
      temp_array = []
      used_grafikons.append(temp_array)
      for x in range(1, len(hub_route_array[i])):
         used_grafikons[i].append(0)
      grafikon_ids.append(data_grafikony['trains'][i]['grafikon_ID'])
      if i != data_grafikony['trains'][i]['grafikon_ID']:
         exit()
      parsed_start_date = datetime.strptime(hub_route_array[i][0]["departure"] , "%H:%M:%S")
      parsed_start_date_in_minutes = parsed_start_date.hour * HOUR + parsed_start_date.minute
      for x in range(len(hub_route_array[i])):
         starting_city = hub_route_array[i][x]["stop_code"]
         parsed_date = datetime.strptime(hub_route_array[i][x]["departure"] , "%H:%M:%S")
         if x == 0:
            op_day = (data_grafikony['trains'][i]['operational_days'] - 1)
         else:
            op_day = (data_grafikony['trains'][i]['operational_days'] - 1) + \
                     (parsed_start_date_in_minutes + hub_route_array[i][x]["curr_total_minutes"]) // DAY
         this_grafikon_time = op_day * DAY + parsed_date.hour * 60 + parsed_date.minute
         for j in range(x+1, len(hub_route_array[i])):
            end_city = hub_route_array[i][j]["stop_code"]
            seznam_sousedu[starting_city].append([end_city, grafikon_ids[i], this_grafikon_time, this_grafikon_time + 
                                                data_grafikony['trains'][i]['hub_route'][j]["curr_total_minutes"]])
      # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("Iterating through df trains:", elapsed_time, "seconds")
         
   valid_container_array_id = []
   total_cost = 0
   path_per_container = []
   grafikons_per_container = []
   container_id_array = []
   # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("Start of path finding:", elapsed_time, "seconds")
   for i in range(len(container_array_id)):
      info = info_array[i]
      idx = info[2]
      kam = kam_array[idx]
      odkud = odkud_array[idx]
      if find_path_type == 0:
         if do_cut:
            a, grafikon_arr = find_path6(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                       used_grafikons, data_grafikony['trains'], grafikons_capacity, cut_time)
         else:
            a, grafikon_arr = find_path20(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                       used_grafikons, data_grafikony['trains'], grafikons_capacity)
      elif find_path_type == 1:
         a, grafikon_arr = find_path3(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                    used_grafikons, data_grafikony['trains'], grafikons_capacity)
      else:
         a, grafikon_arr = find_path4(seznam_sousedu, odkud, kam, info[1], info[0], possible_cesty, 
                                    used_grafikons, data_grafikony['trains'], grafikons_capacity)
      
      valid = True
      """
      for grafikon_id in grafikon_arr:
         found_start = False
         over_capacity = False
         for index, stop_name in enumerate(data_grafikony['trains'][grafikon_id]):
            if stop_name == odkud:
               found_start = True
            if found_start and used_grafikons[grafikon_id][index] + 1 > grafikons_capacity[grafikon_id]:
               over_capacity = True
               break
            if stop_name == kam:
               break
         if over_capacity:
            continue
         if used_grafikons[grafikon_id] + 1 > grafikons_capacity[grafikon_id]:
            a = []
            valid = False
            print("xx")
            exit()
      """
      if valid:
         cost = 0
         for idd, grafikon_id in enumerate(grafikon_arr):
         #   print(a, grafikon_arr)
            odkud_now = a[idd]
            kam_now = a[idd + 1]
            is_not_used = True
            for j in range(len(used_grafikons[grafikon_id])):
               if used_grafikons[grafikon_id][j] != 0:
                  is_not_used = False
                  break
            if is_not_used:
               cost += data_grafikony['trains'][grafikon_id]['cost']

           # used_grafikons[grafikon_id] += 1
            found_start = False
            over_capacity = False
            for j in range(len(data_grafikony['trains'][grafikon_id]["hub_route"])):
               stop_name = data_grafikony['trains'][grafikon_id]["hub_route"][j]["stop_code"]
               if stop_name == odkud_now:
                  found_start = True
                  continue
               if found_start:
                  used_grafikons[grafikon_id][j - 1] += 1
               if stop_name == kam_now:
                  break
    #        print(used_grafikons[grafikon_id], stop_name, odkud, kam, a, grafikons_capacity[grafikon_id])
   #      print(total_cost, cost, i)
         total_cost += cost
      if len(a) > 0:
         container_id_array.append(idx)
         valid_container_array_id.append(idx)
         path_per_container.append(a)
         grafikons_per_container.append(grafikon_arr)
      else:
         container_id_array.append(None)
         path_per_container.append([])
         grafikons_per_container.append([])
   
      # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("End of path finding:", elapsed_time, "seconds")

   # if len(b) > 0:
   #    valid_container_array_id2.append(i)
   print(len(container_array_id))
   print(len(valid_container_array_id))
   print(total_cost)
   print(len(used_grafikons))
   #print(sum(used_grafikons))
  # count_positive = np.sum(used_grafikons > 0)
   cc = 0
   for i in range(len(grafikons_per_container)):
      cc += len(grafikons_per_container[i])
   print(cc)
   count_capacity_used = 0
   for i in range(len(used_grafikons)):
      for j in range(len(used_grafikons[i])):
         count_capacity_used += used_grafikons[i][j]
   print(count_capacity_used)
   print(len(data_grafikony["trains"]))
   print(np.sum(grafikons_capacity))
   file_path = "output3.txt"

   with open(file_path, "w") as file:
      for number in used_grafikons:
         file.write(str(number) + "\n")

   file_path = "output4.txt"
   with open(file_path, "w") as file:
      for number in grafikons_per_container:
         file.write(str(number) + "\n")

  # print("number of used grafikons :", count_positive)

   # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("Start of convertion:", elapsed_time, "seconds")
   used_start_points_array, used_end_points_array = convert_paths_and_grafikons_to_end_points(path_per_container, 
                                                                                             grafikons_per_container, 
                                                                                             data_grafikony['trains'])
      # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("Start of validation:", elapsed_time, "seconds")
   validate_solution2(df, data_grafikony['trains'], container_id_array, grafikons_per_container,
                        used_start_points_array, used_end_points_array)
   
      # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # Print the elapsed time
   print("End of validation:", elapsed_time, "seconds")
   print("everything ok")
   return len(valid_container_array_id)
if __name__=="__main__":
   df = pd.read_csv('kontejnery_filtered_no_impossible_containers.csv')
   sample_size = 1000
   print(len(df))
   a_cost = []
   b_cost = []
   c_cost = []
   a_invalid = []
   b_invalid = []
   c_invalid = []
   valid_containers, total_cost = constructive_heuristic(df, 0)
   exit()
   for j in range(3):
      for i in range(1000, len(df), 1000):
         sample_size = i
         random_sample = df.sample(n=sample_size)
         valid_containers, total_cost = constructive_heuristic(random_sample, j, True)
         invalid_containers = sample_size - valid_containers
         if j == 0:
            a_cost.append(total_cost)
            a_invalid.append(invalid_containers)
            valid_containers, total_cost = constructive_heuristic(random_sample, j, False)
            if total_cost != a_cost[-1] or (sample_size - valid_containers) != a_invalid[-1]:
               exit()
         elif j == 1:
            b_cost.append(total_cost)
            b_invalid.append(invalid_containers)
         else:
            c_cost.append(total_cost)
            c_invalid.append(invalid_containers)
      valid_containers, total_cost = constructive_heuristic(df, j)
      invalid_containers = len(df) - valid_containers
      if j == 0:
         a_cost.append(total_cost)
         a_invalid.append(invalid_containers)
      elif j == 1:
         b_cost.append(total_cost)
         b_invalid.append(invalid_containers)
      else:
         c_cost.append(total_cost)
         c_invalid.append(invalid_containers)
   visualize_three_posibilities(len(df), a_cost, b_cost, c_cost)
   visualize_three_posibilities(len(df), a_invalid, b_invalid, c_invalid)
