import numpy as np
import pandas as pd
import gurobipy as g
import json
import sys
import os

current_path = os.getcwd()
directory_path = os.path.dirname(current_path)

sys.path.append(directory_path)

from global_variables import path_to_kontejnery_filtered_removed_impossible, path_to_terminal_matrix_filtered


def run_mincostflow():
   df = pd.read_csv(path_to_kontejnery_filtered_removed_impossible)
   df = df.sample(14000)
   #1.nacist grafikony
   with open('../data_processing/processed_data_timetables/timetables.json', 'r') as file:
        data_grafikony = json.load(file)


   grafikons_capacity = np.zeros(len(data_grafikony['trains']), dtype=int)
   balance = np.zeros(len(data_grafikony['trains']), dtype=int)
   for i in range(len(data_grafikony['trains'])):
      #length.. 6 = 1 TEU.. wagon - 24 TEU = length / 12 containers
      grafikons_capacity[i] = data_grafikony['trains'][i]['max_length'] // 12
   #2. nacist uvazovane terminaly
   df_cities = pd.read_csv(path_to_terminal_matrix_filtered)
   cities = df_cities.columns[1:].to_list()

   from_city_to_index = {}
   for i, city in enumerate(cities):
      from_city_to_index[city] = i

   seznam_sousedu = {}
   graf_kapacity_city_to_city = np.zeros((len(data_grafikony['trains']), len(data_grafikony['trains'])), dtype=int)
   for city1 in cities:
      for city2 in cities:
         if city1 == city2:
            continue
         seznam_sousedu[city1 + city2] = []

   counter = 0
   graf_kapacity = 0
   for x, city1 in enumerate(cities):
      for y, city2 in enumerate(cities):
         if city1 == city2:
            continue
         for i, grafikon in enumerate(data_grafikony['trains']):
            if grafikon["grafikon_ID"] != i:
               print(i)
               exit()
            hub_route = grafikon['hub_route']
            for j in range(len(hub_route)):
               if hub_route[j]["stop_code"] != city1:
                  continue
               for k in range(j+1, len(hub_route)):
                  if hub_route[k]["stop_code"] != city2:
                     continue
                  seznam_sousedu[city1 + city2].append((i, j, k, grafikons_capacity[i]))
                  counter += 1
                  graf_kapacity += grafikons_capacity[i]
                  graf_kapacity_city_to_city[x, y] += grafikons_capacity[i]

   print(counter)
   print(graf_kapacity)
   #now, get vertices
   m = g.Model()
   flow = m.addVars(len(cities), len(cities), vtype=g.GRB.INTEGER)
   balance_export = np.zeros(len(data_grafikony['trains']), dtype=int)
   balance_import = np.zeros(len(data_grafikony['trains']), dtype=int)
   for i, city1 in enumerate(cities):
      for j, city2 in enumerate(cities):
         if city1 == city2:
            m.addConstr(flow[i, i] == 0)
         else:
            m.addConstr(flow[i, j] >= 0)
            m.addConstr(flow[i, j] <= graf_kapacity_city_to_city[i, j]) # + 6

   for index, row in df.iterrows():
      city_odkud = row['ODKUD']
      city_kam = row['KAM']
      city_odkud_idx = from_city_to_index[city_odkud]
      city_kam_idx = from_city_to_index[city_kam]
      balance[city_odkud_idx] += 1
      balance[city_kam_idx] -= 1
      balance_export[city_odkud_idx] += 1
      balance_import[city_kam_idx] += 1

   #to co vystupuje - to co vstupuje == balance[v]
   m.addConstrs(sum(flow[j, i] for i in range(len(cities)) if i != j) - sum(flow[i, j] for i in range(len(cities)) if i != j)
                == balance[j] for j in range(len(cities)))  
   
   m.setObjective(sum(flow[i, j] for i in range(len(cities)) for j in range(len(cities))), sense=g.GRB.MINIMIZE)
   m.optimize()
  # x_values = m.getAttr('X', flow)
   to_transport = 0
   for i, city in enumerate(cities):
      export_kapacity = sum(graf_kapacity_city_to_city[i])
      import_kapacity = sum(graf_kapacity_city_to_city[:, i])
      if balance_export[i] > export_kapacity or balance_import[i] > import_kapacity:
         print(city, balance[i], balance_export[i], export_kapacity, -balance_import[i], -import_kapacity)

      if balance[i] > 0:
         to_transport += balance[i]
         
   print(to_transport)
   print(sum(balance))

def run_mincostflow_multicommodity():
   df = pd.read_csv(path_to_kontejnery_filtered_removed_impossible)
   #1.nacist grafikony
   with open('../data_processing/processed_data_timetables/timetables.json', 'r') as file:
        data_grafikony = json.load(file)


   grafikons_capacity = np.zeros(len(data_grafikony['trains']), dtype=int)
   balance = np.zeros(len(data_grafikony['trains']), dtype=int)
   for i in range(len(data_grafikony['trains'])):
      #length.. 6 = 1 TEU.. wagon - 24 TEU = length / 12 containers
      grafikons_capacity[i] = data_grafikony['trains'][i]['max_length'] // 12
   #2. nacist uvazovane terminaly
   df_cities = pd.read_csv(path_to_terminal_matrix_filtered)
   cities = df_cities.columns[1:].to_list()

   from_city_to_index = {}
   for i, city in enumerate(cities):
      from_city_to_index[city] = i

   seznam_sousedu = {}
   graf_kapacity_city_to_city = np.zeros((len(data_grafikony['trains']), len(data_grafikony['trains'])), dtype=int)

   commodity_counter = np.zeros((len(cities), len(cities)), dtype=int)

   for x, city1 in enumerate(cities):
      for y, city2 in enumerate(cities):
         if city1 == city2:
            continue
         seznam_sousedu[city1 + city2] = []

   counter = 0
   graf_kapacity = 0
   for x, city1 in enumerate(cities):
      for y, city2 in enumerate(cities):
         if city1 == city2:
            continue
         for i, grafikon in enumerate(data_grafikony['trains']):
            if grafikon["grafikon_ID"] != i:
               print(i)
               exit()
            hub_route = grafikon['hub_route']
            for j in range(len(hub_route)):
               if hub_route[j]["stop_code"] != city1:
                  continue
               for k in range(j+1, len(hub_route)):
                  if hub_route[k]["stop_code"] != city2:
                     continue
                  seznam_sousedu[city1 + city2].append((i, j, k, grafikons_capacity[i]))
                  counter += 1
                  graf_kapacity += grafikons_capacity[i]
                  graf_kapacity_city_to_city[x, y] += grafikons_capacity[i]

   print(counter)
   print(graf_kapacity)
   #now, get vertices
   m = g.Model()
   balance_export = np.zeros(len(data_grafikony['trains']), dtype=int)
   balance_import = np.zeros(len(data_grafikony['trains']), dtype=int)

   for index, row in df.iterrows():
      city_odkud = row['ODKUD']
      city_kam = row['KAM']
      city_odkud_idx = from_city_to_index[city_odkud]
      city_kam_idx = from_city_to_index[city_kam]
      balance[city_odkud_idx] += 1
      balance[city_kam_idx] -= 1
      balance_export[city_odkud_idx] += 1
      balance_import[city_kam_idx] += 1
      commodity_counter[city_odkud_idx, city_kam_idx] += 1

   print(len(commodity_counter))
   num_of_unique = len(commodity_counter[commodity_counter > 0])
   
   flow = m.addVars(num_of_unique, len(cities), len(cities), vtype=g.GRB.INTEGER)
   m.update()
   for i, city1 in enumerate(cities):
      for j, city2 in enumerate(cities):
         if city1 == city2:
         #   print(sum(flow[m, i, j] for m in range(num_of_unique)))
            m.addConstr(sum(flow[mm, i, j] for mm in range(num_of_unique)) == 0)
         else:
            m.addConstr(sum(flow[mm, i, j] for mm in range(num_of_unique)) >= 0)
            m.addConstr(sum(flow[mm, i, j] for mm in range(num_of_unique)) <= graf_kapacity_city_to_city[i, j]) # + 6

   #to co vystupuje - to co vstupuje == balance[v]
   curr_counter = 0
   for x in range(len(commodity_counter)):
      for y in range(len(commodity_counter[x])):
         if commodity_counter[x, y] == 0:
            continue
         m.addConstr(sum(flow[curr_counter, x, i] for i in range(len(cities)) if i != x) - 
                      sum(flow[curr_counter, i, x] for i in range(len(cities)) if i != x)
                     == commodity_counter[x, y]) 
         m.addConstr(sum(flow[curr_counter, y, i] for i in range(len(cities)) if i != y) - 
                     sum(flow[curr_counter, i, y] for i in range(len(cities)) if i != y)
                  == -commodity_counter[x, y])
         for xx in range(len(cities)):
            if xx != x and xx != y:
               m.addConstr(sum(flow[curr_counter, xx, i] for i in range(len(cities)) if i != xx) - 
                           sum(flow[curr_counter, i, xx] for i in range(len(cities)) if i != xx)
                           == 0)
         curr_counter += 1 
   
   m.setObjective(sum(flow[mm, i, j] 
                      for mm in range(num_of_unique)
                      for i in range(len(cities)) 
                      for j in range(len(cities))), sense=g.GRB.MINIMIZE)
   m.optimize()
  # x_values = m.getAttr('X', flow)
   to_transport = 0
   for i, city in enumerate(cities):
      export_kapacity = sum(graf_kapacity_city_to_city[i])
      import_kapacity = sum(graf_kapacity_city_to_city[:, i])
      if balance_export[i] > export_kapacity or balance_import[i] > import_kapacity:
         print(city, balance[i], balance_export[i], export_kapacity, -balance_import[i], -import_kapacity)

      if balance[i] > 0:
         to_transport += balance[i]
         
   print(to_transport)
   print(sum(balance))

if __name__=="__main__":
 #  run_mincostflow()
   run_mincostflow_multicommodity()