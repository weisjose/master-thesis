import pandas as pd
import numpy as np
import json
from datetime import datetime
import copy
from collections import deque
from queue import PriorityQueue
import time
import sys
import os

current_path = os.getcwd()
directory_path = os.path.dirname(current_path)

sys.path.append(directory_path)

from utilities.constants import HOUR, DAY, start_of_month

from utilities.validator import validate_solution, validate_solution2
from utilities.path_finder import find_path, find_path2, find_path40, find_path6, \
                                    find_path200, find_path20, find_path20000, find_path2000, find_path210, find_path222
from utilities.utils import parse_date, sort_by_second_element, sort_by_time_difference, \
                            convert_paths_and_grafikons_to_end_points, sort_by_third_element, sort_by_terminal_then_time_difference
from grafikon_removal import find_least_full_timetable
from heuristika_replanning import replanning

from global_variables import path_to_kontejnery_filtered_removed_impossible, path_to_stops_in_routes, \
                           path_to_terminal_matrix_filtered
import matplotlib.pyplot as plt

def add_to_used_grafikons(grafikon_idd, odkud, kam, data_grafikony, grafikon_stops, curr_container):
   found_start = False
   ended = False
   for j in range(len(data_grafikony['trains'][grafikon_idd]["hub_route"])):
      stop_name = data_grafikony['trains'][grafikon_idd]["hub_route"][j]["stop_code"]
      if stop_name == odkud:
         found_start = True
         start_idx = j
         continue
      if found_start:
         used_grafikons[grafikon_idd][j - 1] += 1
      if stop_name == kam:
         ended = True
         end_idx = j - 1
         break
   if not ended:
      print("not ended")
      exit()
   grafikon_stops[curr_container].append((start_idx, end_idx))

def remove_from_used_grafikons(grafikon_idd, start_idx, end_idx, data_grafikony, grafikon_stops, curr_container):
   for j in range(len(data_grafikony['trains'][grafikon_idd]["hub_route"])):
      if j >= start_idx and j <= end_idx:
         used_grafikons[grafikon_idd][j - 1] += 1
   grafikon_stops[curr_container] = []

def constructive_heuristic(container_info, grafikony_info, data_grafikony, grafikon_stops,
                           grafikons_per_container2, terminal_order = None):
   start_time = time.time()
   #teu_array = []
   containers_in_grafikon = []
   # Record the end time
   end_time = time.time()
   find_path_type = 0
   do_cut = False
   # Calculate the elapsed time
   elapsed_time = end_time - start_time
   odkud_array = container_info[0]
   kam_array = container_info[1]
   info_array = container_info[2]
   df = container_info[3]
   info_array = sorted(info_array, key=lambda item: sort_by_terminal_then_time_difference(item, terminal_order))
   #1st data preprocessing: dat do balicku, 
   #chceme, aby v kazdem balicku byly kontejnery se stejnym pocatecnim a koncovym terminalem

   # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

 #  with open('../data_processing/processed_data_timetables//grafikon_casy_komplet_filter.json', 'r') as file:
 #     data_grafikony = json.load(file)

   with open(path_to_stops_in_routes, 'r') as file:
      possible_cesty = json.load(file)

  # with open('../data_processing/heuristic_simplification_data/paths3.json', 'r') as file:
  #    super_dict = json.load(file)

   #used_grafikons = np.zeros(len(data_grafikony['trains']), dtype=int)

   grafikons_capacity = grafikony_info[0]
   used_grafikons = grafikony_info[1]
   containers_in_grafikon = grafikony_info[2]
   grafikon_ids = grafikony_info[3]
   seznam_sousedu = grafikony_info[4]
   num_containers_in_grafikon = grafikony_info[5]
 #  seznam_sousedu_per_container = grafikony_info[6]
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

         
   valid_container_array_id = []
   total_cost = 0
   path_per_container = []
   grafikons_per_container = []
   container_id_array = []
   # Record the end time
   end_time = time.time()

   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   counted_grafikons = []
   counted_grafikons_counter = 0
   for i in range(len(kam_array)):
      info = info_array[i]
      idx = info[2]
      kam = kam_array[idx]
      odkud = odkud_array[idx]
   #   print(i, odkud, kam, len(valid_container_array_id))
      a, grafikon_arr = find_path200(seznam_sousedu, odkud, kam, info[1], info[0],possible_cesty, #super_dict
                                       used_grafikons, data_grafikony['trains'], grafikons_capacity)
      if len(a) > 0:
         cost = 0
         for idd, grafikon_idd in enumerate(grafikon_arr):
            grafikon_id = grafikon_ids[grafikon_idd]
            odkud_now = a[idd]
            kam_now = a[idd + 1]
            is_not_used = True
            containers_in_grafikon[grafikon_idd].append(idx)
            num_containers_in_grafikon[grafikon_idd] += 1
            for j in range(len(used_grafikons[grafikon_idd])):
               if used_grafikons[grafikon_idd][j] != 0:
                  is_not_used = False
                  break
            if is_not_used:
               cost += data_grafikony['trains'][grafikon_idd]['cost']
               counted_grafikons.append(grafikon_idd)
               counted_grafikons_counter += 1

           # used_grafikons[grafikon_id] += 1
            found_start = False
            over_capacity = False
            start_idx = 0
            end_idx = 0
            ended = False
            for j in range(len(data_grafikony['trains'][grafikon_idd]["hub_route"])):
               stop_name = data_grafikony['trains'][grafikon_idd]["hub_route"][j]["stop_code"]
               if stop_name == odkud_now:
                  found_start = True
                  start_idx = j
                  continue
               if found_start:
                  used_grafikons[grafikon_idd][j - 1] += 1
               if stop_name == kam_now:
                  ended = True
                  end_idx = j - 1
                  break
            if not ended:
               print("not ended")
               exit()
            grafikon_stops[idx].append((start_idx, end_idx))
    #        print(used_grafikons[grafikon_id], stop_name, odkud, kam, a, grafikons_capacity[grafikon_id])
   #      print(total_cost, cost, i)
         total_cost += cost
      if len(a) > 0:
         container_id_array.append(idx)
         valid_container_array_id.append(idx)
         path_per_container.append(a)
         grafikons_per_container.append(grafikon_arr)
         grafikons_per_container2[idx] = grafikon_arr
         
      else:
         container_id_array.append(None)
         path_per_container.append([])
         grafikons_per_container.append([])
   
      # Record the end time
   end_time = time.time()
   # Calculate the elapsed time
   elapsed_time = end_time - start_time

   # if len(b) > 0:
   #    valid_container_array_id2.append(i)
   #print(sum(used_grafikons))
  # count_positive = np.sum(used_grafikons > 0)
   count_capacity_used = 0
   count_grafikons_used = 0
   cost2 = 0
   for i in range(len(used_grafikons)):
      do_count = False
      for j in range(len(used_grafikons[i])):
         count_capacity_used += used_grafikons[i][j]
         if used_grafikons[i][j] != 0:
            do_count = True
      if do_count:
         count_grafikons_used += 1
         cost2 += data_grafikony['trains'][i]['cost']
   file_path = "output20.txt"

   with open(file_path, "w") as file:
      for i in range(len(path_per_container)):
         for terminal in path_per_container[i]:
            file.write(str(terminal) + " ")
         file.write("\n")
   file_path = "output3.txt"

   with open(file_path, "w") as file:
      for number in used_grafikons:
         file.write(str(number) + "\n")

   file_path = "output4.txt"
   with open(file_path, "w") as file:
      for number in grafikons_per_container:
         file.write(str(number) + "\n")

  # print("number of used grafikons :", count_positive)

   # Record the end time
   end_time = time.time()

   elapsed_time = end_time - start_time

   used_start_points_array, used_end_points_array = convert_paths_and_grafikons_to_end_points(path_per_container, 
                                                                                             grafikons_per_container, 
                                                                                             data_grafikony['trains'])
   end_time = time.time()

   elapsed_time = end_time - start_time

   validate_solution2(df, data_grafikony['trains'], container_id_array, grafikons_per_container,
                        used_start_points_array, used_end_points_array)
   
   end_time = time.time()

   elapsed_time = end_time - start_time

   #min_idx, to_drop = find_least_full_timetable(data_grafikony, grafikons_capacity, used_grafikons, 
   #                                            grafikons_per_container2, containers_in_grafikon)
   info_arrays = (grafikons_capacity, used_grafikons, grafikons_per_container2, containers_in_grafikon, 
                  grafikon_stops, num_containers_in_grafikon)
   print("End of validation:", elapsed_time, "seconds")
   print("everything ok", len(valid_container_array_id), total_cost)
   return len(valid_container_array_id), total_cost, info_arrays

if __name__=="__main__":
   df = pd.read_csv(path_to_kontejnery_filtered_removed_impossible)
   sample_size = 1000
   print(len(df))
   num_containers = len(df)
   #save df info
   grafikon_stops = []
   grafikons_per_container2 = []
   kam_array = []
   odkud_array = []
   container_array_id = []
   info_array = []
   df_cities = pd.read_csv(path_to_terminal_matrix_filtered)
   cities = df_cities.columns[1:].to_list()

   for index, row in df.iterrows():
      temp_array4 = []
      grafikon_stops.append(temp_array4)
      temp_array3 = []
      grafikons_per_container2.append(temp_array3)
      kam_array.append(row['KAM'])
      odkud_array.append(row['ODKUD'])

      parsed_deadline = parse_date(row['DEADLINE'])
      parsed_release_time = parse_date(row['RELEASE_TIME'])
      time_difference_deadline = parsed_deadline - start_of_month
      time_difference_release_date = parsed_release_time - start_of_month
      deadline_in_minutes = int(max(0, time_difference_deadline.total_seconds() // 60 // DAY))
      release_date_in_minutes = int(max(0, time_difference_release_date.total_seconds() // 60 // DAY))
      name = row['ODKUD'] + row['KAM']
      container_array_id.append(index)
      odkud_index = cities.index(row['ODKUD'])
      kam_index = cities.index(row['KAM'])
      info = (deadline_in_minutes, release_date_in_minutes, index, odkud_index)
      info_array.append(info)

   seznam_sousedu = {}
   for city in cities:
      seznam_sousedu[city] = []

   with open('../data_processing/processed_data_timetables/timetables.json', 'r') as file:
      data_grafikony = json.load(file)

   grafikons_capacity = np.zeros(len(data_grafikony['trains']), dtype=int)
   used_grafikons = []
   containers_in_grafikon = []
   num_containers_in_grafikon = np.zeros(len(data_grafikony['trains']), dtype=int)
   grafikon_ids = []
   graf_info = []
   for i in range(len(data_grafikony['trains'])):
      #length.. 6 = 1 TEU.. wagon - 24 TEU = length / 12 containers
      grafikons_capacity[i] = data_grafikony['trains'][i]['max_length'] // 12
      hub_route = data_grafikony['trains'][i]['hub_route']
      temp_array = []
      used_grafikons.append(temp_array)
      temp_array2 = []
      containers_in_grafikon.append(temp_array2)
      for x in range(1, len(hub_route)):
         used_grafikons[i].append(0)
      grafikon_ids.append(data_grafikony['trains'][i]['grafikon_ID'])
  #    if i != data_grafikony['trains'][i]['grafikon_ID']:
  #       exit()
      parsed_start_date = datetime.strptime(hub_route[0]["departure"] , "%H:%M:%S")
      parsed_start_date_in_minutes = parsed_start_date.hour * HOUR + parsed_start_date.minute
      for x in range(len(hub_route)):
         starting_city = hub_route[x]["stop_code"]
         parsed_date = datetime.strptime(hub_route[x]["departure"] , "%H:%M:%S")
         if x == 0:
            op_day = (data_grafikony['trains'][i]['operational_days'] - 1)
         else:
            op_day = (data_grafikony['trains'][i]['operational_days'] - 1) + \
                     (parsed_start_date_in_minutes + hub_route[x]["curr_total_minutes"]) // DAY
         this_grafikon_time = op_day * DAY + parsed_date.hour * 60 + parsed_date.minute
         for j in range(x+1, len(hub_route)):
            end_city = hub_route[j]["stop_code"]
            seznam_sousedu[starting_city].append([end_city, i, this_grafikon_time, this_grafikon_time + 
                                                data_grafikony['trains'][i]['hub_route'][j]["curr_total_minutes"]])

   container_info = (odkud_array, kam_array, info_array, df)
   for city in cities:
      seznam_sousedu[city] = sorted(seznam_sousedu[city], key=sort_by_third_element)
   
   #for each container, get all grafikons + sort them
   """
   seznam_sousedu_per_container = []
   for i in range(num_containers):
      seznam_sousedu_per_container.append({})
      info = info_array[i] 
      container_deadline = info[0] * DAY
      container_release_date = info[1] * DAY
      for j in range(len(cities)):
         seznam_sousedu_per_container[i][cities[j]] = []
         curr_grafikons = seznam_sousedu[cities[j]]
         for id, [city, grafikon_id, rel_date, deadline] in enumerate(curr_grafikons):
            if container_release_date > rel_date or container_deadline < deadline:
               continue
            seznam_sousedu_per_container[i][cities[j]].append(curr_grafikons[id])
         seznam_sousedu_per_container[i][cities[j]] = sorted(seznam_sousedu_per_container[i][cities[j]], key=sort_by_third_element)
         print(len(seznam_sousedu_per_container[i][cities[j]]), len(seznam_sousedu[cities[j]]))
     #    info_array = sorted(info_array, key=sort_by_time_difference)
           # print(curr_grafikon, id)
   print("Zde1")
   print("zde")
   exit()
   """

   grafikony_info = (grafikons_capacity, used_grafikons, containers_in_grafikon, grafikon_ids, seznam_sousedu,
                     num_containers_in_grafikon)
  # with open('seznam_soudedu_per_container.json', 'w') as f:
  #      json.dump(seznam_sousedu_per_container, f, indent=4)
  # with open('seznam_soudedu_per_container.json', 'r') as file:
  #    seznam_sousedu_per_container = json.load(file)         
        

   valid_containers, total_cost, info_arrays = constructive_heuristic(container_info, grafikony_info, data_grafikony,
                                                                      grafikon_stops, grafikons_per_container2)
   grafikons_capacity, used_grafikons, grafikons_per_container2, containers_in_grafikon = info_arrays[0], info_arrays[1], \
                                                                                          info_arrays[2], info_arrays[3]
   grafikon_stops = info_arrays[4]
   num_containers_in_grafikon = info_arrays[5]
   immune_grafikons = np.zeros(len(data_grafikony['trains']), dtype=bool)
   grafikony_info2 = (grafikons_capacity, grafikon_ids, seznam_sousedu)
   print(valid_containers, num_containers, total_cost)
   exit()
   with open(path_to_stops_in_routes) as file:
      possible_cesty = json.load(file)
   to_drop_mask = np.zeros(len(containers_in_grafikon), dtype=bool)
   y_array = []
   max_x = 0
   cum_sum = 0
   num_bins = 20

   hist1, bins1, _ = plt.hist(num_containers_in_grafikon, bins=num_bins, alpha=0.5, color='blue', label='Before')

   for x in range(10000):
      min_idx, to_drop, min_val = find_least_full_timetable(data_grafikony, grafikons_capacity, used_grafikons, 
                                                grafikons_per_container2, containers_in_grafikon, immune_grafikons,
                                                num_containers_in_grafikon, to_drop_mask)
    #  print(x, min_val, min_idx)
      if min_val == np.inf:
         max_x = x - 1
         break
      counter = 0
      containers_to_replan = copy.deepcopy(containers_in_grafikon[min_idx])
      previous = copy.deepcopy(containers_in_grafikon[min_idx])
      all_used_grafikons_by_container_to_replan = []
      for i in range(len(containers_to_replan)):
         curr_container = containers_to_replan[i]
         used_grafikons_by_container_to_replan = copy.deepcopy(grafikons_per_container2[curr_container])
         all_used_grafikons_by_container_to_replan.append(used_grafikons_by_container_to_replan)
         for j, used in enumerate(used_grafikons_by_container_to_replan):
            curr_grafikon_stops_start = grafikon_stops[curr_container][j][0]
            curr_grafikon_stops_end = grafikon_stops[curr_container][j][1]
            for k in range(curr_grafikon_stops_start, curr_grafikon_stops_end + 1):
               used_grafikons[used][k] -= 1
            containers_in_grafikon[used].remove(curr_container)
            num_containers_in_grafikon[used] -= 1
         #grafikons_per_container2[curr_container] = []
    #  if (20826 in containers_to_replan):
     #    print("Xx", containers_in_grafikon[2724], containers_to_replan,
     #                2724, len(used_grafikons), 0, len(used_grafikons[2724]),
     #                grafikon_stops[20826], 20826, grafikons_per_container2[20826])
    #     exit()
      if len(containers_in_grafikon[min_idx]) > 0:
         print("xxxx")
         print(min_idx, containers_in_grafikon[min_idx], all_used_grafikons_by_container_to_replan, containers_to_replan)
         exit()
      to_drop.append(min_idx)
      to_drop = sorted(to_drop)
      to_drop_mask[min_idx] = True
    #  new_data_grafikony = copy.deepcopy(data_grafikony)
   #   for j in range(len(to_drop)):
     #    new_data_grafikony["trains"].pop(to_drop[j] - counter)
     #    counter += 1
      #min idx...
    #  print(grafikons_per_container2[27855], containers_in_grafikon[214], containers_in_grafikon[1702], containers_in_grafikon[144])
      valid_containers2, total_cost2, info_arrays2 = replanning(odkud_array, kam_array, info_array, 
                                                                grafikons_capacity, grafikon_ids, seznam_sousedu, 
                                                                data_grafikony,
                                                                containers_to_replan, 
                                                                used_grafikons, 
                                                                to_drop,
                                                                to_drop_mask,
                                                                containers_in_grafikon,
                                                                grafikons_per_container2,
                                                                grafikon_stops,
                                                                possible_cesty,
                                                                num_containers_in_grafikon)
      """
      valid_containers2, total_cost2, info_arrays2 = replanning(container_info, grafikony_info2, 
                                                                data_grafikony,
                                                                containers_to_replan, 
                                                                copy.deepcopy(used_grafikons), 
                                                                to_drop,
                                                                to_drop_mask,
                                                                copy.deepcopy(containers_in_grafikon),
                                                                copy.deepcopy(grafikons_per_container2),
                                                                copy.deepcopy(grafikon_stops))
      """
      cum_sum += len(containers_to_replan)
      y_array.append(min_val)
      if valid_containers2 != len(containers_to_replan):
         for i in range(len(containers_to_replan)):
            curr_container = containers_to_replan[i]
            used_grafikons_by_container_to_replan = grafikons_per_container2[curr_container]
            for j, used in enumerate(used_grafikons_by_container_to_replan):
      #         print(j, used_grafikons_by_container_to_replan, grafikon_stops[curr_container])
               curr_grafikon_stops_start = grafikon_stops[curr_container][j][0]
               curr_grafikon_stops_end = grafikon_stops[curr_container][j][1]
               for k in range(curr_grafikon_stops_start, curr_grafikon_stops_end + 1):
                  used_grafikons[used][k] += 1
               containers_in_grafikon[used].append(curr_container)
               num_containers_in_grafikon[used] += 1
     #    print(containers_in_grafikon[min_idx], grafikons_per_container2[27855], containers_in_grafikon[214], containers_in_grafikon[1702])
         immune_grafikons[min_idx] = True
         containers_in_grafikon[min_idx] = previous
         num_containers_in_grafikon[min_idx] = len(previous)
         to_drop_mask[min_idx] = False
         continue

   hist2, bins2, _ = plt.hist(num_containers_in_grafikon, bins=num_bins, alpha=0.5, color='red', label='After')

   plt.xlabel('Value')
   plt.ylabel('Frequency')
   plt.title('Histogram of grafikon distribution')
   plt.legend()

   plt.show()
   """
   x = np.linspace(0, max_x, max_x + 1)  
   plt.plot(x, y_array, color='green')
   plt.xlabel('Iteration')
   plt.ylabel('Timetables removed')
   plt.title('Removed timetables graph')
   plt.show()
   """
  #   _, used_grafikons, grafikons_per_container3, containers_in_grafikon = info_arrays2[0], info_arrays2[1], \
  #                                                                                        info_arrays2[2], info_arrays2[3]
 #     grafikon_stops = info_arrays2[4]
 #     num_containers_in_grafikon = info_arrays2[5]
  #    for i in range(len(containers_to_replan)):
  #       curr_container = containers_to_replan[i]
  #       grafikons_per_container2[curr_container] = grafikons_per_container3[curr_container]

   #   print(valid_containers2 != len(containers_to_replan), min_idx, containers_to_replan, all_used_grafikons_by_container_to_replan,
   #         containers_in_grafikon[75], containers_in_grafikon[3057], containers_in_grafikon[40], grafikons_per_container2[22151],
   #        grafikons_per_container3[22151], grafikon_stops[22151])
   #   exit()
  #    grafikons_capacity, used_grafikons, grafikons_per_container2, containers_in_grafikon = info_arrays[0], info_arrays[1], \
  #                                                                                        info_arrays[2], info_arrays[3]
  #    grafikon_stops = info_arrays[4]
      
   print(total_cost)
   #calculate the new cost
   new_cost = 0
   for i in range(len(data_grafikony['trains'])):
      curr_used = used_grafikons[i]
      actually_used = False
      for j in range(len(curr_used)):
         if curr_used[j] >= 1:
            actually_used = True
            break
      if actually_used:
         new_cost += data_grafikony['trains'][i]['cost']
   
   print(new_cost)
   exit()
   for j in range(3):
      for i in range(1000, len(df), 1000):
         sample_size = i
         random_sample = df.sample(n=sample_size)
         valid_containers, total_cost = constructive_heuristic(random_sample, j, True)
         invalid_containers = sample_size - valid_containers
         if j == 0:
            a_cost.append(total_cost)
            a_invalid.append(invalid_containers)
            valid_containers, total_cost = constructive_heuristic(random_sample, j, False)
            if total_cost != a_cost[-1] or (sample_size - valid_containers) != a_invalid[-1]:
               exit()
         elif j == 1:
            b_cost.append(total_cost)
            b_invalid.append(invalid_containers)
         else:
            c_cost.append(total_cost)
            c_invalid.append(invalid_containers)
      valid_containers, total_cost = constructive_heuristic(df, j)
      invalid_containers = len(df) - valid_containers
      if j == 0:
         a_cost.append(total_cost)
         a_invalid.append(invalid_containers)
      elif j == 1:
         b_cost.append(total_cost)
         b_invalid.append(invalid_containers)
      else:
         c_cost.append(total_cost)
         c_invalid.append(invalid_containers)
   visualize_three_posibilities(len(df), a_cost, b_cost, c_cost)
   visualize_three_posibilities(len(df), a_invalid, b_invalid, c_invalid)
