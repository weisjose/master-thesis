import pandas as pd
import numpy as np
import os 
from datetime import datetime

path_to_main_dir = os.path.dirname(os.path.abspath(__file__))

#int, datetime variables
HOUR = 60
DAY = 24 * HOUR
start_of_month = datetime(2023, 1, 1)

#metrans data
path_to_metrans_data = path_to_main_dir + '/metrans_data'

path_to_export = path_to_metrans_data + '/export.xlsx'
path_to_import = path_to_metrans_data + '/import.xlsx'
path_to_trains = path_to_metrans_data + '/VLAKY_01_2023.xls'
path_to_terminals = path_to_metrans_data + '/stanice-vlaky-gps.xlsx'
path_to_distances = path_to_metrans_data + '/vzdalenosti.xls'


#processed data timetables
path_to_processed_data_timetables = path_to_main_dir + '/data_processing/processed_data_timetables'

path_to_import_filtered_w_paths = path_to_processed_data_timetables + '/import_filtered_w_paths.csv' 
path_to_export_filtered_w_paths = path_to_processed_data_timetables + '/export_filtered_w_paths.csv' 
path_to_kontejnery_filtered_w_paths = path_to_processed_data_timetables + '/kontejnery_filtered_w_paths.csv'
path_to_timetable_routes = path_to_processed_data_timetables + '/timetable_routes.json'
path_to_trains_filtered = path_to_processed_data_timetables + '/trains_filtered.csv'

path_to_timetables_with_times = path_to_processed_data_timetables + '/timetables_with_times.json'
path_to_finalized_timetables = path_to_processed_data_timetables + '/timetables.json'
path_to_distance_matrix = path_to_processed_data_timetables + '/distance_matrix.csv'
path_to_merged_timetables = path_to_processed_data_timetables + '/merged_timetables.json'

#processed data containers
path_to_processed_data_containers = path_to_main_dir + '/data_processing/processed_data_containers'

path_to_import_filtered = path_to_processed_data_containers + '/import_filtered.csv' 
path_to_export_filtered = path_to_processed_data_containers + '/export_filtered.csv' 
path_to_kontejnery_filtered = path_to_processed_data_containers + '/kontejnery_filtered.csv'
path_to_kontejnery_filtered_removed_impossible = path_to_processed_data_containers + '/kontejnery_filtered_removed_impossible.csv'


#data merge with zelinka
path_to_data_merge_with_zelinka = path_to_main_dir + '/data_processing/data_merge_with_zelinka/processed_data'

path_to_zelinka_timetables = path_to_data_merge_with_zelinka + '/new_grafikon.json' 

#heuristic simplificaton
path_to_heuristic_simplification_data = path_to_main_dir + '/data_processing/heuristic_simplification_data'

path_to_terminal_matrix = path_to_heuristic_simplification_data + '/terminals_matrix.csv'
path_to_terminal_matrix_filtered = path_to_heuristic_simplification_data + '/terminals_matrix_filtered.csv'
path_to_stops_in_routes = path_to_heuristic_simplification_data + '/path_to_stops_in_routes.json'

#heuristic utils
path_to_heuristic_algorithms_utils = path_to_main_dir + '/heuristic_algorithms/utilities'


#other
path_to_random_lengths = path_to_main_dir + '/random_numbers_lengths.txt' 
path_to_random_weights = path_to_main_dir + '/random_numbers_weights.txt' 
