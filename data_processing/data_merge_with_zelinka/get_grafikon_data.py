import json

with open('processed_data/grafikon.json', 'r') as file:
    data = json.load(file)

trains = data['trains']

for train in trains:
   train_number = train['train_number']
   operation_days = train['operation_days']
   hub_route = train['hub_route']
   route = train['route']
   max_speed = train['max_speed']
   max_weight = train['max_weight']
   max_length = train['max_length']
   
   print(f"Train Number: {train_number}, Operation Days: {operation_days}, Max Speed: {max_speed}, Max Weight: {max_weight}, Max Length: {max_length}")
   
  # print("\nHub Route:")
   for hub_stop in hub_route:
      stop_name = hub_stop['stop_name']
      arrival = hub_stop['arrival']
      departure = hub_stop['departure']
      days_passed = hub_stop['days_passed']
      stop_id = hub_stop['stop_id']
      stop_code = hub_stop['stop_code']
  #    print(f"Stop Name: {stop_name}, Arrival: {arrival}, Departure: {departure}, Days Passed: {days_passed}, Stop ID: {stop_id}, Stop Code: {stop_code}")
  # print("\nRoute:")
   for stop in route:
      stop_name = stop['stop_name']
      arrival = stop['arrival']
      departure = stop['departure']
      days_passed = stop['days_passed']
      stop_id = hub_stop['stop_id']
      stop_code = hub_stop['stop_code']
        
     # print(f"Stop Name: {stop_name}, Arrival: {arrival}, Departure: {departure}, Days Passed: {days_passed}, Stop ID: {stop_id}, Stop Code: {stop_code}")
    
   #print("\n---\n")