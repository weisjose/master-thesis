import json
import pandas as pd
from datetime import datetime
import copy

day_number_to_date = {6:[1, 8, 15, 22, 29], 0:[2, 9, 16, 23, 30], 1:[3, 10, 17, 24, 31],
                        2:[4, 11, 18, 25], 3:[5, 12, 19, 26], 4:[6, 13, 20, 27],
                        5:[7, 14, 21, 28]}

with open('processed_data/grafikon.json', 'r') as file:
    data = json.load(file)

df_distance_matrix = pd.read_csv('../distance_matrix.csv')
trains = data['trains']
new_trains = {}
new_trains['trains'] = []
counter = 0
HOUR = 60
DAY = 24 * HOUR
for i in range(len(trains)):
   train_number = trains[i]['train_number']
   operation_days = trains[i]['operation_days']
   hub_route = trains[i]['hub_route']
   route = trains[i]['route']
   trains[i].pop('route')
   max_speed = trains[i]['max_speed']
   max_weight = trains[i]['max_weight']
   max_length = trains[i]['max_length']
   
   print(f"Train Number: {train_number}, Operation Days: {operation_days}, Max Speed: {max_speed}, Max Weight: {max_weight}, Max Length: {max_length}")
  # print("\nHub Route:")
   total_minutes = 0
   count_hub = 0
   previous_stop = None
   distance_sum = 0
   is_valid = True
   if len(hub_route) <= 1:
      continue
   for j in range(len(hub_route)):
      curr_stop_dict = {}

      arrival = hub_route[j]['arrival']
      departure = hub_route[j]['departure']
      days_passed = hub_route[j]['days_passed']
      stop_id = hub_route[j]['stop_id']
      stop_code = hub_route[j]['stop_code']


      time_obj = datetime.strptime(arrival, "%H:%M,%S")
      formatted_time = time_obj.strftime("%H:%M:%S")
      hub_route[j]['arrival'] = formatted_time
      hub_route[j]['departure'] = hub_route[j]['arrival']
      if j == 0:
         start_date = datetime.strptime(hub_route[j]['departure'] , "%H:%M:%S")
         start_days_passed = days_passed
      elif j == len(hub_route) - 1:
         try:
            previous_stop_index = df_distance_matrix.columns.get_loc(previous_stop)
            curr_stop_index = df_distance_matrix.columns.get_loc(stop_code)
         except KeyError:
            is_valid = False
            break
         previous_stop_index = df_distance_matrix.columns.get_loc(previous_stop)
         curr_stop_index = df_distance_matrix.columns.get_loc(stop_code)
         distance = df_distance_matrix.iloc[previous_stop_index - 1, curr_stop_index] 
         if pd.isna(distance):
            is_valid = False
            break
         distance_sum += distance
         parsed_date = datetime.strptime(hub_route[j]['departure'] , "%H:%M:%S")
         time_difference = ((parsed_date.hour - start_date.hour) * HOUR + 
                            (parsed_date.minute - start_date.minute) + 
                            (days_passed  - start_days_passed) * DAY)
         total_minutes = time_difference
         hub_route[j]["curr_total_minutes"] = total_minutes
      else:
         try:
            previous_stop_index = df_distance_matrix.columns.get_loc(previous_stop)
            curr_stop_index = df_distance_matrix.columns.get_loc(stop_code)
         except KeyError:
            is_valid = False
            break
         distance = df_distance_matrix.iloc[previous_stop_index - 1, curr_stop_index] 
         if pd.isna(distance):
            is_valid = False
            break
         parsed_date = datetime.strptime(hub_route[j]['departure'] , "%H:%M:%S")
         time_difference = ((parsed_date.hour - start_date.hour) * HOUR + 
                            (parsed_date.minute - start_date.minute) + 
                            (days_passed  - start_days_passed) * DAY)
         total_minutes = time_difference
         hub_route[j]["curr_total_minutes"] = total_minutes
         distance_sum += distance

      previous_stop = hub_route[j]['stop_code']

   if not is_valid:
      continue
   total_cost = 0
   if distance_sum < 300:
      total_cost += distance_sum * 10
   elif distance_sum < 600:
      total_cost += 300 * 10 + (distance_sum - 300) * 9
   else:
      total_cost += 300 * 10 + 300 * 9 + (distance_sum - 600) * 8

   train_cost = 1000 / (24 * 60)
   total_cost += int(total_minutes * train_cost) 
   trains[i]["cost"] = total_cost
   for op_day in operation_days:
      actual_days = day_number_to_date[op_day]
      for actual_day in actual_days:
         this_train = copy.deepcopy(trains[i])
         this_train['operational_days'] = actual_day
         this_train["grafikon_ID"] = counter
         new_trains["trains"].append(this_train)
         counter += 1
         
          

with open("processed_data/new_grafikon.json", "w") as f:
      json.dump(new_trains, f, indent=4)