import json
import matplotlib.pyplot as plt

with open('processed_data/grafikon.json', 'r') as file:
    data = json.load(file)

max_lengths = [train['max_length'] for train in data['trains']]
plt.hist(max_lengths, bins=10, color='skyblue', edgecolor='black')

plt.xlabel('Max Length')
plt.ylabel('Frequency')
plt.title('Histogram of Max Length Values')

plt.show()