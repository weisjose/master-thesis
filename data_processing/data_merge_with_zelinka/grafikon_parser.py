from enum import Enum
from unidecode import unidecode
import pandas as pd
import numpy as np
import pathlib
import json

TRAIN_NUMBER_STR = "Číslo:"
TRAIN_OPERATION_DAYS_STR = "Kalendář:"
ROUTE_START_STR = "Dopravní bod"

def get_all_weekdays():
    return list(range(0, 7))

class GrafikonParserState(Enum):
    FREE = 0
    TRAIN_NUMBER = 1
    TRAIN_OPERATION_DAYS = 2
    TRAIN_ROUTE = 3

class GrafikonParser:
    def __init__(self, terminals) -> None:
        self.state = GrafikonParserState.FREE
        self.grafikon = {"trains": []}
        self.current_train = None
        self.current_route = None
        self.parameters = None
        self.current_max_speed = None
        self.current_min_max_weight = None
        self.current_min_max_length = None
        self.terminal_names = terminals
    
    def parse(self, fname):
        df = pd.read_excel(fname, engine='openpyxl')

        for _, row in df.iterrows():
            self.parse_row(row)

        return self.grafikon

    def parse_row(self, row):
        if row[0] is np.nan:
            return
        
        tokens = row[0].split()

        if tokens[0] == TRAIN_NUMBER_STR:
            # close the last train
            if self.state == GrafikonParserState.TRAIN_ROUTE:
                if not self.add_train_to_grafikon():
                    print("Error adding train to grafikon, something is missing in the structure.")

            self.new_train(int(tokens[1]))
            self.state = GrafikonParserState.TRAIN_NUMBER

        elif self.state == GrafikonParserState.TRAIN_NUMBER:
            if tokens[0] == TRAIN_OPERATION_DAYS_STR:
                self.state = GrafikonParserState.TRAIN_OPERATION_DAYS
                if not self.add_operation_days(tokens[1:]):
                    print("Error adding operation days, unable to parse the info:", row[0])

        elif self.state == GrafikonParserState.TRAIN_OPERATION_DAYS:
            if row[0] == ROUTE_START_STR:
                self.state = GrafikonParserState.TRAIN_ROUTE
        
        elif self.state == GrafikonParserState.TRAIN_ROUTE:
            # add new stop on route
            self.new_stop_on_route(row)


    def new_train(self, train_number: int):
        # reset the train properties
        self.current_max_speed = None
        self.current_min_max_weight = None
        self.current_min_max_length = None
        self.current_route = None
        self.prev_time = None
        self.days_passed = 0

        self.current_train = {"train_number": train_number}
    
    
    def add_operation_days(self, tokens):
        # TODO imporve the parsing, currently missing nejede v
        operation_days = []
        negation = False
        for i, token in enumerate(tokens):
            if i == 0: 
                if token == "nejede":
                    negation = True
                    operation_days = get_all_weekdays()
                elif token != "jede":
                    return False
                else:
                    continue
            if i == 1:
                if token == "denně":
                    operation_days = get_all_weekdays()
                elif token == "do":
                    # could change those two if i would include the date conditions as well
                    operation_days = get_all_weekdays()
                elif token == "od":
                    operation_days = get_all_weekdays()
                elif token != "v":
                    if negation:
                        # date excluded explicitly, ignore
                        operation_days = get_all_weekdays()
                        break
                    else:
                        # some date added explicitly, hard to take into account
                        return False
                else:
                    continue
            if token == "do":
                # stop parsing
                break
            if token.isnumeric(): 
                current_weekday = int(token) - 1
                if negation:
                    operation_days.remove(current_weekday)
                operation_days.append(current_weekday)
        
        if len(operation_days) == 0:
            return False
        
        self.current_train["operation_days"] = operation_days

        return True

    
    def new_stop_on_route(self, row):
        stop_name = row[0] 
        stop_name = unidecode(stop_name).lower()

        arrival = row[3]
        departure = row[4]
        if departure is np.nan and arrival is np.nan:
            return
        if departure is np.nan:
            # TODO add 5 minutes
            departure = arrival
        if arrival is np.nan:
            # TODO remove 5 minutes
            arrival = departure
        
        if self.prev_time is not None:
            if self.prev_time > departure:
                self.days_passed += 1

        self.prev_time = departure

        length = row[5]
        self.current_min_max_length = min(self.current_min_max_length, length) if self.current_min_max_length is not None else length
        weight = row[6]
        self.current_min_max_weight = min(self.current_min_max_weight, weight) if self.current_min_max_weight is not None else weight
        max_speed = row[7]
        self.current_max_speed = max(self.current_max_speed, max_speed) if self.current_max_speed is not None else max_speed

        stop = {"stop_name": stop_name, "arrival": arrival, "departure": departure, "days_passed": self.days_passed}

        if self.current_route is None:
            self.current_route = []
        self.current_route.append(stop)
    
    def add_route(self):
        self.create_terminal_route()
        self.current_train["route"] = self.current_route
        self.current_train["max_speed"] = self.current_max_speed
        self.current_train["max_weight"] = self.current_min_max_weight
        self.current_train["max_length"] = self.current_min_max_length
    
    def solve_cetrebova_problem(self, stop_name):
        # manually solve the problem with ceska trebova
        if stop_name == "c.trebova vjezd.sk.":
            stop_name = "c.trebova odj.sk."
        return stop_name
    
    def is_terminal(self, stop_name):
        for terminal in self.terminal_names:
            if terminal["station_name"] == stop_name:
                return True
        return False
    
    def get_terminal_ID(self, stop_name):
        for terminal in self.terminal_names:
            if terminal["station_name"] == stop_name:
                return terminal["id"]
        return None
    
    def get_terminal_abbrev(self, stop_name):
        for terminal in self.terminal_names:
            if terminal["station_name"] == stop_name:
                return terminal["code"]
        return None

    def create_terminal_route(self):
        terminal_route = []

        self.prev_ctrebova = False
        for stop in self.current_route:
            stop_name = stop["stop_name"]
            stop_name = self.solve_cetrebova_problem(stop_name)
            if self.is_terminal(stop_name):
                stop["stop_id"] = self.get_terminal_ID(stop_name)
                stop["stop_code"] = self.get_terminal_abbrev(stop_name)
                # check for duplicate previous id
                if len(terminal_route) > 0 and terminal_route[-1]["stop_id"] == stop["stop_id"]:
                    continue
                terminal_route.append(stop)

        if len(terminal_route) > 0:
            self.current_train["hub_route"] = terminal_route

    def add_train_to_grafikon(self):
        if self.current_train is None:
            return False
        if "operation_days" not in self.current_train:
            return False
        if self.current_route is None:
            return False
        if self.current_max_speed is None:
            return False
        if self.current_min_max_weight is None:
            return False
        if self.current_min_max_length is None:
            return False
        
        self.add_route()
        self.create_terminal_route()

        self.grafikon["trains"].append(self.current_train)
        return True

if __name__ == "__main__":
    data_folder = pathlib.Path("METRANS_data")
    data_folder_listopad = data_folder / "listopad"
    out_folder = pathlib.Path("processed_data")
    out_folder.mkdir(parents=True, exist_ok=True)
    fname = data_folder_listopad / "MTRR_ND_Jⁿ_2023_finální_stav_z_22_11_2022.xlsx" # weird name, but whatever

    parser = GrafikonParser()
    grafikon = parser.parse(fname)

    with open(out_folder / "grafikon.json", "w") as f:
        json.dump(grafikon, f, indent=4)