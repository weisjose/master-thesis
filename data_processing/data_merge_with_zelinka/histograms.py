import json
import matplotlib.pyplot as plt

with open('processed_data/grafikon.json', 'r') as file:
    data = json.load(file)

max_lengths = [train['max_length'] for train in data['trains']]
plt.hist(max_lengths, bins=100, color='skyblue', edgecolor='black')

plt.xlabel('Max Length')
plt.ylabel('Frequency')
plt.title('Histogram of max length values')

plt.show()

max_weights = [train['max_weight'] for train in data['trains']]
plt.hist(max_weights, bins=100, color='skyblue', edgecolor='black')

plt.xlabel('Max weight')
plt.ylabel('Frequency')
plt.title('Histogram of max weight values')

plt.show()

max_speeds = [train['max_speed'] for train in data['trains']]
plt.hist(max_speeds, bins=100, color='skyblue', edgecolor='black')

plt.xlabel('Max speed')
plt.ylabel('Frequency')
plt.title('Histogram of max speed values')

plt.show()
