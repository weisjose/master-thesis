import json
import random

# Load the JSON data from the file
with open('processed_data/grafikon.json', 'r') as file:
    data = json.load(file)

max_lengths = [train['max_length'] for train in data['trains']]
max_weights = [train['max_weight'] for train in data['trains']]

length_counts = {}
weight_counts = {}
for length in max_lengths:
    length_counts[length] = length_counts.get(length, 0) + 1

for weight in max_weights:
    weight_counts[weight] = weight_counts.get(weight, 0) + 1

num_lengths = len(max_lengths)
lengths_probs = []
lengths_vals = []
lengths_probs_acc = []
prob_acc = 0

for val in length_counts.keys():
   lengths_probs.append(length_counts[val] / num_lengths)
   lengths_probs_acc.append(length_counts[val] / num_lengths + prob_acc)
   lengths_vals.append(val)
   prob_acc += length_counts[val] / num_lengths

num_weights = len(max_weights)
weight_probs = []
weight_vals = []
weight_probs_acc = []
prob_acc = 0

for val in weight_counts.keys():
   weight_probs.append(weight_counts[val] / num_weights)
   weight_probs_acc.append(weight_counts[val] / num_weights + prob_acc)
   weight_vals.append(val)
   prob_acc += weight_counts[val] / num_weights

def select_random_number(vals, probs_acc):
   rand = random.random()
   for i, prob_acc in enumerate(probs_acc):
      if rand < prob_acc:
         return vals[i]
   print("chyba")
   exit()


random_numbers = [select_random_number(lengths_vals, lengths_probs_acc) for _ in range(3000)]
#print("Randomly selected numbers:", random_numbers)
file_path = 'random_numbers_lengths.txt'

with open(file_path, 'w') as file:
    for number in random_numbers:
        file.write(str(number) + '\n')

print(f"lengths randomised saved to '{file_path}'.")

random_numbers = [select_random_number(weight_vals, weight_probs_acc) for _ in range(3000)]
#print("Randomly selected numbers:", random_numbers)
file_path = 'random_numbers_weights.txt'

with open(file_path, 'w') as file:
    for number in random_numbers:
        file.write(str(number) + '\n')

print(f"lengths randomised saved to '{file_path}'.")