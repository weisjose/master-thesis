# from docplex.cp.model import CpoModel
from docplex.mp.model import Model
import json
import pathlib

def get_incoming(station, train_slots):
    incoming = []
    for idx, train_slot in enumerate(train_slots["trains"]):
        if train_slot["hub_route"][-1]["station"] == station:
            incoming.append(idx)
    return incoming

def get_outgoing(station, train_slots):
    outgoing = []
    for idx, train_slot in enumerate(train_slots["trains"]):
        if train_slot["hub_route"][0]["station"] == station:
            outgoing.append(idx)
    return outgoing

def extract_terminals(train_slots):
    stations = []
    for train_slot in train_slots["trains"]:
        for stop in train_slot["hub_route"]:
            if stop["station"] not in stations:
                stations.append(stop["station"])

    return stations

def departure_inital(train_slots, idx):
    return train_slots["trains"][idx]["hub_route"][0]["departure_time"]

def arrival_final(train_slots, idx):
    return train_slots["trains"][idx]["hub_route"][-1]["arrival_time"]

# read the data
dataset_dir = pathlib.Path("datasets/three_trains_prepared")

with open(dataset_dir / "containers.json") as f:
    containers = json.load(f)

with open(dataset_dir / "grafikon.json") as f:
    train_slots = json.load(f)

with open(dataset_dir / "costs.json") as f:
    costs = json.load(f)

mdl = Model("container routing")
count_train_slots = len(train_slots["trains"])
count_containers = len(containers["containers"])

y = mdl.binary_var_list(count_train_slots, name=lambda i: "y_{}".format(i))
x = mdl.binary_var_matrix(count_containers, count_train_slots, name=lambda ij: "x_{}_{}".format(ij[0], ij[1]))

for c_i in range(count_containers):
    initial_station = containers["containers"][c_i]["start"]
    final_station = containers["containers"][c_i]["end"]
    final_incoming = get_incoming(final_station, train_slots)
    initial_outgoing = get_outgoing(initial_station, train_slots)

    # container starts at the first station
    mdl.add(mdl.sum(x[c_i, j] for j in final_incoming) == 1)
    # container ends at the last station
    mdl.add(mdl.sum(x[c_i, j] for j in initial_outgoing) == 1)

    # container leaves the first station after its release time
    release_time = containers["containers"][c_i]["release_time"]
    mdl.add(mdl.sum(x[c_i, j] * departure_inital(train_slots, j) for j in initial_outgoing) >= release_time)

    # container arrives at the last station before its deadline
    deadline = containers["containers"][c_i]["deadline"]
    mdl.add(mdl.sum(x[c_i, j] * arrival_final(train_slots, j) for j in final_incoming) <= deadline)

    stations = sorted(extract_terminals(train_slots))

    for s in stations:
        if s == final_station or s == initial_station:
            continue
        incoming = get_incoming(s, train_slots)
        outgoing = get_outgoing(s, train_slots)

        # container flows through the network
        mdl.add(mdl.sum(x[c_i, ko] for ko in outgoing) == mdl.sum(x[c_i, ki] for ki in incoming))

        # container leaves the station after it arrives
        arrival_time = mdl.sum(x[c_i, ko] * arrival_final(train_slots, ko) for ko in incoming)
        departure_time = mdl.sum(x[c_i, ki] * departure_inital(train_slots, ki) for ki in outgoing)
        mdl.add(arrival_time <= departure_time)

# max length
for g_j in range(count_train_slots):
    teu_capacity = train_slots["trains"][g_j]["teu_capacity"]
    mdl.add(mdl.sum(x[i, g_j] for i in range(count_containers)) <= teu_capacity)

# select the grafikon slots
for c_i in range(count_containers):
    for g_j in range(count_train_slots):
        mdl.add(x[c_i, g_j] <= y[g_j])

# create criterium
train_slots_cost = mdl.sum(y[j] * train_slots["trains"][j]["cost"] for j in range(count_train_slots))
manipulation_cost = mdl.sum(x[i, j] * costs["manipulation"] for i in range(count_containers) for j in range(count_train_slots))
mdl.minimize(train_slots_cost + manipulation_cost)

# solve
mdl.solve()
mdl.print_solution()

# print(mdl.export_as_lp_string())