# from data_loader import DataLoader
import pathlib
import json
import datetime
from datetime import timedelta

def time_string_to_hour_minute(time_string):
    hour, minute = map(int, time_string.split(':'))
    return hour, minute

def str_to_datetime(string):
    # TODO make sure the format is the same all over the place
    return datetime.datetime.strptime(string, "%Y-%m-%dT%H:%M:%S")

class DatasetPreparator:
    def __init__(self, data_path) -> None:
        # TODO probably add some more info about the dataset
        self.dataset_path = pathlib.Path(data_path)

        self.containers = self.load_json_file("containers.json")
        self.train_slots = self.load_json_file("grafikon.json")
        self.costs = self.load_json_file("costs.json")
        self.terminals = self.load_json_file("terminals.json")
        self.terminal_relations = self.load_json_file("terminal_relations.json")

        self.min_time = self._min_time()
        self.max_time = self._max_time()
        self.max_minutes = self._time_from_min(self.max_time)
        self.days_covered = self._get_days_covered()

        self._edit_container_times()
        self.exploded_train_slots = self._create_train_slots()
        self._create_slots_costs(self.exploded_train_slots)
    
    def _get_days_covered(self):
        return (self.max_time - self.min_time).days

    def load_json_file(self, file_name):
        with open(self.dataset_path / file_name) as f:
            return json.load(f)

    def _min_time(self):
        m = min(c["release_time"] for c in self.containers["containers"])
        return str_to_datetime(m)

    def _max_time(self):
        m = max(c["deadline"] for c in self.containers["containers"])
        return str_to_datetime(m)
    
    def _time_from_min(self, time):
        delta = time - self.min_time
        return int(delta.total_seconds() / 60)
    
    def _edit_container_times(self):
        for container in self.containers["containers"]:
            time = str_to_datetime(container["release_time"])
            container["release_time"] = self._time_from_min(time)
            time = str_to_datetime(container["deadline"])
            container["deadline"] = self._time_from_min(time)
    
    def _stop_times_to_minutes_from_min(self, cur_date, stop):
        arrival = stop["arrival_time"]
        arrival_hour, arrival_minute = time_string_to_hour_minute(arrival)
        arrival_date = cur_date.replace(hour=arrival_hour, minute=arrival_minute)
        departure = stop["departure_time"]
        departure_hour, departure_minute = time_string_to_hour_minute(departure)
        departure_date = cur_date.replace(hour=departure_hour, minute=departure_minute)

        # adding passed days throughout the schedule
        days_passed = stop["days_passed"]
        arrival_date += timedelta(days=days_passed)
        departure_date += timedelta(days=days_passed)

        arrival_minutes = self._time_from_min(arrival_date)
        departure_minutes = self._time_from_min(departure_date)
        stop["arrival_time"] = arrival_minutes
        stop["departure_time"] = departure_minutes

        return stop
    
    def _create_route(self, cur_date, route):
        route_with_minutes = []

        for stop in route:
            stop_with_minutes = self._stop_times_to_minutes_from_min(cur_date, stop)
            if stop_with_minutes["departure_time"] < 0:
                # print("min_violation", stop)
                continue
            if stop_with_minutes["arrival_time"] > self.max_minutes:
                # print("max_violation", stop, self.max_minutes)
                continue

            route_with_minutes.append(stop_with_minutes)

        return route_with_minutes
    
    def _create_exploded_train_slot(self, train, route):
        exploded_train = {}
        exploded_train["train_number"] = train["train_number"]
        exploded_train["hub_route"] = route
        exploded_train["max_speed"] = train["max_speed"]
        exploded_train["teu_capacity"] = train["teu_capacity"]
        exploded_train["carriage_count"] = train["carrige_count"]
        return exploded_train

    def _create_train_slots(self):
        all_trains = []

        for i in range(self.days_covered + 1):
            cur_date = self.min_time + timedelta(days=i)
            cur_weekday = (cur_date).weekday()
            for train in self.train_slots["trains"]:
                # check if the train is running on the current day
                if cur_weekday not in train["operation_days"]:
                    continue

                # create the route from stations that would have been inside the time windows
                route = self._create_route(cur_date, train["hub_route"])

                # not adding a route if it has only one stop
                if len(route) <= 1:
                    continue

                all_trains.append(self._create_exploded_train_slot(train, route))

        return {"trains": all_trains}
    
    def _create_slots_costs(self, exploded_train_slots):
        # find the duration of the slot * the cost of the locomotive + the carriages
        # find the distance of the slot

        for train_slot in exploded_train_slots["trains"]:
            slot_duration = train_slot["hub_route"][-1]["arrival_time"] - train_slot["hub_route"][0]["departure_time"]
            slot_duration = slot_duration / 60

            slot_distance = 0

            for i in range(len(train_slot["hub_route"]) - 1):
                cur_code = train_slot["hub_route"][i]["stop_code"]
                next_code = train_slot["hub_route"][i + 1]["stop_code"]
                slot_distance += self.terminal_relations[cur_code][next_code]["distance"]

            train_slot["cost"] = int(slot_duration * self.costs["train_usage"]) + slot_distance * self.costs["perkm"]

    def save_dataset(self, output_dir):
        def save_json_file(file_name, data):
            with open(output_dir / file_name, "w") as f:
                json.dump(data, f, indent=4)

        output_dir = pathlib.Path(output_dir)
        output_dir.mkdir(parents=True, exist_ok=True)

        save_json_file("containers.json", self.containers)
        save_json_file("grafikon.json", self.exploded_train_slots)
        save_json_file("costs.json", self.costs)


if __name__ == "__main__":
    data_path = "datasets/three_trains"
    print("Preparing dataset from", data_path)
    dataset = DatasetPreparator(data_path)
    print("Dataset prepared")

    output_path = "datasets/three_trains_prepared"
    print("Saving dataset to:", output_path)
    dataset.save_dataset(output_path)