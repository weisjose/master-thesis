import pandas as pd
import numpy as np
import json

from global_variables import path_to_terminal_matrix, path_to_finalized_timetables, path_to_terminal_matrix_filtered

def filter_unreachable_terminals():
    df = pd.read_csv(path_to_terminal_matrix)
    with open(path_to_finalized_timetables, 'r') as file:
        timetables = json.load(file)

    hub_route_array = []
    for i in range(len(timetables['trains'])):
        hub_route_array.append(timetables['trains'][i]['hub_route'])

    filtered_terminals = df['Unnamed: 0'].tolist()
    num_cities = len(filtered_terminals)
    new_num_cities = 0
    #termination condition - when during one iteration cycle the num_cities remains unchanged 
    while num_cities != new_num_cities:
        num_cities = new_num_cities
        #iteration through timetables, find which cities can be reached
        for i in range(len(timetables['trains'])):
            for k in range(len(hub_route_array[i])):
                starting_city = hub_route_array[i][k]["stop_code"]
                for j in range(k+1, len(hub_route_array[i])):
                    end_city = hub_route_array[i][j]["stop_code"]
                    df.loc[df['Unnamed: 0'] == starting_city, end_city] += 1
        filtered_rows = []
        only_import = []
        only_export = []
        #filter out the cities that cannot be reached
        for index, row in df.iterrows():
            city = row['Unnamed: 0']
            if row.iloc[1:].sum() != 0 or df[city].sum() != 0:
                filtered_rows.append(index)
            if row.iloc[1:].sum() != 0 and df[city].sum() == 0:
                only_export.append(df.loc[index][0])
            if row.iloc[1:].sum() == 0 and df[city].sum() != 0:
                only_import.append(df.loc[index][0])
        df = df.loc[filtered_rows]
        filtered_terminals = df['Unnamed: 0'].tolist()
        new_num_cities = len(filtered_terminals)
        df = df.loc[:, ['Unnamed: 0'] + filtered_terminals]

    #save only export and only import cities - might be useful later
    with open('only_export_cities.txt', 'w') as file:
        for city in only_export:
            file.write(city + '\n')
    with open('only_import_cities.txt', 'w') as file:
        for city in only_import:
            file.write(city + '\n')


    df = pd.DataFrame(0, index=filtered_terminals, columns=filtered_terminals)
    df.to_csv(path_to_terminal_matrix_filtered)
    print(f"CSV file with time matrix has been created: {path_to_terminal_matrix_filtered}")
if __name__=="__main__":
   filter_unreachable_terminals()