import pandas as pd
import numpy as np
import json
from collections import deque
from datetime import datetime

from global_variables import path_to_terminal_matrix_filtered, path_to_finalized_timetables, path_to_stops_in_routes

def find_paths(graph, start, end, super_dict):
        queue = deque([(start, [start])])
        paths = []
        while queue:
            current, path = queue.popleft()
            if len(path) > 5:
                break
            if current == end:
                paths.append(path)
            else:
                for neighbor in graph.get(current, []):
                    if neighbor not in path:
                        queue.append((neighbor, path + [neighbor]))
        for path in paths:
            code_string = ""
            for i, city in enumerate(path):
                if city == end:
                    continue
                code_string += city
                code_string_to_save = code_string + end
                if code_string_to_save not in super_dict:
                    super_dict[code_string_to_save] = [path[i+1]]
                else:
                    if path[i+1] not in super_dict[code_string_to_save]:
                        super_dict[code_string_to_save].append(path[i+1])
        return super_dict

def get_possible_stops():
    df = pd.read_csv(path_to_terminal_matrix_filtered)
    cities = df.columns[1:]
    df = pd.DataFrame(0, index=cities, columns=cities)
    with open(path_to_finalized_timetables, 'r') as file:
        timetables = json.load(file)

    hub_route_array = []
    list_of_neighboors = {}
    for city in cities:
        list_of_neighboors[city] = []
    for i in range(len(timetables['trains'])):
        hub_route_array.append(timetables['trains'][i]['hub_route'])
        #for starting city, find possible end cities and save them in list_of_neighboors[starting_city]
        for x in range(len(hub_route_array[i])):
            starting_city = hub_route_array[i][x]["stop_code"]
            for j in range(x+1, len(hub_route_array[i])):
                end_city = hub_route_array[i][j]["stop_code"]
                if end_city not in list_of_neighboors[starting_city]:
                    list_of_neighboors[starting_city].append(end_city)  
    
    #find all possible stops that we can take to get from city to city2
    super_dict = {}
    for city in cities:
        for city2 in cities:
            if city == city2:
                continue
            start_city = city
            end_city = city2
            super_dict = find_paths(list_of_neighboors, start_city, end_city, super_dict)

    with open(path_to_stops_in_routes, 'w') as f:
        json.dump(super_dict, f, indent=4)

#not used yet
def create_cut_time():
   
    with open('grafikony/grafikon_casy_komplet_filter.json', 'r') as file:
        timetables = json.load(file)

    with open('paths2.json', 'r') as file:
        possible_cesty = json.load(file)

    used_grafikons = np.zeros(len(timetables['trains']), dtype=int)
    grafikons_capacity = np.zeros(len(timetables['trains']), dtype=int)
    grafikons_capacity = grafikons_capacity * np.inf
    list_of_neighboors = {}
    df_cities = pd.read_csv('cities_matrix_filtered.csv')
    cities = df_cities.columns[1:]
    for city in cities:
        list_of_neighboors[city] = []

    hub_route_array = []
    grafikon_ids = []

    HOUR = 60
    DAY = HOUR * 24

    for i in range(len(timetables['trains'])):
        hub_route_array.append(timetables['trains'][i]['hub_route'])
        starting_city = hub_route_array[i][0]["stop_code"]
        grafikon_ids.append(timetables['trains'][i]['grafikon_ID'])
        parsed_date = datetime.strptime(hub_route_array[i][0]["departure"] , "%H:%M:%S")
        op_day = timetables['trains'][i]['operational_days'] - 1
        this_grafikon_time = op_day * DAY + parsed_date.hour * 60 + parsed_date.minute
        for j in range(1, len(hub_route_array[i])):
            end_city = hub_route_array[i][j]["stop_code"]
            list_of_neighboors[starting_city].append([end_city, grafikon_ids[i], this_grafikon_time, this_grafikon_time + 
                                                timetables['trains'][i]['hub_route'][j]["curr_total_minutes"]])
            
    valid_container_array_id = []
    total_cost = 0
    path_per_container = []
    grafikons_per_container = []
    container_id_array = []

    finish2 = {}
    for city in cities:
        for city2 in cities:
            if city == city2:
                continue
            print(city, city2)
            for i in range(0, 7):
                start_city = city
                end_city = city2

                odkud = start_city
                kam = end_city
                release_date = i
                deadline = 100
                first_cities = find_path5(list_of_neighboors, odkud, kam, release_date, deadline, possible_cesty)
                if first_cities != None:
                    finish2[city+city2+str(i)] = first_cities // DAY
                else:
                    finish2[city+city2+str(i)] = None
    with open('cut_time_paths.json', 'w') as f:
        json.dump(finish2, f, indent=4)
    
if __name__=="__main__":
   get_possible_stops()
   create_cut_time()