import pandas as pd

from global_variables import path_to_terminals, path_to_terminal_matrix

def create_terminals_matrix():
    df = pd.read_excel(path_to_terminals)
    terminals = df["ODKUD"].unique()
    adj_matrix = pd.DataFrame(0, index=terminals, columns=terminals)

    adj_matrix.to_csv(path_to_terminal_matrix)

    print("Adjacency matrix saved as terminals_matrix.csv.")

if __name__=="__main__":
   create_terminals_matrix()