import pandas as pd
import numpy as np
import json
from datetime import datetime
import copy
from collections import deque
from queue import PriorityQueue

from utilities.validator import validate_solution
from utilities.path_finder import find_path

def parse_date(date_str):
   formats_to_try = ["%Y-%m-%d %H:%M:%S", "%Y-%m-%d", "%H:%M:%S"]
   
   for fmt in formats_to_try:
      try:
         parsed_date = datetime.strptime(date_str, fmt)
         return parsed_date
      except ValueError:
         pass

   raise ValueError("Date string does not match any expected format")

def sort_by_second_element(item):
    return item[1]

def sort_by_time_difference(item):
    return item[0] - item[1]

def sort_by_terminal_then_time_difference(item):
    return item[3] * 10000 + (item[0] - item[1])

def convert_paths_and_grafikons_to_end_points(path_per_container, grafikons_per_container, grafikon_data):
   used_start_points_array = []
   used_end_points_array = []
   for x in range(len(grafikons_per_container)):
      curr_used_end_points_array = []
      curr_used_start_points_array = []
      if len(grafikons_per_container[x]) == 0:
         used_start_points_array.append(curr_used_start_points_array)
         used_end_points_array.append(curr_used_end_points_array)
         continue
      for i in range(len(grafikons_per_container[x])):
         curr_grafikon = grafikons_per_container[x][i]
         start = path_per_container[x][i]
         end = path_per_container[x][i + 1]
         start_point_idx = -1
         end_point_idx = -1
         for j in range(len(grafikon_data[curr_grafikon]['hub_route'])):
            if grafikon_data[curr_grafikon]['hub_route'][j]["stop_code"] == start:
               start_point_idx = j
            elif grafikon_data[curr_grafikon]['hub_route'][j]["stop_code"] == end:
               end_point_idx = j
         if x < 5:
            print(start_point_idx, end_point_idx, start, end, path_per_container[x], grafikons_per_container[x])
         if start_point_idx == -1 or end_point_idx == -1:
            print("ERROR: error in convertion")
            print(start_point_idx, end_point_idx, len(grafikon_data[curr_grafikon]['hub_route']), start, end,
                  path_per_container[x], grafikons_per_container[x], x)
            exit()
         curr_used_start_points_array.append(start_point_idx)
         curr_used_end_points_array.append(end_point_idx)
      used_start_points_array.append(curr_used_start_points_array)
      used_end_points_array.append(curr_used_end_points_array)
   return used_start_points_array, used_end_points_array
