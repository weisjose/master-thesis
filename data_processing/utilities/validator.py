import numpy as np
from datetime import datetime

def parse_date(date_str):
   formats_to_try = ["%Y-%m-%d %H:%M:%S", "%Y-%m-%d", "%H:%M:%S"]
   
   for fmt in formats_to_try:
      try:
         parsed_date = datetime.strptime(date_str, fmt)
         return parsed_date
      except ValueError:
         pass

   raise ValueError("Date string does not match any expected format")

def validate_solution(container_data, grafikon_data, container_id_array, used_grafikons_array,
                      used_start_points_array, used_end_points_array):
   HOUR = 60
   DAY = 24 * HOUR
   untransported_counter = 0
   valid_counter = 0
   all_counter = 0
   grafikons_capacity = np.zeros(len(grafikon_data), dtype=int)
   used_capacity = np.zeros(len(grafikon_data), dtype=int)
   for i in range(len(grafikon_data)):
      grafikons_capacity[i] = grafikon_data[i]['max_length'] // 12

   kam_array = []
   odkud_array = []
   teu_array = []
   deadline_array = []
   release_date_array = []
   start_of_month = datetime(2023, 1, 1)
   for index, row in container_data.iterrows():
      kam = row['KAM']
      odkud = row['ODKUD']
      teu_array = row['TEU']
      parsed_deadline = parse_date(row['DEADLINE'])
      parsed_release_time = parse_date(row['RELEASE_TIME'])
      time_difference_deadline = parsed_deadline - start_of_month
      time_difference_release_date = parsed_release_time - start_of_month
      deadline_in_days = int(max(0, time_difference_deadline.total_seconds() // 60 // DAY))
      release_date_in_days = int(max(0, time_difference_release_date.total_seconds() // 60 // DAY))
      kam_array.append(kam)
      odkud_array.append(odkud)
      deadline_array.append(deadline_in_days)
      release_date_array.append(release_date_in_days)

   for i in range(len(container_id_array)):
      idx = container_id_array[i]
      all_counter += 1
      if idx == None:
         untransported_counter += 1
         continue
      valid_counter += 1
      kam = kam_array[idx]
      odkud = odkud_array[idx]
      deadline = deadline_array[idx]
      release_date = release_date_array[idx]
      if len(used_grafikons_array[i]) == 0:
         print("ERROR: no grafikons were used")
         exit()
      last_destination = None
      curr_time = None
      for j in range(len(used_grafikons_array[i])):
         grafikon_id = used_grafikons_array[i][j]
         start_idx = used_start_points_array[i][j]
         end_idx = used_end_points_array[i][j]
         grafikon = grafikon_data[grafikon_id]
         start_of_grafikon = grafikon['hub_route'][start_idx]
         end_of_grafikon = grafikon['hub_route'][end_idx]
         curr_destination = end_of_grafikon["stop_code"]
         curr_start = start_of_grafikon["stop_code"]
         used_capacity[grafikon_id] += 1
         if j == 0:
            if start_of_grafikon["stop_code"] != odkud:
               print("ERROR: the starting terminal of container is not the same as the start of path")
               exit()

            if grafikon["operational_days"] - 1 < release_date:
               print("ERROR: the grafikon is used before container release date")
               exit()
            
            curr_time = (grafikon["operational_days"] - 1) * DAY + end_of_grafikon["curr_total_minutes"]

         if j != 0:
            if curr_start != last_destination:
               print("ERROR: the path is not continuous")
               exit()
            
            parsed_start_of_grafikon_dt = parse_date(start_of_grafikon["departure"])
            start_of_grafikon_dt_in_minutes = (parsed_start_of_grafikon_dt.hour * HOUR + parsed_start_of_grafikon_dt.minute
                                               + start_of_grafikon["days_passed"] * DAY)
            curr_time = (grafikon["operational_days"] - 1) * DAY + start_of_grafikon_dt_in_minutes
            if curr_time < last_time:
               print("ERROR: the start of this grafikon precedes the end of last grafikon")
               exit()

            curr_time = end_of_grafikon["curr_total_minutes"] + (grafikon["operational_days"] - 1) * DAY 

         if j == (len(used_grafikons_array[i]) - 1):
            if end_of_grafikon["stop_code"] != kam:
               print(kam, used_grafikons_array[i], end_of_grafikon["stop_code"], end_idx)
               print("ERROR: the ending terminal of container is not the same as the end of path")
               exit()

            if curr_time > deadline * DAY:
               print(curr_time, deadline_in_days, deadline_in_days * DAY)
               print(used_grafikons_array[i], odkud, kam, start_of_grafikon, end_of_grafikon, curr_destination, curr_start,
                     deadline, release_date)
               print("ERROR: the time spent on path exceeds the deadline")
               exit()


         last_destination = curr_destination
         last_time = curr_time
   for i in range(len(grafikon_data)):
      if used_capacity[i] > grafikons_capacity[i]:
         print("ERROR: the grafikon capacity is exceeded")
         exit()



def validate_solution2(container_data, grafikon_data, container_id_array, used_grafikons_array,
                      used_start_points_array, used_end_points_array):
   HOUR = 60
   DAY = 24 * HOUR
   untransported_counter = 0
   valid_counter = 0
   all_counter = 0
   grafikons_capacity = np.zeros(len(grafikon_data), dtype=int)
   used_capacity = []
   for i in range(len(grafikon_data)):
      grafikons_capacity[i] = grafikon_data[i]['max_length'] // 12
      temp_array = []
      used_capacity.append(temp_array)
      for j in range(1, len(grafikon_data[i]['hub_route'])):
         used_capacity[i].append(0)

   kam_array = []
   odkud_array = []
   teu_array = []
   deadline_array = []
   release_date_array = []
   start_of_month = datetime(2023, 1, 1)
   for index, row in container_data.iterrows():
      kam = row['KAM']
      odkud = row['ODKUD']
      teu_array = row['TEU']
      parsed_deadline = parse_date(row['DEADLINE'])
      parsed_release_time = parse_date(row['RELEASE_TIME'])
      time_difference_deadline = parsed_deadline - start_of_month
      time_difference_release_date = parsed_release_time - start_of_month
      deadline_in_days = int(max(0, time_difference_deadline.total_seconds() // 60 // DAY))
      release_date_in_days = int(max(0, time_difference_release_date.total_seconds() // 60 // DAY))
      kam_array.append(kam)
      odkud_array.append(odkud)
      deadline_array.append(deadline_in_days)
      release_date_array.append(release_date_in_days)

   for i in range(len(container_id_array)):
      idx = container_id_array[i]
      all_counter += 1
      if idx == None:
         untransported_counter += 1
         continue
      valid_counter += 1
      kam = kam_array[idx]
      odkud = odkud_array[idx]
      deadline = deadline_array[idx]
      release_date = release_date_array[idx]
      if len(used_grafikons_array[i]) == 0:
         print("ERROR: no grafikons were used")
         exit()
      last_destination = None
      curr_time = None
      for j in range(len(used_grafikons_array[i])):
         grafikon_id = used_grafikons_array[i][j]
         start_idx = used_start_points_array[i][j]
         end_idx = used_end_points_array[i][j]
         grafikon = grafikon_data[grafikon_id]
         start_of_grafikon = grafikon['hub_route'][start_idx]
         end_of_grafikon = grafikon['hub_route'][end_idx]
         curr_destination = end_of_grafikon["stop_code"]
         curr_start = start_of_grafikon["stop_code"]
         print(curr_start, curr_destination, odkud, kam)
         for x in range(start_idx, end_idx):
            used_capacity[grafikon_id][x] += 1
         if j == 0:
            if start_of_grafikon["stop_code"] != odkud:
               print("ERROR: the starting terminal of container is not the same as the start of path")
               exit()

            if (grafikon["operational_days"] - 1) + start_of_grafikon["days_passed"] < release_date:
               print("ERROR: the grafikon is used before container release date")
               exit()
            
            curr_time = (grafikon["operational_days"] - 1) * DAY + end_of_grafikon["curr_total_minutes"]

         if j != 0:
            if curr_start != last_destination:
               print("ERROR: the path is not continuous")
               exit()
            
            parsed_start_of_grafikon_dt = parse_date(start_of_grafikon["departure"])
            start_of_grafikon_dt_in_minutes = (parsed_start_of_grafikon_dt.hour * HOUR + parsed_start_of_grafikon_dt.minute
                                               + start_of_grafikon["days_passed"] * DAY)
            curr_time = (grafikon["operational_days"] - 1) * DAY + start_of_grafikon_dt_in_minutes
            if curr_time < last_time:
               print("ERROR: the start of this grafikon precedes the end of last grafikon")
               print(curr_time, last_time, grafikon_id, used_grafikons_array[i])
               exit()

            curr_time = end_of_grafikon["curr_total_minutes"] + (grafikon["operational_days"] - 1) * DAY 

         if j == (len(used_grafikons_array[i]) - 1):
            if end_of_grafikon["stop_code"] != kam:
               print(kam, used_grafikons_array[i], end_of_grafikon["stop_code"], end_idx)
               print(odkud, used_grafikons_array[i], i)
               print("ERROR: the ending terminal of container is not the same as the end of path")
               exit()

            if curr_time > deadline * DAY:
               print(curr_time, deadline_in_days, deadline_in_days * DAY)
               print(used_grafikons_array[i], odkud, kam, start_of_grafikon, end_of_grafikon, curr_destination, curr_start,
                     deadline, release_date)
               print("ERROR: the time spent on path exceeds the deadline")
               exit()


         last_destination = curr_destination
         last_time = curr_time
   for i in range(len(grafikon_data)):
      for j in range(len(used_capacity[i])):
         if used_capacity[i][j] > grafikons_capacity[i]:
            print(used_capacity[i], grafikons_capacity[i], i)
            print("ERROR: the grafikon capacity is exceeded")
            exit()
