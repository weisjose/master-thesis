import pandas as pd
import os

from global_variables import path_to_import_filtered_w_paths, path_to_export_filtered_w_paths, path_to_kontejnery_filtered_w_paths 

from creating_timetables.import_kontejnery_filter_w_paths import parse_import_file
from creating_timetables.export_kontejnery_filter_w_paths import parse_export_file

def parse_import_and_export_file_paths():
   parse_import_file()
   parse_export_file()
   df1 = pd.read_csv(path_to_import_filtered_w_paths)
   df2 = pd.read_csv(path_to_export_filtered_w_paths)

   merged_df = pd.concat([df1, df2], ignore_index=True)
   merged_df.to_csv(path_to_kontejnery_filtered_w_paths, index=False)

   print(f"MERGED CONTAINERS file has been created: {path_to_kontejnery_filtered_w_paths}")

if __name__=="__main__":
   parse_import_and_export_file_paths()