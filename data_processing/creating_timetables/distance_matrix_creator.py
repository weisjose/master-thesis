import pandas as pd
from geopy.distance import geodesic

from global_variables import path_to_terminals, path_to_distances, path_to_distance_matrix

def create_distance_matrix():
    df = pd.read_excel(path_to_terminals)

    #get a list of terminals
    terminals = df["ODKUD"].unique()

    #create an new adjacency matrix
    adj_matrix = pd.DataFrame(0, index=terminals, columns=terminals)
    
    #get incomplete distance adjacency matrix
    df2 = pd.read_excel(path_to_distances)

    #iterate through incomplete distance adjacency matrix, write data to adj_matrix
    for index, row in df2.iterrows():
        city1, city2, distance = row['ODKUD'], row['KAM'], row['VZDALENOST']
        if distance != 0:
            adj_matrix.loc[city1, city2] = distance
            adj_matrix.loc[city2, city1] = distance
    
    #add distances based on gps locations
    for terminal in terminals:
        for terminal2 in terminals:
            if adj_matrix.at[terminal, terminal2] == 0:
                
                #find indices of terminal and terminal2
                row_with_terminal = df.index[df['ODKUD'] == terminal].min()
                row_with_terminal2 = df.index[df['ODKUD'] == terminal2].min()

                #find gps values of terminal and terminal2
                gps_value = df.loc[row_with_terminal, 'GPS']
                gps_value2 = df.loc[row_with_terminal2, 'GPS']

                #check if gps_value == "no". It means that the terminal is unknown and could not be located
                if gps_value == "no" or gps_value2 == "no":
                    adj_matrix.at[terminal, terminal2] = pd.NA
                    adj_matrix.at[terminal2, terminal] = pd.NA 
                    continue

                #split the gps_value string into string parts
                gps_value_parts = gps_value.split(',')
                gps_value_parts2 = gps_value2.split(',')

                #get float values from gps_value_parts
                latitude, longitude = float(gps_value_parts[0]), float(gps_value_parts[1])
                latitude2, longitude2 = float(gps_value_parts2[0]), float(gps_value_parts2[1])

                #create gps points
                point1 = (latitude, longitude)
                point2 = (latitude2, longitude2)

                #calculate distance using geodesic from geopy.distance
                distance = round(geodesic(point1, point2).kilometers)

                #write values to adj_matrix
                adj_matrix.at[terminal, terminal2] = distance
                adj_matrix.at[terminal2, terminal] = distance

    adj_matrix.to_csv(path_to_distance_matrix)

    print(f"CSV file with distance matrix has been created: {path_to_distance_matrix}")

if __name__=="__main__":
    create_distance_matrix()