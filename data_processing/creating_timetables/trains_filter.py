import pandas as pd

from global_variables import path_to_trains, path_to_trains_filtered

def parse_train_file():
   df = pd.read_excel(path_to_trains)

   #keep only useful columns
   filtered_df = df[['CISLO_VLAKU', 'ODJEZD', 'REAL_ARRIVAL', 'ODKUD', 'KAM']]
   filtered_df = filtered_df.rename(columns={'REAL_ARRIVAL': 'PRIJEZD'})

   #save to csv file
   filtered_df.to_csv(path_to_trains_filtered, index=False)

   print(f"VLAKY FILTERED CSV file has been created: {path_to_trains_filtered}")


if __name__=="__main__":
   parse_train_file()