import pandas as pd
import numpy as np
import os

from global_variables import path_to_import, path_to_import_filtered_w_paths, path_to_trains

def parse_import_file():
    df = pd.read_excel(path_to_import)
    df_valid_train = pd.read_excel(path_to_trains)

    #remove reduntant spaces in data
    df['ODKUD_VLAK'] = df['ODKUD_VLAK'].str.replace(' ', '')
    df['KAM_VLAK'] = df['KAM_VLAK'].str.replace(' ', '')
    df['PRISTAV'] = df['PRISTAV'].str.replace(' ', '')
    df['VYKLADKOVY_TERMINAL'] = df['VYKLADKOVY_TERMINAL'].str.replace(' ', '')
    df['TERMINAL_USKLADNENI'] = df['TERMINAL_USKLADNENI'].str.replace(' ', '')

    #consider only containers which are loaded ("lozeny" means loaded)
    df = df[df['PRAZDNY_LOZENY'] == 'LOZENY'][['CONTAINER', 'PRISTAV', 'VYKLADKOVY_TERMINAL', 
                                                        'ODKUD_VLAK', 'KAM_VLAK', 'CISLO_VLAKU', 
                                                                'DATUM_VYKLADKY', 'PRIJEZD_LODI_DO_PRISTAVU']]
    
    #filter the containers for which we do not know the source and destination terminals
    df = df[df['PRISTAV'] != 'NEZNAM']
    df = df[df['PRISTAV'] != '????']
    df = df[df['VYKLADKOVY_TERMINAL'] != 'NEZNAM']
    df = df[df['VYKLADKOVY_TERMINAL'] != '????']
    
    #rename containers ID (one container may be transported more times during month, we need to distinguish these cases)
    #counter 100000 because the df will be merged later with containers from "export.xlsx" file
    last_container_name = None
    counter = 0
    invalid_container_ids = []
    for index, row in df.iterrows():
        current_container_name = row['CONTAINER']
        if current_container_name == last_container_name:
            counter += 0  
        else:
            counter += 1
            if pd.isna(row['CISLO_VLAKU']):
                invalid_container_ids.append(counter)
        df.at[index, 'CONTAINER'] = counter
        last_container_name = current_container_name 
    counter = 0

    df = df[~df['CONTAINER'].isin(invalid_container_ids)]
    
    #filter containers which do not stop at the 'VYKLADKOVY_TERMINAL'
    valid_container_ids = df['CONTAINER'][
        (df['KAM_VLAK'] == df['VYKLADKOVY_TERMINAL']) & (df['CONTAINER'].shift(-1) != df['CONTAINER'])
    ].unique()
    df = df[df['CONTAINER'].isin(valid_container_ids)]
    
    #filter containers which are transported by a train that is not in the data
    valid_train_ids = np.array(df_valid_train['CISLO_VLAKU'].tolist())
    invalid_container_ids2 = df.loc[
        ~df['CISLO_VLAKU'].isin(valid_train_ids) & ~df['CISLO_VLAKU'].isnull(),
        'CONTAINER'
    ].unique()
    df = df[~df['CONTAINER'].isin(invalid_container_ids2)]

    #save to csv file
    csv_file_path = 'processed_data_timetables/import_filtered_w_paths.csv'
    df.to_csv(csv_file_path, index=False)

    #rename the columns
    df = df.rename(columns={'PRISTAV': 'SOURCE'})
    df = df.rename(columns={'VYKLADKOVY_TERMINAL': 'DESTINATION'})
    df = df.rename(columns={'PRIJEZD_LODI_DO_PRISTAVU': 'RELEASE_TIME'})
    df = df.rename(columns={'DATUM_VYKLADKY': 'DEADLINE'})

    #drop duplicate rows
    df = df.drop_duplicates()

    #save to csv file
    df.to_csv(path_to_import_filtered_w_paths, index=False)

    print(f"IMPORT CONTAINERS file has been created: {path_to_import_filtered_w_paths}")

if __name__=="__main__":
    parse_import_file()
