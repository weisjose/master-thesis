import pandas as pd
import json

from global_variables import path_to_kontejnery_filtered_w_paths, path_to_timetable_routes

def get_train_routes():
    df = pd.read_csv('processed_data_timetables/kontejnery_filtered_w_paths.csv')

    route = dict()
    curr_cislo_vlaku = ""
    last_container_name = None
    rows_with_train = []
    rows_without_train = []
    #iterate through each row, remember rows with train number and without
    #allocate rows_without train to the correct rows_with_train_number and save it as timetable
    for index, row in df.iterrows():
        #if there is a train assigned to the row
        if not pd.isna(row['CISLO_VLAKU']):
            curr_cislo_vlaku = row['CISLO_VLAKU']
            #check if the current row container does not correspond to the last_container_name (container from last row)
            if last_container_name != row['CONTAINER']:
                #if there are no rows without train, add rows_with_train to route
                if len(rows_without_train) == 0 and len(rows_with_train) >= 1:
                    for i, roww in enumerate(rows_with_train):
                        if rows_with_train[i][1] in route:
                            route[rows_with_train[i][1]].append(rows_with_train[i][0])
                        else:
                            route[rows_with_train[i][1]] = []
                            route[rows_with_train[i][1]].append(rows_with_train[i][0])

                #otherwise, iterate through rows_with_train and try to assign it to rows_without_train
                else:
                    for i, roww in enumerate(rows_with_train):
                        terminals = roww[0]
                        train = roww[1]
                        #if starting terminal (terminals[0]) is the starting stop in rows_without_train
                        if terminals[0] == rows_without_train[0]:
                            #if end terminal (terminals[1]) in stops in rows_without_train, exit with error
                            if terminals[1] in rows_without_train:
                                print("error 3")
                                exit()
                            
                            #otherwise, append end terminal to rows_without_train stops and add to route
                            rows_without_train.append(terminals[1])
                            if train in route:
                                route[train].append(rows_without_train)
                            else:
                                route[train] = []
                                route[train].append(rows_without_train)
                            
                            #since we already assigned rows_without_train, all other rows_with_train
                            #are added to route as 2-stop long timetables
                            for k, roww2 in enumerate(rows_with_train):
                                if k == i:
                                    continue
                                terminals2 = roww2[0]
                                train2 = roww2[1]
                                if train in route:
                                    route[train2].append(terminals2)
                                else:
                                    route[train2] = []
                                    route[train2].append(terminals2)
                            break
                        #if end terminal (terminals[1]) is the end stop in rows_without_train
                        elif terminals[1] == rows_without_train[-1]:
                            #if staring terminal (terminals[0]) in stops in rows_without_train, exit with error
                            if terminals[0] in rows_without_train:
                                print("error 3")
                                exit()

                            #otherwise, insert end terminal to rows_without_train stops 
                            #as second-last element and add to route
                            rows_without_train.insert(0, terminals[0])
                            if train in route:
                                route[train].append(rows_without_train)
                            else:
                                route[train] = []
                                route[train].append(rows_without_train)
                            #since we already assigned rows_without_train, all other rows_with_train
                            #are added to route as 2-stop long timetables
                            for k, roww2 in enumerate(rows_with_train):
                                if k == i:
                                    continue
                                terminals2 = roww2[0]
                                train2 = roww2[1]
                                if train in route:
                                    route[train2].append(terminals2)
                                else:
                                    route[train2] = []
                                    route[train2].append(terminals2)
                            break
                #since the container on this row differs from the last rows container, reset arrays
                rows_without_train = []
                rows_with_train = [([row['ODKUD_VLAK'], row['KAM_VLAK']], curr_cislo_vlaku)]

            #if the container on this row is same as the container on last row
            else:
                #add this row to rows_with_train
                rows_with_train.append(([row['ODKUD_VLAK'], row['KAM_VLAK']], curr_cislo_vlaku))

                #if length of rows_without_train array is over 0
                if len(rows_without_train) > 0:
                    #iterate and try to assign rows_without_train to some row in rows_with_train
                    for i, roww in enumerate(rows_with_train):
                        terminals = roww[0]
                        train = roww[1]

                        #if starting terminal (terminals[0]) is the starting stop in rows_without_train
                        if terminals[0] == rows_without_train[0]:
                            #if end terminal (terminals[1]) in stops in rows_without_train, exit with error
                            if terminals[1] in rows_without_train:
                                print("error 3")
                                exit()
                            rows_without_train.append(terminals[1])
                            if train in route:
                                route[train].append(rows_without_train)
                            else:
                                route[train] = []
                                route[train].append(rows_without_train)
                            rows_with_train.pop(i)
                            break
                        
                        #if end terminal (terminals[1]) is the end stop in rows_without_train
                        elif terminals[1] == rows_without_train[-1]:
                            #if starting terminal (terminals[0]) in stops in rows_without_train, exit with error
                            if terminals[0] in rows_without_train:
                                print("error 3")
                                exit()
                            #otherwise, insert end terminal to rows_without_train stops 
                            #as second-last element and add to route
                            rows_without_train.insert(0, terminals[0])
                            if train in route:
                                route[train].append(rows_without_train)
                            else:
                                route[train] = []
                                route[train].append(rows_without_train)
                            break

                rows_without_train = []

            last_container_name = row['CONTAINER']

        #there is not a train assigned to the row
        else:
            #if rows_without_train array has no stops, add 'ODKUD_VLAK' and 'KAM_VLAK' as current end points
            if len(rows_without_train) == 0:
                rows_without_train = [row['ODKUD_VLAK'], row['KAM_VLAK']]
            else:
                #if the last stop in rows_without_train is not the start of current row, print error and exit
                if row['ODKUD_VLAK'] != rows_without_train[-1]:
                    print(rows_without_train, row['CONTAINER'])
                    print("error")
                    exit()
                #otherwise add the end of current row to the end of the rows_without_train array
                rows_without_train.append(row['KAM_VLAK'])

    #for every "route" key (train number), pick the route with the most stops
    route_keys = route.keys()
    to_pop = []
    for key in route_keys:
        max_len = 1
        best_array = None
        for candidate in route[key]:
            curr_len = len(candidate)
            if curr_len > max_len:
                best_array = candidate
                max_len = curr_len
        #check if a route has stops with identical names (should be happening only once) CETR - LINZ - LINZ for example
        for i, elmnt in enumerate(best_array):
            if elmnt in best_array[i+1:]:
                to_pop.append(key)
                break
        route[key] = best_array

    #remove key with route stops with identical names
    for key_to_pop in to_pop:
        route.pop(key_to_pop)
    
    #save to csv file
    with open(path_to_timetable_routes, "w") as f:
        json.dump(route, f, indent=4)

if __name__=="__main__":
   get_train_routes()