import pandas as pd
import numpy as np

from global_variables import path_to_export, path_to_export_filtered_w_paths, path_to_trains

def parse_export_file():
    df = pd.read_excel(path_to_export)
    df_valid_train = pd.read_excel(path_to_trains)

    #remove reduntant spaces in data
    df['ODKUD_VLAK'] = df['ODKUD_VLAK'].str.replace(' ', '')
    df['KAM_VLAK'] = df['KAM_VLAK'].str.replace(' ', '')
    df['PRISTAV'] = df['PRISTAV'].str.replace(' ', '')
    df['NAKLADKOVY_TERMINAL'] = df['NAKLADKOVY_TERMINAL'].str.replace(' ', '')
    df['TERMINAL_UVOLNENI'] = df['TERMINAL_UVOLNENI'].str.replace(' ', '')

    #consider only containers which are loaded ("lozeny" means loaded)
    df = df[df['PRAZDNY_LOZENY'] == 'LOZENY']
    
    #filter the containers for which we do not know the source and destination terminals
    df = df[df['NAKLADKOVY_TERMINAL'] != '????']
    df = df[df['NAKLADKOVY_TERMINAL'] != 'NEZNAM']
    df = df[df['PRISTAV'] != 'NEZNAM']
    df = df[df['PRISTAV'] != '????']
    
    #rename containers ID (one container may be transported more times during month, we need to distinguish these cases)
    last_container_name = None
    counter = 100000
    invalid_container_ids = []
    for index, row in df.iterrows():
        current_container_name = row['CONTAINER']
        if current_container_name == last_container_name:
            counter += 0  
        else:
            counter += 1
            if pd.isna(row['CISLO_VLAKU']):
                invalid_container_ids.append(counter)
        df.at[index, 'CONTAINER'] = counter
        last_container_name = current_container_name 
    counter = 0

    print(len(invalid_container_ids))
    df = df[~df['CONTAINER'].isin(invalid_container_ids)]

    #filter containers which do not stop at the 'PRISTAV'
    valid_container_ids = df['CONTAINER'][
        (df['KAM_VLAK'] == df['PRISTAV']) & (df['CONTAINER'].shift(-1) != df['CONTAINER'])
    ].unique()
    df = df[df['CONTAINER'].isin(valid_container_ids)]

    #filter containers which are transported by a train that is not in the data
    valid_train_ids = np.array(df_valid_train['CISLO_VLAKU'].tolist())
    invalid_container_ids2 = df.loc[
        ~df['CISLO_VLAKU'].isin(valid_train_ids) & ~df['CISLO_VLAKU'].isnull(),
        'CONTAINER'
    ].unique()
    df = df[~df['CONTAINER'].isin(invalid_container_ids2)]

    #rename the columns
    df = df.rename(columns={'NAKLADKOVY_TERMINAL': 'SOURCE'})
    df = df.rename(columns={'PRISTAV': 'DESTINATION'})
    df = df.rename(columns={'PRISTAVENI_DO_PRISTAVU': 'DEADLINE'})
    df = df.rename(columns={'DATUM_NAKLADKY': 'RELEASE_TIME'})

    #drop duplicate rows
    df = df.drop_duplicates()

    #save to csv file
    df.to_csv(path_to_export_filtered_w_paths, index=False)

    print(f"EXPORT CONTAINERS file has been created: {path_to_export_filtered_w_paths}")

if __name__=="__main__":
    parse_export_file()
