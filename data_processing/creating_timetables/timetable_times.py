import pandas as pd
import json
from datetime import datetime

from global_variables import path_to_trains_filtered, path_to_timetable_routes, path_to_timetables_with_times

def add_times_to_trains():
    df = pd.read_csv(path_to_trains_filtered)
    with open(path_to_timetable_routes) as file:
        timetable_routes = json.load(file)
    times = dict()
    #iterate through each row in filtered train file
    for index, row in df.iterrows():
        train_number = row['CISLO_VLAKU']
        if train_number not in timetable_routes:
            continue
        departure = row['ODJEZD']
        arrival = row['PRIJEZD']
        dt = datetime.strptime(departure, "%Y-%m-%d %H:%M:%S")
        if dt.year != 2023 or dt.month != 1:
            print("chyba")
            exit()
        current_train = dict()
        current_train["route"] = timetable_routes[train_number]
        current_train["arrival"] = arrival
        current_train["departure"] = departure
        times[train_number] = current_train

    with open(path_to_timetables_with_times, "w") as f:
        json.dump(times, f, indent=4)

if __name__=="__main__":
    add_times_to_trains()