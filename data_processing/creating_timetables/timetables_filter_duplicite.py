import json
import pandas as pd
import numpy as np
import math
from datetime import datetime, timedelta
import copy

from global_variables import path_to_merged_timetables

def filter_duplicite_timetables(timetables = None):
   if timetables == None:
      with open(path_to_merged_timetables, 'r') as file:
         timetables = json.load(file)

   num_timetables = len(timetables["trains"])
   grafikons_to_drop = []

   #iterate through first 7 days in january 2023 (1: SUNDAY, 2:MONDAY, ..., 7: SATURDAY)
   for i in range(1, 8):
      cities_dict = []
      cities_dict_j = []
      #iterate through each timetable
      for j in range(num_timetables):
         if timetables["trains"][j]["operational_days"] != i:
            continue
         hub_route = timetables["trains"][j]["hub_route"]
         start_code = hub_route[0]["stop_code"]
         start_time = hub_route[0]["departure"]

         #iterate through stops of timetable route
         for y in range(1, len(hub_route)):
            end_code = hub_route[y]["stop_code"]
            total_code = start_code + end_code + start_time
            if total_code in cities_dict:
               index = cities_dict.index(total_code)
               if (len(timetables["trains"][j]["hub_route"]) > len(timetables["trains"][cities_dict_j[index]]["hub_route"])
                   and cities_dict_j[index] not in grafikons_to_drop):
                  #if i <= 3, the week name is in month calendar 5 times
                  if i <= 3:
                     for x in range(5):
                        grafikons_to_drop.append(cities_dict_j[index] + x)
                  #otherwise the week name is in month calendar 4 times
                  else:
                     for x in range(4):
                        grafikons_to_drop.append(cities_dict_j[index] + x)
                  cities_dict_j[index] = j
               else:
                  #if i <= 3, the week name is in month calendar 5 times
                  if i <= 3:
                     for x in range(5):
                        grafikons_to_drop.append(j + x)
                  #otherwise the week name is in month calendar 4 times
                  else:
                     for x in range(4):
                        grafikons_to_drop.append(j + x)
               break
            else:
               cities_dict.append(total_code)
               cities_dict_j.append(j)

   grafikons_to_drop = sorted(grafikons_to_drop)
   unique_values = set(grafikons_to_drop)

   # Check if the length of the set is less than the length of the original list
   # If it is, it means there are duplicate values
   if len(unique_values) < len(grafikons_to_drop):
      print("There is a value occurring more than once.")
   else:
      print("All values are unique.")

   count_dict = {}

   # Iterate over the list and count the occurrences of each value
   for num in grafikons_to_drop:
      if num in count_dict:
         count_dict[num] += 1
      else:
         count_dict[num] = 1

   # Check which values occur more than once
   duplicate_values = [num for num, count in count_dict.items() if count > 1]

  # print("Values occurring more than once:", duplicate_values)
   counter = 0
   for j in range(len(grafikons_to_drop)):
      timetables["trains"].pop(grafikons_to_drop[j] - counter)
      counter += 1

   counter = 0
   for j in range(len(timetables["trains"])):
      timetables["trains"][j]["grafikon_ID"] = counter
      counter += 1
      
   return timetables

if __name__=="__main__":
   filter_duplicite_timetables()