import json
import pandas as pd
import numpy as np
import math
from datetime import datetime, timedelta
import copy
import sys
import os

from creating_timetables.timetables_filter_duplicite import filter_duplicite_timetables
from creating_timetables.timetables_merge import merge_timetables
from global_variables import path_to_timetables_with_times, path_to_random_lengths, path_to_random_weights, \
                              path_to_distance_matrix, path_to_finalized_timetables
from global_variables import DAY, HOUR

DEFAULT_LENGTH = 660
DEFAULT_WEIGHT = 2000

def finalize_timetables():

   #date to week conversion for january 2023
   date_to_week_name = {1: "SUN", 2:"MON", 3:"TUE", 4:"WED", 5:"THU", 6:"FRI", 7:"SAT", 
                        8: "SUN", 9:"MON", 10:"TUE", 11:"WED", 12:"THU", 13:"FRI", 14:"SAT", 
                        15: "SUN", 16:"MON", 17:"TUE", 18:"WED", 19:"THU", 20:"FRI", 21:"SAT", 
                        22: "SUN", 23:"MON", 24:"TUE", 25:"WED", 26:"THU", 27:"FRI", 28:"SAT", 
                        29: "SUN", 30:"MON", 31:"TUE"}

   week_name_to_date = {"SUN":[1, 8, 15, 22, 29], "MON":[2, 9, 16, 23, 30], "TUE":[3, 10, 17, 24, 31],
                        "WED":[4, 11, 18, 25], "THU":[5, 12, 19, 26], "FRI":[6, 13, 20, 27],
                        "SAT":[7, 14, 21, 28], "THU":[5, 12, 19, 26], "FRI":[6, 13, 20, 27]}

   with open(path_to_timetables_with_times, 'r') as file:
      timetable_routes = json.load(file)


   num_timetables = len(timetable_routes)
   list_timetable_routes = list(timetable_routes)
   timetables = {}

   df_distance_matrix = pd.read_csv(path_to_distance_matrix)

   timetables["trains"] = []
   counter = 0
   for i in range(num_timetables):
      curr_grafikon = {}
      curr_train_id = list_timetable_routes[i]
      curr_grafikon["train_number"] = curr_train_id
      curr_grafikon["grafikon_ID"] = counter
      op_days = []
      curr_grafikon["operational_days"] = op_days
      hub_route = []
      is_valid = True
      distance_sum = 0 
      total_minutes = 0 
      start_date = 0
      #iterate through each stop in route for given curr_train_id
      for idx, stop in enumerate(timetable_routes[curr_train_id]["route"]):
         curr_stop_dict = {}

         #if it's the start of the route
         if idx == 0:
            parsed_date = datetime.strptime(timetable_routes[curr_train_id]["departure"] , "%Y-%m-%d %H:%M:%S")
            parsed_date_init_departure = datetime.strptime(timetable_routes[curr_train_id]["departure"] , "%Y-%m-%d %H:%M:%S")
            time = parsed_date.strftime("%H:%M:%S")
            op_days = week_name_to_date[date_to_week_name[parsed_date.day]]
            curr_grafikon["operational_days"] = op_days
            total_minutes = 0
            start_date = parsed_date

            curr_stop_dict["arrival"] = time
            curr_stop_dict["departure"] = time
            curr_stop_dict["days_passed"] = 0

         #if it's the last stop in the route
         elif idx == len(timetable_routes[curr_train_id]["route"]) - 1:
            if pd.isna(timetable_routes[curr_train_id]["arrival"]):
               is_valid = False
               break

            previous_stop_index = df_distance_matrix.columns.get_loc(timetable_routes[curr_train_id]["route"][idx - 1])
            curr_stop_index = df_distance_matrix.columns.get_loc(stop)
            distance = df_distance_matrix.iloc[previous_stop_index - 1, curr_stop_index] 
            if pd.isna(distance):
               is_valid = False
               break
            distance_sum += distance

            parsed_date = datetime.strptime(timetable_routes[curr_train_id]["arrival"] , "%Y-%m-%d %H:%M:%S")
            time = parsed_date.strftime("%H:%M:%S")
            time_difference = parsed_date - start_date
            total_minutes = time_difference.total_seconds() // 60

            curr_stop_dict["curr_total_minutes"] = total_minutes

            curr_stop_dict["arrival"] = time
            curr_stop_dict["departure"] = time
            curr_stop_dict["days_passed"] = ((parsed_date_init_departure.hour * HOUR + \
                                              parsed_date_init_departure.minute) + total_minutes) // DAY
            
         #if the stop is not the start and not the end
         else:
            previous_stop_index = df_distance_matrix.columns.get_loc(timetable_routes[curr_train_id]["route"][idx - 1])
            curr_stop_index = df_distance_matrix.columns.get_loc(stop)
            distance = df_distance_matrix.iloc[previous_stop_index - 1, curr_stop_index] 
            if pd.isna(distance):
               is_valid = False
               break
            converted_distance_to_time = int(distance * 0.6)
            new_date = parsed_date + timedelta(minutes=converted_distance_to_time + total_minutes)
            time_difference = new_date - start_date
            total_minutes = time_difference.total_seconds() // 60

            hours = new_date.hour
            minutes = new_date.minute

            time_string = "{:02d}:{:02d}:00".format(hours, minutes)
            curr_stop_dict["arrival"] = time_string
            curr_stop_dict["departure"] = time_string
            curr_stop_dict["curr_total_minutes"] = total_minutes
            distance_sum += distance
            curr_stop_dict["days_passed"] = ((parsed_date_init_departure.hour * HOUR + \
                                              parsed_date_init_departure.minute) + total_minutes) // DAY

         curr_stop_dict["stop_id"] = -1
         curr_stop_dict["stop_code"] = stop
         hub_route.append(curr_stop_dict)

      #if there was an error during processing of the timetable, throw it away and continue
      if not is_valid:
         continue

      curr_grafikon["hub_route"] = hub_route
      curr_grafikon["route"] = []
      curr_grafikon["max_speed"] = 100
      curr_grafikon["max_weight"] = DEFAULT_WEIGHT
      curr_grafikon["max_length"] = DEFAULT_LENGTH
      curr_grafikon["distance"] = distance_sum
      curr_grafikon["total_minutes"] = total_minutes

      total_cost = 0
      #add the distance cost
      if distance_sum < 300:
         total_cost += distance_sum * 10
      elif distance_sum < 600:
         total_cost += 300 * 10 + (distance_sum - 300) * 9
      else:
         total_cost += 300 * 10 + 300 * 9 + (distance_sum - 600) * 8
      #add the time cost
      train_cost = 1000 / (24 * 60)
      total_cost += int(total_minutes * train_cost) 
      curr_grafikon["cost"] = total_cost

      op_days = curr_grafikon["operational_days"]

      #multiply the timetable for each week
      for day in op_days:
         graficon_copy = copy.deepcopy(curr_grafikon)
         graficon_copy["grafikon_ID"] = counter
         graficon_copy["operational_days"] = day
         timetables["trains"].append(graficon_copy)
         counter += 1
      
   with open(path_to_finalized_timetables, "w") as f:
      json.dump(timetables, f, indent=4)
   
 #  timetables = merge_timetables(timetables)
   timetables = filter_duplicite_timetables(timetables)

   with open(path_to_finalized_timetables, "w") as f:
      json.dump(timetables, f, indent=4)


if __name__=="__main__":
   finalize_timetables()