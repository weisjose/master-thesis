import pandas as pd
import numpy as np
import json
import copy

from global_variables import path_to_finalized_timetables, path_to_zelinka_timetables, path_to_merged_timetables

def merge_timetables(timetables = None):
   if timetables == None:
      with open(path_to_finalized_timetables, 'r') as file:
         timetables = json.load(file)

   with open(path_to_zelinka_timetables, 'r') as file:
      timetables_zelinka = json.load(file)

   new_timetables = copy.deepcopy(timetables)
   last_grafikon_id = timetables['trains'][-1]["grafikon_ID"] 
   for i in range(len(timetables_zelinka["trains"])):
      curr_timetable = timetables_zelinka["trains"][i]
      curr_timetable["grafikon_ID"] = last_grafikon_id + i + 1
      new_timetables["trains"].append(curr_timetable)

   with open(path_to_merged_timetables, "w") as f:
      json.dump(new_timetables, f, indent=4)

if __name__=="__main__":
   merge_timetables()