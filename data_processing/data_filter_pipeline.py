import sys
import os
import pandas as pd

current_path = os.getcwd()
directory_path = os.path.dirname(current_path)

sys.path.append(directory_path)

from creating_timetables.kontejnery_filter_w_paths import parse_import_and_export_file_paths
from creating_container_dataset.kontejnery_filter import filter_containers
from creating_timetables.route_creator import get_train_routes
from creating_timetables.trains_filter import parse_train_file
from creating_timetables.distance_matrix_creator import create_distance_matrix
from heuristic_simplification.terminal_matrix_creator import create_terminals_matrix
from creating_timetables.timetable_times import add_times_to_trains
from creating_timetables.timetables_finalizer import finalize_timetables
from creating_timetables.timetables_merge import merge_timetables
from creating_timetables.timetables_filter_duplicite import filter_duplicite_timetables
from heuristic_simplification.filter_terminals import filter_unreachable_terminals
from heuristic_simplification.stops_in_route_maker import get_possible_stops
from creating_container_dataset.kontejnery_filter_remove_impossible_containers import filter_impossible_containers

"""
parse_import_and_export_file_paths()
print("parse_import_and_export_file_paths completed")

filter_containers()
print("filter_containers completed")
get_train_routes()
print("get_train_routes completed")
parse_train_file()
print("parse_train_file completed")
create_distance_matrix()
print("create_distance_matrix completed")
create_terminals_matrix()
print("create_terminals_matrix completed")
add_times_to_trains()
print("add_times_to_trains completed")

finalize_timetables()
print("finalize_timetables completed")
"""
filter_unreachable_terminals()
print("filter_unreachable_terminals completed")
exit()
get_possible_stops()
print("get_possible_stops completed")
filter_impossible_containers()
print("filter_impossible_containers completed")
