import pandas as pd
from creating_container_dataset.import_kontejnery_filter import create_import_dataset
from creating_container_dataset.export_kontejnery_filter import create_export_dataset

from global_variables import path_to_import_filtered, path_to_export_filtered, path_to_kontejnery_filtered

def filter_containers():
   create_import_dataset()
   create_export_dataset()

   df1 = pd.read_csv(path_to_import_filtered)
   df2 = pd.read_csv(path_to_export_filtered)

   merged_df = pd.concat([df1, df2], ignore_index=True)
   merged_df.to_csv(path_to_kontejnery_filtered, index=False)

   print(f"Export + Import dataset file has been created: {path_to_kontejnery_filtered}")
if __name__=="__main__":
   filter_containers()