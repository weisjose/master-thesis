import pandas as pd
import json 
from datetime import datetime
import sys



#from global_variables import path_to_heuristic_algorithms_utils
#sys.path.append(path_to_heuristic_algorithms_utils)
#print(path_to_heuristic_algorithms_utils)
from utilities.utils import parse_date
from utilities.path_finder import find_path2


from global_variables import path_to_kontejnery_filtered, path_to_finalized_timetables, path_to_stops_in_routes
from global_variables import path_to_terminal_matrix_filtered, path_to_kontejnery_filtered_removed_impossible
from global_variables import HOUR, DAY, start_of_month

def filter_impossible_containers():
   df = pd.read_csv(path_to_kontejnery_filtered)

   df_terminals = pd.read_csv(path_to_terminal_matrix_filtered)

   with open(path_to_finalized_timetables, 'r') as file:
      timetables = json.load(file)

   with open(path_to_stops_in_routes, 'r') as file:
      possible_stops = json.load(file)

   kam_array = []
   odkud_array = []
   container_array_id = []
   info_array = []

   #gather container information
   for index, row in df.iterrows():
      kam_array.append(row['KAM'])
      odkud_array.append(row['ODKUD'])
      parsed_deadline = parse_date(row['DEADLINE'])
      parsed_release_time = parse_date(row['RELEASE_TIME'])
      time_difference_deadline = parsed_deadline - start_of_month
      time_difference_release_date = parsed_release_time - start_of_month
      deadline_in_minutes = int(max(0, time_difference_deadline.total_seconds() // 60 // DAY))
      release_date_in_minutes = int(max(0, time_difference_release_date.total_seconds() // 60 // DAY))
      deadline_in_minutes = min(31, deadline_in_minutes)
      container_array_id.append(index)
      info = (deadline_in_minutes, release_date_in_minutes, index)
      info_array.append(info)
     # if release_date_in_minutes >= deadline_in_minutes:
     #    print(release_date_in_minutes, deadline_in_minutes, index, row['CONTAINER'], row['RELEASE_TIME'])
     #    print("error")
     #    exit()

   hub_route_array = []
   grafikon_ids = []
   list_of_neighbors = {}
   terminals = df_terminals.columns[1:]
   for terminal in terminals:
      list_of_neighbors[terminal] = []

   #find all possibilites how to get from starting city to end city using one timetable
   for i in range(len(timetables['trains'])):
      #length.. 6 = 1 TEU.. wagon - 24 TEU = length / 12 containers
      hub_route = timetables['trains'][i]['hub_route']
      grafikon_ids.append(timetables['trains'][i]['grafikon_ID'])
  #    if i != data_grafikony['trains'][i]['grafikon_ID']:
  #       exit()
      parsed_start_date = datetime.strptime(hub_route[0]["departure"] , "%H:%M:%S")
      parsed_start_date_in_minutes = parsed_start_date.hour * HOUR + parsed_start_date.minute
      for x in range(len(hub_route)):
         starting_city = hub_route[x]["stop_code"]
         parsed_date = datetime.strptime(hub_route[x]["departure"] , "%H:%M:%S")
         if x == 0:
            op_day = (timetables['trains'][i]['operational_days'] - 1)
         else:
            op_day = (timetables['trains'][i]['operational_days'] - 1) + \
                     (parsed_start_date_in_minutes + hub_route[x]["curr_total_minutes"]) // DAY
         this_grafikon_time = op_day * DAY + parsed_date.hour * 60 + parsed_date.minute
         for j in range(x+1, len(hub_route)):
            end_city = hub_route[j]["stop_code"]
            list_of_neighbors[starting_city].append([end_city, i, this_grafikon_time, this_grafikon_time + 
                                                timetables['trains'][i]['hub_route'][j]["curr_total_minutes"]])


   container_id_array = []
   invalid_container_array_id = []

   #for each container, check if it is possible to transport it in time disregarding the train capacities
   for i in range(len(container_array_id)):
      info = info_array[i]
      idx = info[2]
      kam = kam_array[idx]
      odkud = odkud_array[idx]
      path, grafikon_arr = find_path2(list_of_neighbors, odkud, kam, info[1], info[0], possible_stops)
      if len(path) == 0:
         container_id_array.append(idx)
         invalid_container_array_id.append(idx)
      else:
         container_id_array.append(None)
   
   #drop containers which cannot be transported in time
   df = df.drop(invalid_container_array_id)

   df.to_csv(path_to_kontejnery_filtered_removed_impossible, index=False)
   print(f"kontejnery_filtered_removed_impossible file has been created: {path_to_kontejnery_filtered_removed_impossible}")

def filter_containers_with_no_train():
   #iterace pres grafikony - ziskat cisla vlaku
   #iterace pres kontejnery - zkontrolovat zda je cislo vlaku v grafikonech

   with open('grafikony/grafikon_casy_komplet_filter.json', 'r') as file:
      timetables = json.load(file)

   num_grafikons = len(timetables['trains'])
   train_numbers = []
   for i in range(num_grafikons):
      train_number = timetables['trains'][i]["train_number"]
      if train_number not in train_numbers:
         train_numbers.append(train_number)

   df_kontejnery = pd.read_csv('kontejnery_filtered_no_impossible_containers.csv')
   containers_to_drop = []
   for index, row in df_kontejnery.iterrows():
        used_trains = row['CISLO_VLAKU']
        used_trains = used_trains.split(',')
        print(used_trains, index)
        for train in used_trains:
           if train not in train_numbers:
              containers_to_drop.append(index)

   df_kontejnery = df_kontejnery.drop(containers_to_drop)

   file_path = 'kontejnery_filtered_with_valid_trains.csv'
   df_kontejnery.to_csv(file_path, index=False)

   print(f"kontejnery_filtered_with_valid_trains file has been created: {file_path}")
    

if __name__=="__main__":
  # filter_impossible_containers()
   filter_containers_with_no_train()