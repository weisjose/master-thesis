import pandas as pd
import numpy as np

from global_variables import path_to_import, path_to_import_filtered, path_to_trains, start_of_month

def create_import_dataset():
    df = pd.read_excel(path_to_import)
    df_valid_train = pd.read_excel(path_to_trains)

    #remove reduntant spaces in data
    df['ODKUD_VLAK'] = df['ODKUD_VLAK'].str.replace(' ', '')
    df['KAM_VLAK'] = df['KAM_VLAK'].str.replace(' ', '')
    df['PRISTAV'] = df['PRISTAV'].str.replace(' ', '')
    df['VYKLADKOVY_TERMINAL'] = df['VYKLADKOVY_TERMINAL'].str.replace(' ', '')
    df['TERMINAL_USKLADNENI'] = df['TERMINAL_USKLADNENI'].str.replace(' ', '')

    #consider only containers which are loaded ("lozeny" means loaded)
    df = df[df['PRAZDNY_LOZENY'] == 'LOZENY'][['CONTAINER', 'PRISTAV', 'VYKLADKOVY_TERMINAL', 
                                                'TERMINAL_USKLADNENI', 'TEU', 'ODKUD_VLAK', 'KAM_VLAK', 'CISLO_VLAKU', 
                                                'DATUM_VYKLADKY', 'PRIJEZD_LODI_DO_PRISTAVU', 'VAHA_ZBOZI']]
    
    #filter the containers for which we do not know the source and destination terminals
    df = df[df['PRISTAV'] != 'NEZNAM']
    df = df[df['PRISTAV'] != '????']
    df = df[df['VYKLADKOVY_TERMINAL'] != 'NEZNAM']
    df = df[df['VYKLADKOVY_TERMINAL'] != '????']
    
    #rename containers ID (one container may be transported more times during month, we need to distinguish these cases)
    #counter 100000 because the df will be merged later with containers from "export.xlsx" file
    last_container_name = None
    counter = 0
    invalid_container_ids = []
    for index, row in df.iterrows():
        current_container_name = row['CONTAINER']
        if current_container_name == last_container_name:
            counter += 0  
        else:
            counter += 1 
            if pd.isna(row['CISLO_VLAKU']):
                invalid_container_ids.append(counter)
        df.at[index, 'CONTAINER'] = counter
        last_container_name = current_container_name 
    counter = 0

    print(len(invalid_container_ids))

    #
    df = df[~df['CONTAINER'].isin(invalid_container_ids)]

    #filter containers which do not stop at the 'VYKLADKOVY_TERMINAL'
    valid_container_ids = df['CONTAINER'][
        (df['KAM_VLAK'] == df['VYKLADKOVY_TERMINAL']) & (df['CONTAINER'].shift(-1) != df['CONTAINER'])
    ].unique()
    df = df[df['CONTAINER'].isin(valid_container_ids)]

    #filter containers which are transported by a train that is not in the data
    valid_train_ids = np.array(df_valid_train['CISLO_VLAKU'].tolist())
    invalid_container_ids2 = df.loc[
        ~df['CISLO_VLAKU'].isin(valid_train_ids) & ~df['CISLO_VLAKU'].isnull(),
        'CONTAINER'
    ].unique()
    df = df[~df['CONTAINER'].isin(invalid_container_ids2)]

    #filter containers for which we do not know release date('PRIJEZD_LODI_DO_PRISTAVU') and deadline ('DATUM_VYKLADKY')
    valid_rows = df['PRIJEZD_LODI_DO_PRISTAVU'].notnull()
    df = df[valid_rows]

    valid_rows = df['DATUM_VYKLADKY'].notnull()
    df = df[valid_rows]
    
    #filter redundant columns
    df = df[['CONTAINER', 'VYKLADKOVY_TERMINAL', 'PRISTAV', 
                                                                'TEU', 'DATUM_VYKLADKY', 'PRIJEZD_LODI_DO_PRISTAVU',
                                                                'CISLO_VLAKU', 'VAHA_ZBOZI']]
    
    #rename the columns
    df = df.rename(columns={'VYKLADKOVY_TERMINAL': 'KAM'})
    df = df.rename(columns={'PRISTAV': 'ODKUD'})
    df = df.rename(columns={'PRIJEZD_LODI_DO_PRISTAVU': 'RELEASE_TIME'})
    df = df.rename(columns={'DATUM_VYKLADKY': 'DEADLINE'})

    #to every container ID, find all trains the container uses and write them to 'CISLO_VLAKU' column
    #maybe not used - might delete this later
    last_container_name = None
    counter = 0
    train_number_array = []
    num_of_rows = 0
    iter_counter = 0
    last_idx = 0
    for idx, row in df.iterrows():
        current_container_name = row['CONTAINER']
        train_number = row['CISLO_VLAKU']
        if current_container_name == last_container_name or last_container_name == None:
            if train_number not in train_number_array and not pd.isna(train_number):
                train_number_array.append(train_number)
            counter += 0  
            num_of_rows += 1
        else:
            train_number_string = ""
            for i, train in enumerate(train_number_array):
                train_number_string = train_number_string + train
                if i != len(train_number_array) - 1:
                    train_number_string = train_number_string + ","
            for i in range(last_idx - num_of_rows + 1, last_idx + 1):
                df.at[i, 'CISLO_VLAKU'] = train_number_string
            train_number_array = []
            if train_number not in train_number_array and not pd.isna(train_number):
                train_number_array.append(train_number)
            num_of_rows = 1
            counter += 1 
        last_container_name = current_container_name 
        iter_counter += 1
        last_idx = idx

    train_number_string = ""
    for i, train in enumerate(train_number_array):
        train_number_string = train_number_string + train
        if i != len(train_number_array) - 1:
            train_number_string = train_number_string + ","
    for i in range(last_idx - num_of_rows + 1, last_idx + 1):
        df.at[i, 'CISLO_VLAKU'] = train_number_string
    counter = 0
    
    #drop duplicate rows
    df = df.drop_duplicates()

    #check if deadline is later than release date, filter if not
    #this is to handle error when one of container has datetime year 2302
    def check_year(row):
        try:
            if isinstance(row['RELEASE_TIME'], str):
                year_part_release_time = int(row['RELEASE_TIME'][:4])
            else:
                year_part_release_time = row['RELEASE_TIME'].year
            if isinstance(row['DEADLINE'], str):
                year_part_deadline = int(row['DEADLINE'][:4])
            else:
                year_part_deadline = row['DEADLINE'].year
            return year_part_release_time <= 2025 and year_part_deadline <= 2025
        except ValueError:
            return False
        
    def release_time_mask(row):
        if isinstance(row['RELEASE_TIME'], str):
            release_time = int(row['RELEASE_TIME'][0:2])
        else:
            release_time = row['RELEASE_TIME'].day
        return release_time < 31

    mask = df.apply(check_year, axis=1)
    df = df[mask]

    df['DEADLINE'] = pd.to_datetime(df['DEADLINE'])
    df['RELEASE_TIME'] = pd.to_datetime(df['RELEASE_TIME'])

    df = df[df['DEADLINE'] > df['RELEASE_TIME']]
    df = df[df['DEADLINE'] > start_of_month]
    mask = df.apply(release_time_mask, axis=1)
    df = df[mask]

    #save to csv file
    df.to_csv(path_to_import_filtered, index=False)

    print("number of containers after filtering: ", len(df))

    print(f"Filtered CSV file has been created: {path_to_import_filtered}")
if __name__=="__main__":
    create_import_dataset()