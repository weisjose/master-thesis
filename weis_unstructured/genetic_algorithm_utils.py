import numpy as np
from heuristika_1_container_serazeni_validator5_pro_LS import constructive_heuristic_for_LS
import copy

def createInitSolution(num_dims = 10, mean = 0, std_dev = 0.1):
    init_solution = np.random.normal(mean, std_dev, num_dims)
    normalized_init_solution = (init_solution - np.min(init_solution)) / (np.max(init_solution) - np.min(init_solution))
    return normalized_init_solution

def evaluate_terminal_order_with_cost(x, df):
    print(x)
    terminal_priorities = np.argsort(np.argsort(x))
    print(terminal_priorities)
#    f, cost = constructive_heuristic_with_terminal_end(df, terminal_priorities)
    f, cost = constructive_heuristic_for_LS(df, terminal_priorities)
    return f, cost

def evaluate_terminal_order(x, df):
    print(x)
    terminal_priorities = np.argsort(np.argsort(x))
    print(terminal_priorities)
#    f, cost = constructive_heuristic_with_terminal_end(df, terminal_priorities)
    f = constructive_heuristic_for_LS(df, terminal_priorities)
    return f

def PertubationNormalNormalize(x, std_dev = 0.1):
    x_copy = copy.deepcopy(x)
    normal_mean = 0
    x_copy += np.random.normal(normal_mean, std_dev, len(x))
    normalized_x_copy = (x_copy - np.min(x_copy)) / (np.max(x_copy) - np.min(x_copy))
    return normalized_x_copy